module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    cssmin: {
      target: {
        files: {
            'wp-content/themes/sbpvr/css/app.min.css': [
                'wp-content/themes/sbpvr/bootstrap/bootstrap.min.css',
                'wp-content/themes/sbpvr/bootstrap/datepicker/css/bootstrap-datepicker.min.css',
                'wp-content/themes/sbpvr/apartment-font/css/font-awesome.min.css',
                'wp-content/themes/sbpvr/apartment-font/css/iconmoon.css',
                'wp-content/themes/sbpvr/css/plugins.css',
                'wp-content/themes/sbpvr/css/apartment-layout.css',
                'wp-content/themes/sbpvr/css/apartment-colors-blue.css',
                'wp-content/themes/sbpvr/css/lightbox.css',
                'wp-content/themes/sbpvr/css/sbpvr.css',
                'wp-content/themes/sbpvr/css/fishing.css'
            ]
        }
      }
    },
    uglify: {
        options: {
          mangle: {
            except: ['jQuery', 'Backbone']
          }
        },
        my_target: {
          files: {
            'wp-content/themes/sbpvr/js/app.min.js': [
                'wp-content/themes/sbpvr/js/jQuery/jquery.min.js',
                'wp-content/themes/sbpvr/js/jQuery/jquery-ui.min.js',
                'wp-content/themes/sbpvr/js/jquery.lazyload.min.js',
                'wp-content/themes/sbpvr/bootstrap/bootstrap.min.js',
                'wp-content/themes/sbpvr/bootstrap/datepicker/js/bootstrap-datepicker.min.js',
                'wp-content/themes/sbpvr/js/plugins.js',
                /*'wp-content/themes/sbpvr/mail/validate.js',*/
                'wp-content/themes/sbpvr/js/apartment.js',
                'wp-content/themes/sbpvr/js/lightbox.js'
            ]
          }
        }
      },
      watch: {
          scripts: {
            files: ['wp-content/themes/sbpvr/js/*.js', 'wp-content/themes/sbpvr/css/*.css'],
            tasks: ['cssmin', 'uglify'],
            options: {
              spawn: false,
            },
          },
        }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['cssmin', 'uglify', 'watch']);

};