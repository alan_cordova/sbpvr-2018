<!DOCTYPE html>
<html >
<head>
  <meta charset="utf-8">
  <title>Mantenimiento 24/7 | Silva Brisset Realtors</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">

  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/styles.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>

  
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/wp-content/themes/sbpvr2/images/fav/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/wp-content/themes/sbpvr2/images/fav/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/wp-content/themes/sbpvr2/images/fav/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/wp-content/themes/sbpvr2/images/fav/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/wp-content/themes/sbpvr2/images/fav/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/wp-content/themes/sbpvr2/images/fav/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Silva Brisset Realtors"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/wp-content/themes/sbpvr2/images/fav/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/wp-content/themes/sbpvr2/images/fav/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/wp-content/themes/sbpvr2/images/fav/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/wp-content/themes/sbpvr2/images/fav/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/wp-content/themes/sbpvr2/images/fav/mstile-310x310.png" />
</head>
<body class="body">
  <div id="code1" style="display:none;">82</div>
  <div id="code2" style="display:none;">97863</div>
  <div id="code3" style="display:none;">8</div>
    <?php //Analitics ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        var a = document.getElementById('code1').innerHTML;
        var b = document.getElementById('code2').innerHTML;
        var c = document.getElementById('code3').innerHTML;
        ga('create', 'UA' + '-'+(c+b+a)+'-1', 'auto');
        ga('send', 'pageview');
    </script>

  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <div class="navbar-container w-container">
        <a href="#intro" class="navbar-brand w-nav-brand">
            <img src="images/logo.svg" class="brand-img" alt="">
        </a>
        
        <nav role="navigation" class="nav-menu w-nav-menu">
            <a href="#nosotros" class="nav-link w-nav-link">Nosotros</a>
            <a href="#servicios" class="nav-link w-nav-link">Servicios</a>
            <a href="#clientes" class="nav-link w-nav-link">Nuestros Clientes</a>
            <a href="#contacto" class="nav-link w-nav-link">Contacto</a>
        </nav>
      
        <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
        </div>
    </div>
  </div>

  <div data-animation="slide" data-hide-arrows="1" data-duration="500" data-infinite="1" id="intro" class="slider w-slider">
    <div class="slider-intro-main-wrapper">
      <div class="slider-intro-wrapper">
        <div class="slider-intro-container">
          <div class="intro-bubbles-main-wrapper">
            <div class="intro-bubbles-wrapper">
              <div data-ix="bubble" class="intro-bubble red"></div>
              <div data-ix="bubble-2" class="green intro-bubble"></div>
              <div data-ix="bubble-3" class="intro-bubble orange"></div>
            </div>
          </div>
          <div data-ix="bubble-end" class="text-block">Lealtad<br>Compromiso<br>Garantía</div>
        </div>
      </div>
    </div>
    <div class="slider-mask w-slider-mask">
      <div class="slide w-slide"><img src="images/bg-mantenimiento.jpg" srcset="images/bg-mantenimiento-p-500.jpeg 500w, images/bg-mantenimiento-p-1080.jpeg 1080w, images/bg-mantenimiento-p-1600.jpeg 1600w, images/bg-mantenimiento.jpg 2000w" sizes="100vw" class="slide-img" alt="Mantenimiento 24/7" title="Mantenimiento 24/7"></div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-left">
      <div class="w-icon-slider-left"></div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-right">
      <div class="w-icon-slider-right"></div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-round w-slider-nav"></div>
  </div>
  <div id="nosotros" class="intro-wrapper">
    <div class="intro-inner">
        <img src="images/logo-tools.svg" class="intro-left">
        <div class="intro-right">
            <div class="intro-text">El trabajo que realizamos consiste en conocer a nuestros clientes para poder ofrecerles los servicios de acuerdo a sus necesidades y/o condiciones especiales.</div>
            <div class="intro-text last">Somos una equipo especializado en <strong class="bold">mantenimiento, Limpieza
y administración</strong> de negocios comerciales y casas residenciales.</div>
        </div>
    </div>
  </div>
  <div id="servicios" class="services-wrapper">
    <img src="images/sample-sideimg.jpg" srcset="images/sample-sideimg-p-500.jpeg 500w, images/sample-sideimg.jpg 800w" sizes="(max-width: 991px) 100vw, 50vw" class="services-img" alt="Servicios | Mantenimiento 24/7" title="Servicios | Mantenimiento 24/7">
    <div class="services-right">
      <div class="services-right-wrapper">
        <div class="services-right-inner">
          <div class="services-title-wrapper">
            <h1 class="service-title">Nuestros Servicios</h1>
          </div>
          <div class="services-list-wrapper">
            <ul class="services-list w-clearfix">
              <li class="services-list-item">
                <div class="services-list-text">Obra civil</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Cerrajería</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Carpintería</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Fumigaciones</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Trabajos de electricidad alta y baja presión</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Aire acondicionado</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Instalación de focos LED</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Refrigeración</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Limpieza profunda de cristales</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Boilers</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Trabajos de Aluminio en general</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Lavado de muebles</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Impermeabilización</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Pulido de pisos</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Pintura y tablaroca</div>
              </li>
              <li class="services-list-item">
                <div class="services-list-text">Limpieza gruesa y fina</div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="clientes" class="clients-wrapper">
    <div class="clients-inner">
      <h2 class="clients-title">Algunos de nuestros clientes</h2>
      <div class="clients-list">
        <a href="http://tresmares.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/tresmares-logo.jpg" class="client-img" alt="">
        </a>
        <a href="https://sbpvr.com/es/desarrollos/shangri-la" class="client-item w-inline-block" target="_blank">
            <img src="images/shengrila-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://www.grandvenetian.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/grand-venetian-logo.jpg" class="client-img" alt="">
        </a>
        <a href="https://tintoque.mx" class="client-item w-inline-block" target="_blank">
            <img src="images/tintoque-logo.jpg" class="client-img" alt="">
        </a>
        <a href="https://www.hangten.com.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/hangten-logo.jpg" class="client-img" alt="">
        </a>
        <a href="https://www.juliocepeda.com/" class="client-item w-inline-block" target="_blank">
            <img src="images/juliocepeda-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://www.innovasport.com/" class="client-item w-inline-block" target="_blank">
            <img src="images/innova-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://www.laislapuertovallarta.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/laisla-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://www.sirloin.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/sirloin-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://www.comex.com.mx/" class="client-item w-inline-block" target="_blank">
            <img src="images/comex-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://condoportofinopv.com/" class="client-item w-inline-block" target="_blank">
            <img src="images/portofino-logo.jpg" class="client-img" alt="">
        </a>
        <a href="http://dealers.kia.com/mx/vallarta/main.html" class="client-item w-inline-block" target="_blank">
            <img src="images/kia-logo.jpg" class="client-img" alt="">
        </a>
      </div>
    </div>
  </div>
  <div id="contacto" class="contact">
    <h2 class="clients-slogan">Valoramos su inversión sin perder de vista su crecimiento.</h2>
    <p class="clients-slogan-2">Tu mantenimiento en la manos de nuestros expertos contactanos y trabajemos juntos.</p>
    <div class="form-wrapper">
      <form action="/wp-content/themes/sbpvr2/includes/modules/contact_mantenimiento24-7.php" id="contact_form" method="post" class="form">
        <div class="input-wrapper">
            <input type="text" class="input w-input" maxlength="256" name="f_name" placeholder="Nombre:" required="">
            <div class="input-separator"></div>
            <input type="text" placeholder="Teléfono: " maxlength="256" name="f_phone" required="" class="input w-input">
        </div>
        <input type="email" class="input w-input" maxlength="256" name="f_email" placeholder="Correo electrónico: " required="">
        
        <h2 class="im-interest-call">Me interesa:</h2>
        <div class="checkbox-list-outter-wrapper">
          <div class="checkbox-list-wrapper">
            <div class="checkbox-list-left">
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Obra Civil">
                    <label for="checkbox" class="w-form-label">Obra Civil</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-10" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Carpintería">
                    <label for="checkbox-10" class="w-form-label">Carpintería</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-9" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Trabajos de electricidad alta y baja tensión">
                    <label for="checkbox-9" class="w-form-label">Trabajos de electricidad alta y baja tensión</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-8" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Instalación de focos LED">
                    <label for="checkbox-8" class="w-form-label">Instalación de focos LED</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-7" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Limpieza profunda de cristales">
                    <label for="checkbox-7" class="w-form-label">Limpieza profunda de cristales</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-6" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Trabajos de Aluminio en general">
                    <label for="checkbox-6" class="w-form-label">Trabajos de Aluminio en general</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-5" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Impermeabilización">
                    <label for="checkbox-5" class="w-form-label">Impermeabilización</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-4" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Pintura y Tablaroca">
                    <label for="checkbox-4" class="w-form-label">Pintura y Tablaroca</label>
                </div>
            </div>
            <div class="checkbox-list-right">
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-2" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Cerrajería">
                    <label for="checkbox-2" class="w-form-label">Cerrajería</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-11" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Obra Civil">
                    <label for="checkbox-11" class="w-form-label">Obra Civil</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-12" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Aire acondicionado">
                    <label for="checkbox-12" class="w-form-label">Aire acondicionado</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-13" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Refrigeración">
                    <label for="checkbox-13" class="w-form-label">Refrigeración</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-14" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Boilers">
                    <label for="checkbox-14" class="w-form-label">Boilers</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-15" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Lavado de muebles">
                    <label for="checkbox-15" class="w-form-label">Lavado de muebles</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-16" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Pulido de pisos">
                    <label for="checkbox-16" class="w-form-label">Pulido de pisos</label>
                </div>
                <div class="checkbox-list-item w-checkbox">
                    <input type="checkbox" id="checkbox-17" name="checkbox[]" class="checkbox-input w-checkbox-input" value="Limpieza gruesa y fina">
                    <label for="checkbox-17" class="w-form-label">Limpieza gruesa y fina</label>
                </div>
            </div>
          </div>
        </div>
        <label for="field" class="label">Permitenos saber si tienes una petición especial:</label>
        <textarea name="f_message" maxlength="5000" class="input textarea w-input"></textarea>
        <input type="submit" value="Enviar" class="submit w-button"></form>
      <div class="w-form-done">
        <div>Tu solicitud ha sido enviada, la atenderemos lo más pronto posible.</div>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="footer-row">
      <div class="footer-col"><a href="#" class="footer-brand navbar-brand w-nav-brand"><img src="images/logo.svg" class="brand-img"></a></div>
      <div class="center footer-col">
        <div class="text-block-2">Paseo de la Marina 355-20<br>Marina Vallarta, Puerto Vallarta<br>Jalisco México</div>
      </div>
      <div class="footer-col right">
        <div>
          <a class="link-phone">+52 (322) 221 0051 <br xmlns="http://www.w3.org/1999/xhtml"></a>
          <a href="mailto:info@sbpvr.com" class="footer-link">Info@sbpvr.com</a><br>
          <a href="https://sbpvr.com" target="_blank" class="footer-link">sbpvr.com</a>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-copyright">© Copyright 2017 <a href="https://sbpvr.com" target="_blank" class="copyright-link">Silva Brisset Realtors</a>. Todos los derechos reservados.</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

    <script>
        $(function(){
            function form_in_process($btn, init){
                init = (typeof init == "undefined") ? false : init;
                var in_process = $btn.data('in-process');
                var dots = 0;
                var text = '';

                if(typeof in_process == "undefined" || init == true){
                    $btn.data('in-process', '1');
                    $btn.data('dots', '1');

                    if(!$btn.data('original-value')){ //set original value for first time
                        $btn.data('original-value', $btn.val());
                    }
                    in_process = 1;
                }

                in_process = parseInt(in_process);
                if(in_process == 1){
                    dots = parseInt($btn.data('dots'));
                    text = $btn.data('original-value');
                    for(var i = 0; i < dots; i++){
                        text += '.';
                    }

                    dots++;
                    dots = (dots > 3) ? 1 : dots;

                    $btn.data('dots', dots);
                    $btn.val(text);
                    setTimeout(function(){
                        form_in_process($btn);
                    }, 500);
                }
                
            }
            function form_stop_process($btn){
                $btn.data('in-process', '0');
                $btn.data('dots', '1');
                $btn.val($btn.data('original-value'));
            }

            // contact form
            var sending_contact_form = false;
            $("#contact_form").submit(function(e){
                e.preventDefault();
                var form = $(this);

                if(!sending_contact_form){
                    var btn_process = form.find('.submit');
                    form_in_process(btn_process, true);

                    sending_contact_form = false;
                    var action = $(this).attr('action');
                    var req = $(this).serializeArray();

                    form.closest('.form-wrapper').find(".w-form-done").hide();
                    $.post(action, req, function(data){
                        /*form.fadeOut('slow', function(){
                        });*/
                    
                        form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                        
                        form_stop_process(btn_process);
                        sending_contact_form = false;
                        form.trigger("reset");
                    },'json');
                }
            });
            if($("#contact_form").length > 0){
                setTimeout(function(){
                    var input = $("<input type='hidden' name='cmd' value='765476543a'>");
                    $("#contact_form").append(input);
                },1500);
            }
        });
    </script>


</body>
</html>