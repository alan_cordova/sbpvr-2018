<?php
/*
Template Name: For Realtors
*/
?>
<?php
    the_post();

    $list = get_field('c_fr_development');
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>

<div class="for-realtors">
    <div class="for-realtors-container">
        <div class="for-realtors-container-parent">
            <div class="container">
                <div class="development-content">
                    <h1 class="developments-title"><?php the_title()?></h1>
                    <div><?php the_content()?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?php foreach($list as $item):?>
        <div class="for-realtor-item">
            <img class="for-realtor-img" 
                src="<?=$item['c_fr_development_img']['sizes']['large']?>" 
                alt="<?=$item['c_fr_development_img']['alt']?>"
                title="<?=$item['c_fr_development_img']['title']?>"
            />
            <div class="for-realtor-container">
                <a class="for-realtor-title-link w-inline-block" target="_blank" href="<?=get_permalink($item['c_fr_development_page']->ID);?>">
                    <h1 class="for-realtor-title"><?=$item['c_fr_development_page']->post_title;?></h1>
                </a>
                <?php if($item['c_fr_development_files']):?>
                    <div class="for-realtor-list">
                        <?php foreach($item['c_fr_development_files'] as $file):?>
                            <a class="for-realtor-link" href="<?=$file['c_fr_file']['url']?>" target="_blank">
                                · <?=$file['c_fr_file_name']?>
                            </a>
                        <?php endforeach;?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    <?php endforeach;?>
</div>

<?php get_footer();?>