<?php
    include 'includes/cms_ids.php';
    $quick_links = c_prepare_menu_items(wp_get_nav_menu_items('Quick links menu ' . LANG));
    $services    = c_prepare_menu_items(wp_get_nav_menu_items('Services menu ' . LANG));
    $address     = get_field('c_global_address', 'option');
    $phone       = get_field('c_global_phone', 'option');
    $email       = get_field('c_global_email', 'option');
    $be          = get_field('c_global_be', 'option');
    $instagram   = get_field('c_global_instagram', 'option');
    $facebook    = get_field('c_global_facebook', 'option');
?>
    <?php require_once 'includes/modules/mapSearch.php'; ?>
    <div class="v2-footer">
        <div class="newsletter-section">
            <div class="container-3 w-container">
                <h3 class="heading"><?=pll__('Suscribe title')?></h3>
                <p class="paragraph"><?=pll__('Suscribe subtitle')?></p>
                <div class="newsletter-form">
                    <div class="form-block">
                        <form id="newsletter-form" class="form" action="<?=THEME_PATH?>/includes/modules/constant_contact.php">
                            <input type="hidden" name="f_cmd" class="cmd">
                            <input type="email" class="w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Enter your email')?>" required="required">
                            <input type="submit" value="<?=pll__('Send')?>" class="submit-button w-button">
                        </form>
                        <div class="newsletter-success"><?=pll__('Newsletter success')?></div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer-section">
            <div class="container-4 w-container">
                <a href="<?=pll_home_url()?>" class="v2-logo-footer">
                    <img src="<?=THEME_PATH?>/images/sb-logo-blanco.svg">
                </a>
                <div class="div-block-3">
                    <p class="paragraph"><?=$address?> | <a href="mailto:<?=$email?>" class="link-2"><?=$email?> | </a><a href="tel: <?=$phone?>" class="link-3"><?=$phone?></a></p>
                </div>
                <div class="v2-container-icons-footer">
                    <div>
                        <div class="v2-text-icons-footer"><?=pll__('Follow us and never miss a thing')?></div>
                    </div>
                    <div class="v2-icons-footer">
                        <div class="v2-icon-text-footer"><a href="mailto:<?=$email?>" class="v2-link-icon-footer"><em class="v2-icon-footer"><i class="far fa-envelope-open"></i></em></a></div>
                        <div class="v2-icon-text-footer"><a href="<?=$be?>" class="v2-link-icon-footer" target="_blank"><em class="v2-icon-footer"><i class="fab fa-behance"></i></em></a></div>
                        <div class="v2-icon-text-footer"><a href="<?=$instagram?>" class="v2-link-icon-footer" target="_blank"><em class="v2-icon-footer"><i class="fab fa-instagram"></i></em></a></div>
                        <div class="v2-icon-text-footer"><a href="<?=$facebook?>" class="v2-link-icon-footer" target="_blank"><em class="v2-icon-footer"><i class="fab fa-facebook-f"></i></em></a></div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    
    <?php $tawkKey = (LANG == 'en') ? '594c4057e9c6d324a4736e3c' : '594c42fbe9c6d324a4736e3f';?>
    <?php $tawkTime = (is_front_page()) ? '5000' : '0';?>
    <script>
        var _tawkKey = '<?=$tawkKey?>';
        var _tawkTime = <?=$tawkTime?>;
        var _is_front_page = <?=(is_front_page()) ? 'true' : 'false';?>;
        var _path = '<?=THEME_PATH?>';
    </script>

    <?php wp_footer(); ?>
    <script type="text/javascript">
        var $ = jQuery;
    </script>
    <script src="<?=THEME_PATH?>/js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js"></script>
    <script src="<?=THEME_PATH?>/js/jrange/jquery.range.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.0/masonry.pkgd.min.js"></script>-->
    
    <?php /* if(is_front_page()):?>
        <script src="<?=THEME_PATH?>/js/howler/howler.min.js"></script>
    <?php endif; */?>

    <script>
        var mapInterval = setInterval(function() {
            if (typeof initMaps === "function") {
                initMaps();
                clearInterval(mapInterval);
            }
        }, 500);

        var _THEME_PATH = '<?=THEME_PATH?>';
        var _LIVE_SITE = <?=LIVE_SITE ? "true" : "false";?>;
        var _IS_ADMIN = <?=is_user_logged_in() ? "true" : "false";?>;
        var _LANG = '<?=LANG?>';
        var _HOME_PAGE = '<?=get_permalink($cms_ids["home"]);?>';
    </script>
    
    <script type="text/javascript">var base_url = '<?=THEME_PATH?>/';</script>

    <script src="<?=THEME_PATH?>/vendor/calendar/js/bootstrap-year-calendar.min.js"></script>

    <?php if(is_user_logged_in()):?>
        <script type="text/javascript" src="<?=THEME_PATH?>/js/clipboard.min.js"></script>
    <?php endif;?>
    <script src="<?=THEME_PATH?>/js/map_styles.js"></script>
    <script src="<?=THEME_PATH?>/js/js-cookie.js"></script>
    <script src="<?=THEME_PATH?>/js/scripts.js?v=<?=THEME_VERSION?>"></script>


</body>

</html>
        