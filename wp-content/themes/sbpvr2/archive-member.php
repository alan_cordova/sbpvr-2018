<?php require_once 'includes/modules/process-form-member.php';?>
<?php get_header();?>
<div class="modal-bg">
    <div class="modal-content">
        <div class="modal-container w-clearfix">
            <a href="#" class="v2-close-modal btn-close w-button"><em class="italic-text-2"></em></a>
            <div class="v2-contact-form">
                <h3><?=pll__('Contact Member')?></h3>
                <div class="form-block-2">
                    <form id="contact_form" action="<?=get_permalink()?>" class="form-2">
                        <input type="hidden" name="f_cmd" class="cmd" value="" />
                        <div class="div-block-9">
                            <input class="contact-form-input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                            <input class="contact-form-input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                        </div>
                        <input class="v2-member-email" type="hidden" name="f_member" value="">
                        <input class="contact-form-input-lg w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                        <textarea class="contact-form-input-lg w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                        <input class="btn-black w-button" type="submit" value="<?=pll__('Send')?>">
                    </form>
                    <div class="w-form-done">
                        <div><?=pll__('Contact success response')?></div>
                    </div>
                    <div class="w-form-fail">
                        <div><?=pll__('Contact error response')?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>

<div class="team-section">
    <div class="v2-container-meet-our-team w-container">
        <?php require_once 'includes/modules/breadcrumbs.php';?>
        <h3 class="v2-title-meet-our-team"><?=pll__('Title Meet Our Team')?></h3>
        <p class="paragraph"><?=pll__('Subtitle Meet Our Team')?></p>
        <div class="v2-container-meet-team w-hidden-small w-hidden-tiny">
            <?php foreach($posts as $post):?>
                <?php setup_postdata($post);?>
                <?php
                    $isAgent     = get_field('c_is_agent', $post->ID);
                    $imgMember   = get_field('c_image_member', $post->ID);
                    $jobMember   = get_field('c_job_member', $post->ID);
                    $emailMember = get_field('c_email_member', $post->ID);
                ?>
                <a href="<?=($isAgent)?get_permalink():'#'?>" data-email="<?=$emailMember?>" class="<?=($isAgent)?'':'v2-contact-member'?> slide-team-item w-inline-block">
                    <div class="slide-team-item-picture-home" style="background-image: url(<?=$imgMember['sizes']['img-member-300x300']?>)">
                        <img src="<?=THEME_PATH?>/images/image-placeholder.svg" class="v2-img-profile">
                    </div>
                    <div class="slide-team-item-name"><?=$post->post_title?></div>
                    <div class="slide-team-item-contact"><?=$jobMember?> | <?=$emailMember?></div>
                </a>
            <?php endforeach;?>
        </div>
        <div class="v2-container-meet-team w-hidden-main w-hidden-medium">
            <?php foreach($posts as $post):?>
                <?php setup_postdata($post);?>
                <?php
                    $imgMember   = get_field('c_image_member', $post->ID);
                    $jobMember   = get_field('c_job_member', $post->ID);
                    $emailMember = get_field('c_email_member', $post->ID);
                ?>
                <?php if($isAgent) { ?>
                    <a href="<?=get_permalink()?>" class="slide-team-item w-inline-block">
                <?php }else{ ?>
                    <div class="slide-team-item w-inline-block">
                <?php } ?>
                    <div class="slide-team-item-picture" style="background-image: url(<?=$imgMember['sizes']['img-member-300x300']?>)">
                        <img src="<?=THEME_PATH?>/images/image-placeholder.svg" class="v2-img-profile">
                    </div>
                    <div class="slide-team-item-name"><?=$post->post_title?></strong></div>
                    <div class="slide-team-item-contact"><?=$jobMember?> | <?=$emailMember?></div>
                <?php if($isAgent) { ?>
                    </a>
                <?php }else{ ?>
                    </div>
                <?php } ?>
            <?php endforeach;?>
        </div>
    </div>
</div>
<?php get_footer();?>