<?php
/* HOME TRANSLATIONS */
pll_register_string('menu-about', 'SB REALTORS');
pll_register_string('menu-sub-text-about', 'About Us and Contact');
pll_register_string('menu-developments', 'DEVELOPMENTS');
pll_register_string('menu-sub-text-developments', 'Discover what the bay has to offer');
pll_register_string('menu-properties', 'PROPERTIES');
pll_register_string('menu-sub-text-properties', 'Discover our Amazing Property portfolio');
pll_register_string('menu-rentals', 'RENTALS');
pll_register_string('menu-sub-text-rentals', 'Discover our Amazing Vacation rental portfolio');
pll_register_string('menu-concierge', 'CONCIERGE');
pll_register_string('menu-sub-text-concierge', 'Our qualify team and services best in the bay');
pll_register_string('menu-mls-search', 'MLS SEARCH');
pll_register_string('menu-sub-text-mls-search', 'Discover our Amazing Property portfolio');

pll_register_string('btn-view-all-members', 'VIEW ALL MEMBERS');
pll_register_string('btn-get-in-touch', 'GET IN TOUCH');

pll_register_string('form-name', 'Name');
pll_register_string('form-phone', 'Phone');
pll_register_string('form-email', 'Email');
pll_register_string('form-message', 'Message');
pll_register_string('form-sucess-response', 'Contact success response');
pll_register_string('form-error-response', 'Contact error response');

pll_register_string('suscribe-title', 'Suscribe title');
pll_register_string('suscribe-subtitle', 'Suscribe subtitle');
pll_register_string('suscribe-placeholder', 'Enter your email');
pll_register_string('suscribe-send', 'Send');
pll_register_string('suscribe-sucess', 'Newsletter success');

pll_register_string('footer-follow-us', 'Follow us and never miss a thing');

/* ABOUT US TRANSLATIONS */
pll_register_string('title-about-us-bg', 'ABOUT US');
pll_register_string('take-virtual-tour', 'Take the Virtual Tour');
pll_register_string('take-me-there', 'Take Me There');

/* DEVELOPMENTS TRANSLATIONS */
pll_register_string('title-developments', 'DEVELOPMENTS');
pll_register_string('show-property-map', 'Show me this property in map');
pll_register_string('get-pdf', 'Get PDF');
pll_register_string('amenities-specs', 'Amenities &amp; Specs');
pll_register_string('view-sales-listing', 'View sales listing');
pll_register_string('view-rentals', 'View rentals');
pll_register_string('view-more', 'VIEW MORE');

/* ARCHIVE PROPERTIES TRANSLATIONS */
pll_register_string('baths', 'Baths');
pll_register_string('beds', 'Beds');
pll_register_string('load-more-properties', 'Load more properties');

/* SINGLE PROPERTY TRANSLATIONS */
pll_register_string('parking-space', 'Parking Space');
pll_register_string('contact-agent', 'Contact Agent');
pll_register_string('related-properties', 'Related Properties');

/* CONCIERGE TRANSLATIONS */
pll_register_string('CONCIERGE', 'CONCIERGE');

/* MEET OUR TEAM TRANSLATIONS */
pll_register_string('title-meet-our-team', 'Title Meet Our Team');
pll_register_string('subtitle-meet-our-team', 'Subtitle Meet Our Team');
pll_register_string('ask-this-agent', 'Ask This Agent');
pll_register_string('properties-not-found', 'Properties Not Found');
pll_register_string('contact-member', 'Contact Member');