<?php 


//load language constants
if(function_exists('pll_current_language')){
    define('LANG', pll_current_language());

    //pages ids for production or development
    include('cms_ids.php');
}


//custom functions (avoid multiple call to general functions)
if(!function_exists('hide_email')){
    require_once 'functions/general.php';
    require_once 'helpers/dimox_breadcrumbs.php';
    require_once 'functions/query_filters.php';
}