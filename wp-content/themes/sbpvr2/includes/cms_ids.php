<?php

$cms_ids = array();

if(LIVE_SITE){ 
    //production pages ids
    $cms_ids['home'] = pll_get_post(21, LANG);
    $cms_ids['about'] = pll_get_post(47, LANG);
    $cms_ids['tres-mares'] = pll_get_post(2902, LANG);
    $cms_ids['shangrila'] = pll_get_post(2904, LANG);
    $cms_ids['marina-tower'] = pll_get_post(2918, LANG);
    $cms_ids['azulejos'] = pll_get_post(4880, LANG);
    $cms_ids['vmarina'] = 5692; //not translated to ES
    $cms_ids['developments'] = pll_get_post(38, LANG);
    $cms_ids['favorites'] = pll_get_post(4969, LANG);
    $cms_ids['concierge'] = pll_get_post(4888, LANG);
    $cms_ids['service-prorperty-management'] = pll_get_post(6924, LANG);
    $cms_ids['service-monthly-services'] = pll_get_post(6891, LANG);
    $cms_ids['service-maintenance'] = pll_get_post(6900, LANG);

    //production taxonomy ids
    $cms_ids['tax-tres-mares'] = pll_get_term(417, LANG);
    $cms_ids['tax-shangri-la'] = pll_get_term(485, LANG);
    $cms_ids['tax-marina-tower'] = pll_get_term(415, LANG);
    $cms_ids['tax-nima-bay'] = pll_get_term(497, LANG);
    $cms_ids['tax-peninsula'] = pll_get_term(493, LANG);
}else{
    //development pages ids
    $cms_ids['home'] = pll_get_post(21, LANG);
    $cms_ids['about'] = pll_get_post(47, LANG);
    $cms_ids['tres-mares'] = pll_get_post(2902, LANG);
    $cms_ids['shangrila'] = pll_get_post(2904, LANG);
    $cms_ids['marina-tower'] = pll_get_post(2918, LANG);
    $cms_ids['azulejos'] = pll_get_post(4880, LANG);
    $cms_ids['vmarina'] = 5692; //not translated to ES
    $cms_ids['developments'] = pll_get_post(38, LANG);
    $cms_ids['favorites'] = pll_get_post(4969, LANG);
    $cms_ids['concierge'] = pll_get_post(4888, LANG);
    $cms_ids['service-prorperty-management'] = pll_get_post(6638, LANG);
    $cms_ids['service-monthly-services'] = pll_get_post(6643, LANG);
    $cms_ids['service-maintenance'] = pll_get_post(6608, LANG);

    //development taxonomy ids
    $cms_ids['tax-tres-mares'] = pll_get_term(417, LANG); 
    $cms_ids['tax-shangri-la'] = pll_get_term(411, LANG); 
    $cms_ids['tax-marina-tower'] = pll_get_term(415, LANG);
    $cms_ids['tax-nima-bay'] = pll_get_term(497, LANG);
    $cms_ids['tax-peninsula'] = pll_get_term(493, LANG);

}
