<?php

function get_property_info($setup_postdata){
    if($setup_postdata){
        $property = get_post();
        $property->listing_description = get_field('c_listing_description');
        $property->year_built = get_field('c_year_built');
        $property->beds = get_field('c_bedrooms');
        $property->baths = get_field('c_bathrooms');
        $property->parking = get_field('c_parking');
        $property->m2 = get_field('c_m2');
        $property->sqft = get_field('c_ft2');
        $property->type = get_field('c_type');
        $property->development = get_field('c_development');
        $property->location = get_field('c_location');
        $property->features = get_field('c_features');
        $property->gallery = get_field('c_gallery');
        $property->city = get_field('c_city');
        $property->address = get_field('c_address');
        $property->google_maps = get_field('c_google_maps');
        $property->price_usd = get_field('c_price');
        $property->price_mxn = get_field('c_price_mxn');
        $property->sold = get_field('c_property_sold');
        $property->mandarina = get_field('c_mandarina_property');
        return $property;
    }

    return false;
}

function get_property_info_query($setup_postdata, $item){
    if($setup_postdata){
        $property = $item;
        $property->listing_description = get_field('c_listing_description', $item->ID);
        $property->year_built = get_field('c_year_built', $item->ID);
        $property->beds = get_field('c_bedrooms', $item->ID);
        $property->baths = get_field('c_bathrooms', $item->ID);
        $property->parking = get_field('c_parking', $item->ID);
        $property->m2 = get_field('c_m2', $item->ID);
        $property->sqft = get_field('c_ft2', $item->ID);
        $property->type = get_field('c_type', $item->ID);
        $property->development = get_field('c_development', $item->ID);
        $property->location = get_field('c_location', $item->ID);
        $property->features = get_field('c_features', $item->ID);
        $property->gallery = get_field('c_gallery', $item->ID);
        $property->city = get_field('c_city', $item->ID);
        $property->address = get_field('c_address', $item->ID);
        $property->google_maps = get_field('c_google_maps', $item->ID);
        $property->price_usd = get_field('c_price', $item->ID);
        $property->price_mxn = get_field('c_price_mxn', $item->ID);
        $property->sold = get_field('c_property_sold', $item->ID);
        $property->mandarina = get_field('c_mandarina_property', $item->ID);
        return $property;
    }

    return false;
}

function get_property_pin($post_id){
    $term = wp_get_post_terms($post_id, 'property-type');
    $slug = 'default';
    if($term){ 
        switch($term[0]->slug){
            case 'barco':
            case 'boat':
                $slug = 'boat';
                break;
            case 'casa':
            case 'house':
                $slug = 'house';
                break;
            case 'comercial':
            case 'commercial':
                $slug = 'commercial';
                break;
            case 'condo-es':
            case 'condo':
                $slug = 'condo';
                break;
            case 'lote':
            case 'lot':
                $slug = 'lot';
                break;
            case 'villa':
            case 'villa-es':
                $slug = 'villa';
                break;
        }
    }
    return THEME_PATH . '/images/pins/' . $slug . '.png';
}

function clear_empty_taxonomy_by_type($tax_terms, $post_type){
    foreach($tax_terms as $v){
        $term_name = $v->slug;
        $term_id = $v->term_id;
        $custom_field_val = array(
            'property-type' => 'c_type',
            'development' => 'c_development',
            'location'=>'c_location'
        );
        $args = array(
            'posts_per_page' => 1,
            'post_type' => $post_type,
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => $custom_field_val[$v->taxonomy],
                    'value' => $term_id,
                    'compare' => '='
                )
            )
        );
        $query = new WP_Query($args);
        $types = array();
        if(count($query->posts) > 0){
            $types = $query->posts;
        }
        if(count($types)>0){
            $return_terms[] = $v;
        }

    }
    return $return_terms;
}

function get_taxonomy_list($taxonomy, $post_type = false){
    $term_args=array(
      'hide_empty' => true,
      'orderby' => 'name',
      'order' => 'ASC'
    );
    $tax_terms = get_terms($taxonomy, $term_args);

    if($post_type != false){
        $tax_terms = clear_empty_taxonomy_by_type($tax_terms, $post_type);    
    }

    $categories = array();
    $count = 0;
    foreach($tax_terms as $v){
        if($v->parent == 0){
            $categories[$count] = $v;

            $term_args=array(
              'hide_empty' => true,
              'orderby' => 'name',
              'order' => 'ASC',
              'child_of' => $v->term_id
            );
            $child_terms = get_terms($taxonomy,$term_args);
            $categories[$count]->childs = $child_terms;
        }
        $count++;
    }

    return $categories;
}

function is_active_menu($menu_url){
    return ($menu_url == SITE_DOMAIN . $_SERVER['REQUEST_URI']);
}

function img_metadata($img = false, $default = false, $default_type = '', $default_index = ''){
    $title = '';
    $alt = '';

    if($default != false){
        if(is_object($default) && isset($default->post_title)){ //post
            $title = $default->post_title;
            $alt = $default->post_title;
        }else if(is_object($default) && ($default->name)){ //term 
            $title = $default->name;
            $alt = $default->name;
        }else if(is_string($default)){
            $title = $default;
            $alt = $default;
        }
    }

    //overwrites img not empty name
    if($img !== false){
        if($img['title']){
            $title = $img['title'];
        }
        if($img['alt']){
            $alt = $img['alt'];
        }
    }
    

    //adds the type
    /*if($default_type != ''){
        $title .= " - " . $default_type;
        $alt .= " - " . $default_type;
    }*/

    //adds the index
    if($default_index != ''){
        $title .= " - " . $default_index;
        $alt .= " - " . $default_index;
    }

    $output = " title='".htmlspecialchars($title)."' ";
    $output .= " alt='".htmlspecialchars($alt)."' ";

    return $output;

}

function get_page_post_id(){
    $page_id = get_queried_object_id();
    $post = get_post($page_id);
    the_post();
}

function get_acf_data($post_id, $fields = array()){
    $data = array();
    foreach($fields as $field){
        $data[$field] = get_field($field, $post_id);
    }

    return $data;
}

function get_acf_fields($groups = false){
    $fields = array();
    if($groups != false){
        if(! is_array($groups) ){
            $groups = array($groups);
        }
        foreach($groups as $group){
            switch($group){
                case 'general_property_fields':
                    $group_id = 7;
                    break;
            }

            switch($group){
                case 'prices_for_sale':
                    $group_id = 9;
                    break;
            }

            switch($group){
                case 'prices_for_rentals':
                    $group_id = 10;
                    break;
            }
            
            $fields_array = apply_filters('acf/field_group/get_fields', array(), $group_id);
            foreach($fields_array as $v){
                $fields[] = $v['name'];
            }
        }
    }

    return $fields;
}

function get_substring($text, $length = 50){
    $text = trim($text);
    if(strlen($text) > $length){
        $text = substr($text, 0, $length);
        $text = substr($text, 0, strrpos($text, ' '));
        $text .= '...';
    }
    return $text;
}

function custom_search_by_keywords( $where, $wp_query ){
    global $wpdb;
    if ( $filter_post_title = $wp_query->get( 'filter_post_keywords' ) ) {
        $where .= ' AND '
                . '('
                . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $filter_post_title ) ) . '%\' '
                . ' OR '
                . $wpdb->posts . '.post_content LIKE \'%' . esc_sql( $wpdb->esc_like( $filter_post_title ) ) . '%\' '
                . ' OR '
                . $wpdb->posts . '.post_excerpt LIKE \'%' . esc_sql( $wpdb->esc_like( $filter_post_title ) ) . '%\' '
                . ')';
    }
    return $where;
}

function get_rental_pricing($post_id){
    $data = array();
    $data['has_month'] = false;
    $data['has_week'] = false;
    $data['has_night'] = false;
    $data['currency'] = get_field('c_rents_currency', $post_id);

    $loop = get_field('c_pricing_rent_range_loop', $post_id);
    $ranges = array();
    if($loop){
        foreach($loop as $count => $range){
            if($range['c_rents_season'] == 'custom'){
                $ranges[$count]['description'] = $range['c_rents_season_custom'];
            }else{
                $ranges[$count]['description'] = pll__(ucfirst($range['c_rents_season']));
            }

            $ranges[$count]['start'] = (isset($range['c_range_star_date'])) ? $range['c_range_star_date'] : '';
            $ranges[$count]['end'] = (isset($range['c_range_end_date'])) ? $range['c_range_end_date'] : '';

            if($range['c_range_month']){
                $data['has_month'] = true;
                $ranges[$count]['month'] = $range['c_range_month'];
            }
            if($range['c_range_week']){
                $data['has_week'] = true;
                $ranges[$count]['week'] = $range['c_range_week'];
            }
            if($range['c_range_night']){
                $data['has_night'] = true;
                $ranges[$count]['night'] = $range['c_range_night'];
            }
        }
    }
    
    $data['ranges'] = $ranges;
    if( !$data['has_month'] && !$data['has_week'] && !$data['has_night'] ){
        return false;
    }else{
        return $data;
    }
}

function get_readable_date($date){
    $date_unix = strtotime($date);

    $date = date("j, Y", $date_unix);
    $month = date('n', $date_unix);

    if(LANG == 'es'){
        $months = array(
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        );
        $month_str = $months[$month - 1];
    }else{
        $month_str = date('F', $date_unix);
    }

    return $month_str . ' ' . $date;
}

function get_property_price_reduced($post_id){
    $price = get_field('c_price', $post_id);
    $price_reduced = trim(get_field('c_price_reduced', $post_id));
    if(!empty($price_reduced) && $price < $price_reduced){
        return $price_reduced;
    }else{
        return false;
    }
}

function get_price_sale($price, $price_reduced){
    $current_price = 0;
    if (!empty($price_reduced)) {
        $current_price = $price_reduced;
    }else{
        $current_price = $price;
    }
    return $current_price;
}

function get_lower_price_rent($prices){
    $lower_price = 999999999999999;
    $type = '';
    foreach ($prices as $m) {
        if (!empty($m['c_range_night']) && $m['c_range_night'] < $lower_price) {
            $lower_price = $m['c_range_night'];
            $type = 'night';
        }else if (!empty($m['c_range_week']) && $m['c_range_week'] < $lower_price) {
            $lower_price = $m['c_range_week'];
            $type = 'week';
        }else if (!empty($m['c_range_month']) && $m['c_range_month'] < $lower_price) {
            $lower_price = $m['c_range_month'];
            $type = 'month';
        }
        
    }
    return array('price' => $lower_price, 'type' => $type);
}
