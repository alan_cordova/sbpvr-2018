<?php

function get_post_type_property(&$query){
    //prepares filter search values
    $filter_args = array(
        'tax_query'=>array(),
        'meta_query'=>array()
    );

    //override search with development ID sent through URL
    if(isset($_GET['devID'])){
        $_GET['f_development_tax_id'] = $_GET['devID'];
    }

    //search keywords
    if(isset($_GET['f_search'])){
        $s_str = trim($_GET['f_search']);
        if(strlen($s_str) >= 3){
            $query->set('filter_post_keywords', $s_str );
            add_filter('posts_where', 'custom_search_by_keywords', 10, 2);
            $keywords_filter = true;
        }
    }

    //price range price
    if(isset($_GET['f_price_range'])){
        $range = explode(',', trim($_GET['f_price_range']));
        if($range[0] != $range[1]){ //the bar range moved
            $filter_args['meta_query'] = array(
                'AND',
                array(
                    'key' => 'c_price',
                    'value' => $range[1],
                    'type' => 'numeric',
                    'compare' => '<='
                ),
                array(
                    'key' => 'c_price',
                    'value' => $range[0],
                    'type' => 'numeric',
                    'compare' => '>='
                )
            );
        }
    }

    //filter by Type
    $filter_types = array();
    if(isset($_GET['f_property_type_tax_id'])){
        $type_id = trim($_GET['f_property_type_tax_id']);
        if($type_id != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'property-type',
                'field'    => 'id',
                'terms'    => array($type_id)
            );
        }
    }

    //filter by Location
    $filter_location = array();
    if(isset($_GET['f_location_tax_id'])){
        $location = trim($_GET['f_location_tax_id']);
        if($location != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'location',
                'field'    => 'term_id',
                'terms'    => array($location)
            );
        }
    }

    //filter by Development
    $filter_development = array();
    if(isset($_GET['f_development_tax_id'])){
        $development = trim($_GET['f_development_tax_id']);
        if($development != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'development',
                'field'    => 'term_id',
                'terms'    => array($development)
            );
        }
    }

    
    $query->set( 'posts_per_page', 16 );
    $query->set( 'tax_query', $filter_args['tax_query']);
    $query->set( 'meta_query', $filter_args['meta_query']);

    $filter = (isset($_GET['f_order_filter'])) ? $_GET['f_order_filter'] : '';
    switch($filter){
        case 'price-desc':
            $meta_query['relation'] = 'AND';
            $meta_query['c_featured_property'] = array(
                'key' => 'c_featured_property',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $meta_query['c_price'] = array(
                'key' => 'c_price',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $query->set('meta_query', $meta_query);
            $query->set('orderby', array(
                'c_featured_property' => 'DESC',
                'c_price' => 'DESC'
            ));
        break;
        case 'alphabetic-asc':
            $query->set('orderby', 'post_title');
            $query->set('order', 'ASC');
        break;
        case 'alphabetic-desc':
            $query->set('orderby', 'post_title');
            $query->set('order', 'DESC');
        break;
        case 'recent':
            $query->set('orderby', 'post_date');
            $query->set('order', 'DESC');
            break;
        case 'oldest':
            $query->set('orderby', 'post_date');
            $query->set('order', 'ASC');
            break;
        case 'price-asc':
        default:
            $meta_query['relation'] = 'AND';
            $meta_query['c_featured_property'] = array(
                'key' => 'c_featured_property',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $meta_query['c_price'] = array(
                'key' => 'c_price',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $query->set('meta_query', $meta_query);
            $query->set('orderby', array(
                'c_featured_property' => 'DESC',
                'c_price' => 'DESC'
            ));
        break;
    }
    
}

function get_post_type_rent(&$query){
    //prepares filter search values
    $filter_args = array(
        'tax_query'=>array(),
        'meta_query'=>array()
    );

    //override search with development ID sent through URL
    if(isset($_GET['devID'])){
        $_GET['f_development_tax_id'] = $_GET['devID'];
    }

    //search keywords
    if(isset($_GET['f_search'])){
        $s_str = trim($_GET['f_search']);
        if(strlen($s_str) >= 3){
            $query->set('filter_post_keywords', $s_str );
            add_filter('posts_where', 'custom_search_by_keywords', 10, 2);
            $keywords_filter = true;
        }
    }

    //filter by Type
    $filter_types = array();
    if(isset($_GET['f_property_type_tax_id'])){
        $type_id = trim($_GET['f_property_type_tax_id']);
        if($type_id != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'property-type',
                'field'    => 'id',
                'terms'    => array($type_id)
            );
        }
    }

    //filter by Location
    $filter_location = array();
    if(isset($_GET['f_location_tax_id'])){
        $location = trim($_GET['f_location_tax_id']);
        if($location != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'location',
                'field'    => 'term_id',
                'terms'    => array($location)
            );
        }
    }

    //filter by Development
    $filter_development = array();
    if(isset($_GET['f_development_tax_id'])){
        $development = trim($_GET['f_development_tax_id']);
        if($development != ''){
            $filter_args['tax_query'][] = array(
                'taxonomy' => 'development',
                'field'    => 'term_id',
                'terms'    => array($development)
            );
        }
    }

    
    $query->set( 'posts_per_page', 16 );
    $query->set( 'tax_query', $filter_args['tax_query']);

    $filter = (isset($_GET['f_order_filter'])) ? $_GET['f_order_filter'] : '';
    switch($filter){
        case 'price-desc':
            $meta_query['c_featured_property'] = array(
                'key' => 'c_featured_property',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $meta_query['c_price_rentals_filter'] = array(
                'key' => 'c_price_rentals_filter',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $query->set('orderby', array(
                'c_featured_property' => 'DESC',
                'c_price_rentals_filter' => 'ASC'
            ));
            $query->set('meta_query', $meta_query);
        break;
        case 'alphabetic-asc':
            $query->set('orderby', 'post_title');
            $query->set('order', 'ASC');
        break;
        case 'alphabetic-desc':
            $query->set('orderby', 'post_title');
            $query->set('order', 'DESC');
        break;
        case 'recent':
            $query->set('orderby', 'post_date');
            $query->set('order', 'DESC');
            break;
        case 'oldest':
            $query->set('orderby', 'post_date');
            $query->set('order', 'ASC');
            break;
        case 'price-asc':
        default:
            $meta_query['relation'] = 'AND';
            $meta_query['c_featured_property'] = array(
                'key' => 'c_featured_property',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $meta_query['c_price_rentals_filter'] = array(
                'key' => 'c_price_rentals_filter',
                'type' => 'NUMERIC',
                'compare' => 'EXISTS'
            );
            $query->set('meta_query', $meta_query);
            $query->set('orderby', array(
                'c_featured_property' => 'DESC',
                'c_price_rentals_filter' => 'DESC'
            ));
        break;
    }

}

