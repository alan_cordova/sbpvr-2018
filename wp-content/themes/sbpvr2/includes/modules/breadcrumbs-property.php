<?php require_once __DIR__ .'/../helpers/dimox_breadcrumbs.php'; ?>
<?php $breadcrumbs = dimox_breadcrumbs();?>

<div class="breadcrum-section-black">
    <?php foreach($breadcrumbs as $k=>$b):?>
        <a href="<?=$b['url']?>" class="link-breadcrumbs-black w-inline-block">
            <div><?=$b['title']?></div>
        </a>
        <?php if(count($breadcrumbs) != ($k+1) ):?>
        <div class="v2-breadcrumb-separator-black">/</div>
        <?php endif;?>
    <?php endforeach;?>
</div>