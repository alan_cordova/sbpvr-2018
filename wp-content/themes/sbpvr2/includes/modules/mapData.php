<?php

$cacheFile = __DIR__ . '/mapCache/mapData_'.LANG.'.txt';

if(file_exists($cacheFile)){
    $response = file_get_contents($cacheFile);
}else{

    $filter_args['meta_query'] = array(
        'AND',
        array(
            'key' => 'c_price',
            'value' => $range[1],
            'type' => 'numeric',
            'compare' => '<='
        ),
        array(
            'key' => 'c_price',
            'value' => $range[0],
            'type' => 'numeric',
            'compare' => '>='
        )
    );

    $args = array(
        'post_type' => array('property', 'rent'),
        'posts_per_page' => -1,
        'orderby' => 'post_title',
        'order' => 'ASC',
    );
    $query = new WP_Query($args);

    $items = array();

    if(count($query->posts) > 0){

        //get all properties
        foreach($query->posts as $post){
            setup_postdata($post);
            
            //filter if sold
            if($post->post_type == 'property'){
                if(get_field('c_property_sold', $post->ID)){
                    continue; //if sold skip this property
                }
            }



            $gallery = get_field('c_gallery');
            $featuredImg = ($gallery) ? $gallery[0]['sizes']['gallery-250x120'] :  false; 

            $position = get_field('c_google_maps');
            if($position){
                if($post->post_type == 'property'){
                    $price = number_format(get_field('c_price', $post->ID), 0);
                }else{
                    $pricing = get_rental_pricing($post->ID);
                    if($pricing['has_night']){
                        $price = number_format($pricing['ranges'][0]['night'], 0);
                    }else if($pricing['has_week']){
                        $price = number_format($pricing['ranges'][0]['week'], 0);
                    }else if($pricing['has_month']){
                        $price = number_format($pricing['ranges'][0]['month'], 0);
                    }
                }
                if(isset($price)){
                    $items[] = array(
                        'permalink' => get_permalink($post->ID),
                        'title' => $post->post_title,
                        'description' => get_substring(get_field('c_listing_description'),100),
                        'img' => $featuredImg,
                        'position' => array(
                            'lat' => $position['lat'],
                            'lng' => $position['lng']
                        ),
                        'type' => $post->post_type,
                        'price' => $price
                    );
                }
            }
        }

        //get all developments
        $developments = array($cms_ids['marina-tower'], $cms_ids['azulejos'], $cms_ids['tres-mares'], $cms_ids['shangrila'], $cms_ids['vmarina']);
        foreach($developments as $development_id){
            $development = get_post($development_id);
            $gallery = get_field('c_development_gallery', $development->ID);
            $featuredImg = ($gallery) ? $gallery[0]['sizes']['gallery-250x120'] :  false; 
            $map = get_field('c_development_map', $development->ID);

            if($development_id == $cms_ids['vmarina']){
                if(LANG == 'es'){
                    $description = 'Su ubicación única en Marina Vallarta le permitirá a sus residentes la oportunidad de disfrutar de una amplia selección de amenidades como restaurantes , actividades de recreación y culturales, boutiques, spas y galerías de arte.';
                }else{
                    $description = 'Its privileged location in Marina Vallarta will allow residents easy access to a broad variety of entertainment options, including gourmet restaurants, ecreational and cultural activities, boutiques, spas and art galleries.';
                }
            }else{
                $description = get_field('c_development_introduction', $development->ID);
            }

            $items[] = array(
                'permalink' => get_permalink($development->ID),
                'title' => $development->post_title,
                'description' => get_substring($description, 100),
                'img' => $featuredImg,
                'position' => array(
                    'lat' => $map['lat'],
                    'lng' => $map['lng']
                ),
                'type' => 'development'
            );
        }

        //SB pin
        $img = THEME_PATH . '/images/sb2-bg-office.jpg';
        $about_post = get_post($cms_ids['about']);
        $description = strip_tags($about_post->post_content);
        $items[] = array(
            'permalink' => get_permalink($cms_ids['about']),
            'title' => 'Silva Brisset Realtors',
            'description' => get_substring($description, 100),
            'img' => $img,
            'position' => array(
                'lat' => 20.66434883,
                'lng' => -105.2523306
            ),
            'type' => 'permanent-sb'
        );
    }


    $file = fopen($cacheFile, 'w+');
    ftruncate($file, 0);
    $response = json_encode($items);
    fwrite($file , $response);
    fclose($file );
}




header('Content-Type: application/json');
echo $response;
exit;