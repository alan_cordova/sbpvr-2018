<?php
    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '7764') ){
            exit;
        }
        $body = '';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        $body .= "<b>".pll__('Message').":</b> " . nl2br(htmlspecialchars($_POST['f_message'])) . '<br>';
        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';

        $subject = 'SBPVR - Contact';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        if(LIVE_SITE){
            $to = htmlspecialchars($_POST['f_member']);
            wp_mail( $to, $subject, $body, $headers );
        }

        $to2 = 'alan.cordova@mexmags.com';
        wp_mail( $to2, $subject, $body, $headers );
        
        echo json_encode(array('success'=>true));
        exit;
    }
?>