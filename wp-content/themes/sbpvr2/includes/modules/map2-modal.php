<div class="map2-modal">
    <div class="map2-wrapper">
        <div class="map2-menu-wrapper">
            <div data-collapse="medium" data-animation="default" data-duration="400" class="map2-menu w-nav">
                <div class="map2-menu-container w-container">
                    <a href="#" class="map2-menu-brand w-nav-brand">
                        <img src="<?=THEME_PATH?>/images/map-logo.svg" alt="" class="map2-brand-img">
                    </a>
                    <nav role="navigation" class="map2-nav-menu w-nav-menu">
                        <div class="map2-search-form w-form">
                            <form action="" method="post" class="map2-search-form-inner map2-form">
                                <input type="text" class="map2-input regular w-hidden-medium w-hidden-small w-hidden-tiny w-input"
                                       name="search" placeholder="<?=pll__('Search')?>...">

                                <select name="status" class="map2-input select w-select map2-status-select capitalize"></select>

                                <select name="type" class="map2-input select w-select map2-type-select capitalize"
                                        data-default-option="<?=pll__('Type')?>">
                                </select>

                                <input type="text" class="map2-input short w-input" name="beds"
                                       placeholder="<?=pll__('Beds')?>">

                                <input type="text" class="map2-input short w-input" name="baths"
                                       placeholder="<?=pll__('Baths')?>">
                                <input type="text" class="map2-input short w-input" maxlength="256" name="min_price"
                                       placeholder="<?=pll__('Min Price')?>">
                                <input type="text" class="map2-input short w-input" name="max_price"
                                       placeholder="<?=pll__('Max Price')?>" >
                                <input type="submit" value="<?=pll__('Go')?>" class="map2-button w-button">
                            </form>
                        </div>
                    </nav>
                    <div class="map2-menu-toggle w-nav-button">
                        <img src="<?=THEME_PATH?>/images/img-icon-search.png" alt="" class="map2-search-icon">
                    </div>
                    <img src="<?=THEME_PATH?>/images/map-icon-close.png" alt="" class="map2-close-icon">
                </div>
            </div>
        </div>

        <div class="map2-container">
            <div class="sbmap-main-wrapper" id="map2-google"></div>
        </div>

        <div class="map2-properties-wrapper">
            <div class="map2-properties-inner">
            </div>
        </div>
    </div>
</div>


<div id="map2-item-template">
    <div class="map2-property-item">
        <div class="map2-property-flex">
            <a href="#" target="_blank" class="map2-property-img-wrapper w-inline-block map2-tpl-img-url">
                <img src="<?=THEME_PATH?>/images/sample-500x400.png" alt="" class="map2-property-img map2-tpl-img">
            </a>
            <div class="map2-property-info">
                <a href="#" target="_blank" class="map2-property-title map2-tpl-title"></a>
                <div class="map2-details-wrapper">
                    <div class="map2-detail map2-tpl-beds">
                        <img src="<?=THEME_PATH?>/images/Path-45.png" alt="" class="map2-detail-icon">
                        <div class="map2-detail-number"></div>
                    </div>
                    <div class="map2-detail map2-tpl-baths">
                        <img src="<?=THEME_PATH?>/images/Path-46.png" alt="" class="map2-detail-icon">
                        <div class="map2-detail-number"></div>
                    </div>
                    <div class="map2-detail map2-tpl-parking">
                        <img src="<?=THEME_PATH?>/images/Group-15.png" alt="" class="map2-detail-icon">
                        <div class="map2-detail-number"></div>
                    </div>
                </div>
                <p class="map2-property-description map2-tpl-description"></p>
                <div class="map2-property-price">$<span class="map2-tpl-price"></span> usd</div>
                <a href="#" target="_blank" class="map2-property-link map2-tpl-more">See More</a>
            </div>
        </div>
    </div>
</div>
