<div class="sbmap-wrapper" id="searchMapWrapper">
    <div class="sbmap-search-wrapper">
        <div class="sbmap-search-inner">
            <a id="mapSearchSale" href="#" class="sbmap-search-btn w-button"><?=pll__('Properties for Sale')?></a>
            <a id="mapSearchRent" href="#" class="disabled sbmap-search-btn sbmap-search-btn-rent w-button"><?=pll__('Properties for Rent')?></a>
            <a id="mapSearchDev" href="#" class="disabled sbmap-search-btn sbmap-search-btn-devs w-button"><?=pll__('Developments')?></a>
            
            <a href="#" class="searchMapClose sbmap-close sbmap-close-mobile w-button w-hidden-main w-hidden-medium">X</a>
            <div class="sbmap-search-form w-clearfix w-hidden-small w-hidden-tiny" style="visibility: hidden;">
                <form id="email-form" name="email-form" data-name="Email Form" class="sbmap-search-form-inner">
                    <input type="text" class="sbmap-search-form-input w-input" maxlength="256" name="name" data-name="Name" placeholder="<?=pll__('Search')?>..." id="name">
                    <input type="submit" value="<?=pll__('Go!')?>" class="sbmap-search-form-submit w-button">
                </form>
            </div>
            <a href="#" class="searchMapClose sbmap-close w-button w-hidden-small w-hidden-tiny">X</a>
        </div>
    </div>
    <div class="sbmap-main-wrapper" id="searchMap"></div>
</div>