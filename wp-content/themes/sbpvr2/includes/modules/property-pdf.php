<?php

require_once __dir__ . '/../vendor/TCPDF/tcpdf.php';

$property = get_property_info(true);
$img_one = ($property->gallery) ? $property->gallery[0]['sizes']['gallery-1500x850'] :  false;
$img_two = ($property->gallery) ? $property->gallery[1]['sizes']['gallery-1500x850'] :  false; // verificar si existe

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//remove header and footer lines
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Silva Brisset Realtors');
$pdf->SetTitle($property->post_title);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(10, 10, 10, true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 5);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$raleway_bold = TCPDF_FONTS::addTTFfont(
    __dir__ . '/../vendor/TCPDF/fonts/raleway/Raleway-Bold.ttf', 
    'TrueTypeUnicode', 
    '', 
    96
);
$raleway_medium = TCPDF_FONTS::addTTFfont(
    __dir__ . '/../vendor/TCPDF/fonts/raleway/Raleway-Medium.ttf', 
    'TrueTypeUnicode', 
    '', 
    96
);
$raleway_regular = TCPDF_FONTS::addTTFfont(
    __dir__ . '/../vendor/TCPDF/fonts/raleway/Raleway-Regular.ttf', 
    'TrueTypeUnicode', 
    '', 
    96
);

// add a page
$pdf->AddPage('L', 'A4');


//add image
if($img_one){
    $pdf->SetXY(10, 10);
    $pdf->Image($img_one, '', '', 137, null);
}

if($img_two){
    $pdf->SetXY(150, 10);
    $pdf->Image($img_two, '', '', 137, null);
}

// property title
$pdf->SetXY(10, 95);
$pdf->SetFont($raleway_bold, '', 12);
$html = '<h1 style="font-weight:bold; color: #0C1218;">'.$property->post_title.'</h1>';
$pdf->writeHTML($html, false, false, true, false, '');

//features starting position
$x_position = 10;

// beds feature
if($property->beds){

    $pdf->SetXY($x_position, 107);
    $pdf->Image(__dir__ . '/../../images/icon-bed.png', '', '', 5, null);

    $x_position += 7 + strlen($property->beds) - 1;

    $pdf->SetXY($x_position, 107);
    $pdf->SetFont($raleway_regular, '', 9);
    $html = '<span style="color:#95989A;">'.$property->beds.'</span>';
    $pdf->writeHTML($html, true, false, true, false, '');

    $x_position += 10;
}
// beds feature
if($property->baths){

    $pdf->SetXY($x_position, 107);
    $pdf->Image(__dir__ . '/../../images/icon-bath.png', '', '', 6, null);

    $x_position += 7 + strlen($property->baths) - 1;

    $pdf->SetXY($x_position, 107);
    $pdf->SetFont($raleway_regular, '', 9);
    $html = '<span style="color:#95989A;">'.$property->baths.'</span>';
    $pdf->writeHTML($html, true, false, true, false, '');

    $x_position += 10;
}
// ft2 feature
if($property->sqft){

    $pdf->SetXY($x_position, 107);
    $pdf->Image(__dir__ . '/../../images/icon-rule.png', '', '', 6, null);

    $x_position += 9;

    $pdf->SetXY($x_position, 107);
    $pdf->SetFont($raleway_regular, '', 9);
    $html = '<span style="color:#95989A;">'.$property->sqft.' ft2</span>';
    $pdf->writeHTML($html, true, false, true, false, '');

    $len = strlen($property->sqft);

    $x_position += 14 + $len;
}
// m2 feature
if($property->m2){

    $pdf->SetXY($x_position, 107);
    $pdf->Image(__dir__ . '/../../images/icon-rule.png', '', '', 6, null);
    

    $x_position += 9;

    $pdf->SetXY($x_position, 107);
    $pdf->SetFont($raleway_regular, '', 9);
    $html = '<span style="color:#95989A;">'.$property->m2.' m2</span>';
    $pdf->writeHTML($html, true, false, true, false, '');

    $len = strlen($property->m2);

    $x_position += 14 + $len;
}
// parking feature
if($property->parking){

    $pdf->SetXY($x_position, 107);
    $pdf->Image(__dir__ . '/../../images/icon-car.png', '', '', 6, null);

    $x_position += 9;

    $pdf->SetXY($x_position, 107);
    $pdf->SetFont($raleway_regular, '', 9);
    $html = '<span style="color:#95989A;">'.$property->parking.'</span>';
    $pdf->writeHTML($html, true, false, true, false, '');
}

// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

//main content
$pdf->SetXY(10, 115);
$content = trim(strip_tags($property->post_content));
if(strlen($content)>=400){
    $content = substr($content, 0, 950);
    $content = substr($content, 0, strrpos($content, ' ')) . "...";
}

$html = '<div style="color:#0C1218;">'.nl2br($content).'</div>';
$pdf->writeHTMLCell(137, null, 10, 115, $html, 0, 2, 0, true, 'left');

//address
$y = $pdf->GetY() + 3;
$x = $pdf->GetX();
$pdf->SetDrawColor(51, 141, 208);
$pdf->Line(10, $y, $x, $y);

$y = $y + 4;
$pdf->SetFont($raleway_bold, '', 9);
$html = '<div style="font-weight:bold; color:#0C1218; text-align:center;">'.$property->address.'</div>';
$pdf->writeHTMLCell(137, null, 10, $y, $html, 0, 2, 0, true, 'left');

$y = $pdf->GetY() + 4;
$x = $pdf->GetX();
$pdf->SetDrawColor(51, 141, 208);
$pdf->Line(10, $y, $x, $y);


//logo
$pdf->SetXY(10, 188);
$pdf->Image(__dir__ . '/../../images/logo-silva-brisset-2017.jpg', '', '', 30, null);

//footer info
$x = $pdf->GetX();
$y = $pdf->GetY() + 5;
$html = '<div style="color:#0C1218;text-align: right;">info@sbpvr.com<br>(322) 221 0051</div>';
$pdf->writeHTMLCell(278, null, $x, $y, $html, 0, 2, 0, true, 'left');


if($property->features){
    //rectangle
    $height = 20;
    $max_height = 114;
    $total_features = 0;
    $limit_features = 9; // Always put a number less than the number you want.
    //$property->features = array_merge($property->features, $property->features);
    foreach($property->features as $f){
        $total_features++;
        $height += 6;

        if($height >= $max_height){
            break;
        }

        if($total_features >= $limit_features){
            break;
        }
    }

    $pdf->SetFillColor(235, 235, 235);
    $pdf->SetDrawColor(235, 235, 235);
    $border_style = array('all' => array('width' => 2, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0));
    $pdf->Rect(150, 95, 137, $height + 10, 'DF', null);

    if($property->post_type == 'property'){
        $price_x = 150;
        $price_y = 95 + $height;
    }

    $pdf->SetX(150);
    if($property->post_type == 'property'){
        $pdf->SetY(95);  
    }else{
        $pdf->SetY(100);  
    }

    for($i=0; $i <= $limit_features; $i++){
        $pdf->SetY($pdf->GetY() + 3);
        $y = $pdf->GetY();
        $html = '<div style="color:#0C1218;text-align: center;">'.$property->features[$i]->name.'</div>';
        $pdf->writeHTMLCell(137, null, 150, $y, $html, 0, 2, 0, true, 'center');    
    }
}else{
    $price_x =  150;
    $price_y = 95;
}


if($property->post_type == 'property'){
    $pdf->SetFillColor(22, 37, 52);
    $pdf->SetDrawColor(22, 37, 52);
    $border_style = array('all' => array('width' => 2, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'phase' => 0));
    $pdf->Rect($price_x, $price_y, 137, 13, 'DF', null);
    if($property->price_mxn){
        $price = '$' . number_format($property->c_price_mxn,2) . ' MXN';
    }else{
        $price = '$' . number_format($property->c_price,2) .  ' USD';
    }

    $html = '<div style="color:#ffffff;text-align: center; font-size: 18px; font-weight: bold;">'.$price.'</div>';
    $pdf->writeHTMLCell(137, null, $price_x, $price_y + 3, $html, 0, 2, 0, true, 'center');        
}/*else{
    $pricing = get_rental_pricing(get_the_ID());

    $offset_first = $price_x;
    $offset_second = $price_x;
    $offset_third = $price_x;

    if($pricing['has_night'] && $pricing['has_week'] && $pricing['has_month']){
        $offset_first = $price_x - 30;
        $offset_second = $price_x;
        $offset_third = $price_x + 30;
    }else if( ($pricing['has_night'] && $pricing['has_week']) || 
              ($pricing['has_night'] && $pricing['has_month']) ||
              ($pricing['has_week'] && $pricing['has_month'])){
        $offset_first = $price_x - 20;
        $offset_second = $price_x + 20;
        $offset_third = $price_x;
    }

    if($pricing['has_night']){
        $offset = $offset_first;
        unset($offset_first);

        $html = '<div style="color:#ffffff;text-align: center; font-size: 10px;">'.pll__('Nightly').'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 2, $html, 0, 2, 0, true, 'center'); 

        $price = number_format($pricing['ranges'][0]['night'],0) . ' ' . get_field('c_rents_currency');
        $html = '<div style="color:#ffffff;text-align: center; font-size: 15px;">$'.$price.'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 6, $html, 0, 2, 0, true, 'center');  
    }

    if($pricing['has_week']){
        if(isset($offset_first)){
            $offset = $offset_first;
            unset($offset_first);
        }else{
            $offset = $offset_second;
            unset($offset_second);
        }

        $html = '<div style="color:#ffffff;text-align: center; font-size: 10px;">'.pll__('Weekly').'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 2, $html, 0, 2, 0, true, 'center'); 

        $price = number_format($pricing['ranges'][0]['week'],0) . ' ' . get_field('c_rents_currency');
        $html = '<div style="color:#ffffff;text-align: center; font-size: 15px;">$'.$price.'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 6, $html, 0, 2, 0, true, 'center');  
    }

    if($pricing['has_month']){
        if(isset($offset_first)){
            $offset = $offset_first;
            unset($offset_first);
        }else if(isset($offset_second)){
            $offset = $offset_second;
            unset($offset_second);
        }else{
            $offset = $offset_third;
        }

        $html = '<div style="color:#ffffff;text-align: center; font-size: 10px; font-weight: bold;">'.pll__('Monthly').'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 2, $html, 0, 2, 0, true, 'center'); 

        $price = number_format($pricing['ranges'][0]['month'],0) . ' ' . get_field('c_rents_currency');
        $html = '<div style="color:#ffffff;text-align: center; font-size: 15px; font-weight: bold;">$'.$price.'</div>';
        $pdf->writeHTMLCell(85, null, $offset, $price_y + 6, $html, 0, 2, 0, true, 'center');  
    }
}*/

//print the pdf
$pdf->Output();


