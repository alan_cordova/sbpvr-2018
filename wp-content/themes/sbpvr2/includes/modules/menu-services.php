<?php  
    $menu = c_prepare_menu_items(wp_get_nav_menu_items('menu '.$typeMenu.' '. LANG));
    $language = c_prepare_menu_items(wp_get_nav_menu_items('Language'));
?>
<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <div class="navbar-container w-container" style="height: auto;">
        <a href="#intro" class="navbar-brand w-nav-brand">
            <img src="<?=$logo?>" class="brand-img" alt="">
        </a>
        
        <nav role="navigation" class="nav-menu w-nav-menu"><!--
            --><?php foreach($menu as $k =>$m): ?><!--
                --><a href="<?=$m->url?>" class="nav-link w-nav-link"><?=$m->title?></a><!--
            --><?php endforeach;?>
        </nav>
      
        <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
        </div>
    </div>
</div>
