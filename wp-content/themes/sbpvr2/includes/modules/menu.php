<?php  
    $menu      = c_prepare_menu_items(wp_get_nav_menu_items('main menu ' . LANG));
    $language  = c_prepare_menu_items(wp_get_nav_menu_items('Language'));
    $email     = get_field('c_global_email', 'option');
    $be        = get_field('c_global_be', 'option');
    $instagram = get_field('c_global_instagram', 'option');
    $facebook  = get_field('c_global_facebook', 'option');
?>
<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <div class="container-13 w-container">
        <?php if ( !is_home() && ! is_front_page() ){ ?>
            <div class="btn-custom-menu w-hidden-medium w-hidden-small w-hidden-tiny" data-ix="menu-transform">
                <div class="top-line"></div>
                <div class="middle-line"></div>
                <div class="bottom-line"></div>
            </div>
        <?php } ?>
        <a href="<?=pll_home_url()?>" class="brand w-nav-brand">
            <img src="<?=THEME_PATH?>/images/sb-logo-blanco.svg" height="45" class="image-6">
        </a>
        <nav role="navigation" class="nav-menu-2 w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-nav-menu">
        <?php foreach($menu as $k =>$m): ?>
            <?php 
                $childs = (count($m->menu_childs) > 0) ? true : false;
                $active = is_active_menu($m->url);
                $link_childs = ($childs)?"href='#'":"href='".$m->url."'";
            ?>
            <div data-delay="0" class="v2-dropdown-item-menu w-dropdown">
                <a class="nav-link-2 w-nav-link w-dropdown-toggle <?=$active ? 'w--current':''?>" <?=$link_childs?>>
                    <div><em class="italic-text-5"><?=$m->title?></em></div>
                    <?=($childs)?'<div class="dropdown-icon w-icon-dropdown-toggle"></div>':''?>
                </a>
                <?php if($childs):?>
                    <nav class="dropdown-list w-dropdown-list">
                        <?php foreach($m->menu_childs as $k2 => $m2): ?>
                            <a class="v2-item-submenu w-dropdown-link <?=is_active_menu($m2->url) ? 'w--current':''?>" href="<?=$m2->url?>" <?=$cms_ids['vmarina']==$m2->object_id ? 'target="_blank"':''?> >
                                <?=$m2->title?>
                            </a>
                        <?php endforeach;?>
                    </nav>
                <?php endif;?>
            </div>
        <?php endforeach;?>
            <div class="div-block-19">
                <a href="mailto:<?=$email?>" class="nav-link-2 w-nav-link"><em class="italic-text-4"><i class="far fa-envelope-open"></i></em></a>
                <a href="<?=$be?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-behance"></i></em></a>
                <a href="<?=$instagram?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-instagram"></i></em></a>
                <a href="<?=$facebook?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-facebook-f"></i></em></a>
            </div>
            <?php foreach($language as $l):?>
                <a class="nav-link-3 w-nav-link" href="<?=$l->url?>">
                    <?=$l->title?>
                </a>
            <?php endforeach;?>
        </nav>
        <div class="menu-button w-hidden-medium w-nav-button" data-ix="menu-transform-movil">
            <div class="btn-custom-menu-movil div-block-20 w-hidden-medium">
                <div class="top-line"></div>
                <div class="middle-line"></div>
                <div class="bottom-line"></div>
            </div>
        </div>
        <div class="btn-social-lang w-hidden-small w-hidden-tiny">
            <a href="mailto:<?=$email?>" class="nav-link-2 w-nav-link"><em class="italic-text-4"><i class="far fa-envelope-open"></i></em></a>
            <a href="<?=$be?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-behance"></i></em></a>
            <a href="<?=$instagram?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-instagram"></i></em></a>
            <a href="<?=$facebook?>" class="nav-link-2 w-nav-link" target="_blank"><em class="italic-text-4"><i class="fab fa-facebook-f"></i></em></a>
            <?php foreach($language as $l):?>
                <a class="nav-link-2 w-nav-link" href="<?=$l->url?>">
                    <?=$l->title?>
                </a>
            <?php endforeach;?>
        </div>
        <?php if ( !is_home() && ! is_front_page() ){ ?>
            <div class="btn-custom-menu w-hidden-main w-hidden-small w-hidden-tiny" data-ix="menu-transform">
                <div class="top-line"></div>
                <div class="middle-line"></div>
                <div class="bottom-line"></div>
            </div>
        <?php } ?>
    </div>
</div>