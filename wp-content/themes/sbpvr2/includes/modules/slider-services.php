<div data-animation="slide" data-hide-arrows="1" data-duration="500" data-infinite="1" id="intro" class="slider w-slider">
    <div class="slider-intro-main-wrapper">
        <div class="slider-intro-wrapper">
            <div class="slider-intro-container">
                <div class="intro-bubbles-main-wrapper">
                    <div class="intro-bubbles-wrapper">
                        <div data-ix="bubble" class="intro-bubble red"></div>
                        <div data-ix="bubble-2" class="green intro-bubble"></div>
                        <div data-ix="bubble-3" class="intro-bubble orange"></div>
                    </div>
                </div>
                <div data-ix="bubble-end" class="text-block">
                    <?=pll__('Loyalty')?><br>
                    <?=pll__('Commitment')?><br>
                    <?=pll__('Warranty')?>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-mask w-slider-mask">
        <div class="slide w-slide">
            <img src="<?=$mainImage['sizes']['slider-services-2040x803']?>" class="slide-img" alt="<?=$mainImage['alt']?>" title="<?=$mainImage['title']?>">
        </div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
    </div>
    <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-round w-slider-nav"></div>
</div>