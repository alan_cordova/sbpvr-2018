<?php
    /*$search = mysqli_escape_string($con, $_POST['search-word']);
    $status = mysqli_escape_string($con, $_POST['status']);
    if ($status) {
        $status = mysqli_escape_string($con, $_POST['status']);
    }else{
        $status = 'sale,rent';
    }
    $type = mysqli_escape_string($con, $_POST['type']);
    if ($type) {
        $type = mysqli_escape_string($con, $_POST['type']);
    }else{
        $type = 'condo,house,commercial,lot,fractional,business,boat,villa';
    }
    $beds = mysqli_escape_string($con, $_POST['beds']);
    $baths = mysqli_escape_string($con, $_POST['baths']);
    $amenities = mysqli_escape_string($con, $_POST['amenities']);
    if ($amenities) {
        $amenities = mysqli_escape_string($con, $_POST['amenities']);
    }else{
        $amenities = 'handicap facilities,children allowed,pet friendly,security,furnished,elevator,pool,gym,maid services,tennis court,ocean view,beach access,beach front,internet,equiped kitchen,air conditioning,tv,jacuzzy,bar,terrace';
    }*/
    /*$query = "SELECT distinct pr.* FROM properties as pr 
    INNER JOIN status as st
    ON pr.status_id = st.id
    INNER JOIN types as ty
    ON pr.type_id = ty.id
    INNER JOIN property_has_features as prfe
    ON prfe.property_id = pr.id
    INNER JOIN features as fe
    ON prfe.feature_id = fe.id
    WHERE ";
    $status = explode(",", $status);
    $type = explode(",", $type);
    $amenities = explode(",", $amenities);
    $statusCount = 0;
    $typeCount = 0;
    $amenitiesCount = 0;

    if(is_array($status) && !empty($status)){
      foreach ($status as $v) {
          if ($statusCount > 0){
              $query .= "OR ";
          }
          else{
              $query .= "(";
          }
          $query .= "st.title = '$v' "; 
          ++$statusCount;
      }

      $query .= ") ";
    }

    if(is_array($type) && !empty($type)){
      foreach ($type as $k) {
          if ($typeCount > 0){
              $query .= "OR ";
          }else{
              $query .= "AND (";
          }
          $query .= "ty.title = '$k' "; 
          ++$typeCount;
      }

      $query .= ") ";
    }

    if(is_array($amenities) && !empty($amenities)){
      foreach ($amenities as $j) {
          if ($amenitiesCount > 0){
              $query .= "OR ";
          }else{
              $query .= "AND (";
          }
          $query .= "fe.title = '$j' "; 
          ++$amenitiesCount;
      }

      $query .= ")";
    }

    $query .= "
    AND pr.beds LIKE '%".$beds."' 
    AND pr.baths LIKE '%".$baths."'
    ";*/

    $query = "SELECT distinct * FROM properties";

    $res = $con->query($query);

    if (!$res) {
      trigger_error('Invalid query: ' . $con->error);
    }

    $items = array();

    if ($res->num_rows) {
        while ($row = $res->fetch_object()) {
            $main_image = json_decode($row->images);
            $description = get_substring($row->description, 150);

            $items[] = array(
                'permalink' => $row->permalink,
                'title' => $row->title,
                'description' => $description,
                'beds' => $row->beds,
                'baths' => $row->baths,
                'parking' => $row->parking,
                'img' => $main_image->image0,
                'position' => array(
                    'lat' => $row->position['lat'],
                    'lng' => $row->position['lng']
                ),
                'type' => 'development'
            );
        }
    }else{
          echo "No results";
    }
    
    $res->free();
    $con->close();

    $response = json_encode($items);

    header('Content-Type: application/json');
    echo $response;
?>