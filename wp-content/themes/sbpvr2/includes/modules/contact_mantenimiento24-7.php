<?php 
    
    require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
    

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['cmd']) && $_POST['cmd'] == '765476543a') ){
            exit;
        }
        $body = '';
        $body .= "<b>".pll__('Nombre').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Teléfono').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        $body .= "<b>".pll__('Petición especial').":</b> " . htmlspecialchars($_POST['f_message']) . '<br>';
        $body .= "--------------------------------------------------------- <br>";

        if(isset($_POST['checkbox']) && count($_POST['checkbox'])>0){
            $body .= "<h4>Servicios Solicitados</h4>";
            $body .= '<ul>';
            foreach($_POST['checkbox'] as $service){
                $body .= '<li>' . $service . '</li>';
            }
            $body .= '</ul>';
        }

        $subject = 'Mantenimiento 24/7 - SBPVR';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $headers[] = "From: " . $_POST['f_name'] . ' <' . $_POST['f_email'] . '>';
        $headers[] = "Cc: Tyan <tyan@sbpvr.com>";
        $headers[] = "Bcc: José Faro <jose@vallartalifestyles.com>";

        $to = get_field('c_global_email','option');
        wp_mail( $to, $subject, $body, $headers );
        

        //$to2 = 'jose@vallartalifestyles.com';
        //wp_mail( $to2, $subject, $body, $headers );
        
    }


    echo json_encode(array());
    exit;


