<?php require_once __DIR__ .'/../helpers/dimox_breadcrumbs.php'; ?>
<?php $breadcrumbs = dimox_breadcrumbs();?>

<div class="breadcrum-section-white">
    <?php foreach($breadcrumbs as $k=>$b):?>
        <?php if($b['title'] == 'member') {
            $title = 'meet our team';
        }else{
            $title = $b['title'];
        } ?>
        <a href="<?=$b['url']?>" class="link-breadcrumbs-white w-inline-block">
            <div><?=$title?></div>
        </a>
        <?php if(count($breadcrumbs) != ($k+1) ):?>
        <div class="v2-breadcrumb-separator-white">/</div>
        <?php endif;?>
    <?php endforeach;?>
</div>