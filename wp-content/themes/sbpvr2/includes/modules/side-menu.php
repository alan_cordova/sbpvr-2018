<?php  
    $menu      = c_prepare_menu_items(wp_get_nav_menu_items('main menu ' . LANG));
    $language  = c_prepare_menu_items(wp_get_nav_menu_items('Language'));
?>
<aside id="side-menu" class="side-menu" data-ix="ocultarmenu">
    <?php foreach($menu as $k =>$m): ?>
        <?php 
            $childs = (count($m->menu_childs) > 0) ? true : false;
            $active = is_active_menu($m->url);
            $urlDomain = SITE_DOMAIN . $_SERVER['REQUEST_URI'];
            if ( ( ( strpos($urlDomain, 'properties-for-sale') !== false ) && ( $m->post_name == 'properties-for-sale') ) || ( ( strpos($urlDomain, 'vacation-rentals') !== false ) && ( $m->post_name == 'vacation-rentals') ) || ( ( strpos($urlDomain, 'developments') !== false ) && ( $m->post_name == 4898) ) ) {
                $activeUrl = true;
            }else{
                $activeUrl = false;
            }
            $link_childs = ($childs)?"href='#'":"href='".$m->url."'";
        ?>
        <div data-delay="0" class="v2-dropdown-item-menu w-dropdown">
            <a class="v2-item-side-menu w-dropdown-toggle <?=$active ? 'w--current':''?> <?=$activeUrl ? 'w--current':''?>" <?=$link_childs?>>
                <div><?=$m->title?></div>
                <?=($childs)?'<div class="dropdown-icon w-icon-dropdown-toggle"></div>':''?>
            </a>
            <?php if($childs):?>
                <nav class="w-dropdown-list">
                    <?php foreach($m->menu_childs as $k2 => $m2): ?>
                        <a class="v2-item-submenu w-dropdown-link <?=is_active_menu($m2->url) ? 'w--current':''?>" href="<?=$m2->url?>" <?=$cms_ids['vmarina']==$m2->object_id ? 'target="_blank"':''?> >
                            <?=$m2->title?>
                        </a>
                    <?php endforeach;?>
                </nav>
            <?php endif;?>
        </div>
    <?php endforeach;?>
</aside>