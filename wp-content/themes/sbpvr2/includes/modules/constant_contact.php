<?php 
    
    require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
    require_once '../vendor/autoload.php';

    use Ctct\ConstantContact;
    use Ctct\Components\Contacts\Contact;
    use Ctct\Exceptions\CtctException;

    // Enter your Constant Contact APIKEY and ACCESS_TOKEN
    define("APIKEY", "9y5uv7r8bhcme3bk4znnrfnw");
    define("ACCESS_TOKEN", "c8f9bf62-b39d-4ab7-8a3c-af55c03bc5e7");
    define('LIST_ID','1542178260'); //SBPVR Website List
	

    // Response data prepared
    $res = array(
        'success' => true
    );

    $cc = new ConstantContact(APIKEY);

    $_POST['f_email'] = trim($_POST['f_email']);
    if(empty($_POST['f_email']) || $_POST['f_cmd'] != '9871'){
        $res['success'] = false;
    }else{
        // attempt to fetch lists in the account, catching any exceptions and printing the errors to screen
        try {
            
            // check to see if a contact with the email address already exists in the account
            $response = $cc->contactService->getContacts(ACCESS_TOKEN, array("email" => $_POST['f_email']));

            // create a new contact if one does not exist
            if (empty($response->results)) {
                $action = "Creating Contact";

                $contact = new Contact();
                $contact->addEmail($_POST['f_email']);
                $contact->addList(LIST_ID);

                /*
                 * The third parameter of addContact defaults to false, but if this were set to true it would tell Constant
                 * Contact that this action is being performed by the contact themselves, and gives the ability to
                 * opt contacts back in and trigger Welcome/Change-of-interest emails.
                 *
                 * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                 */
                $returnContact = $cc->contactService->addContact(ACCESS_TOKEN, $contact);

                

            } else {
                $action = "Updating Contact";

                $contact = $response->results[0];
                if ($contact instanceof Contact) {
                    $contact->addList(LIST_ID);

                    $returnContact = $cc->contactService->updateContact(ACCESS_TOKEN, $contact);
                } else {
                    $e = new CtctException();
                    $e->setErrors(array("type", "Contact type not returned"));
                    throw $e;
                }
            }

            //send notification of users suscribed to newsletter
            // update the existing contact if address already existed
            $body = '';
            $body .= "<p>New user suscribed to newsletter</p><br>";
            $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
            $body .= "--------------------------------------------------------- <br>";
            $body .= "<b>Website language:</b> ";
            $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';
            
            $subject = 'SBPVR - Newsletter';
            $headers = array('Content-Type: text/html; charset=UTF-8');

            if(LIVE_SITE){
                $to = get_field('c_global_email','option');
                wp_mail( $to, $subject, $body, $headers );
            }

            $to2 = 'alan.cordova@mexmags.com';
            wp_mail( $to2, $subject, $body, $headers );

        } catch (CtctException $ex) {
            $res['success'] = false;
        }
    }



    echo json_encode($res);
    exit;


