<?php
/*
 * WordPress Breadcrumbs
 * author: Dimox
 * version: 2015.09.14
 * license: MIT
*/
function dimox_breadcrumbs() {

	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page
	$text['page']     = 'Page %s'; // text 'Page N'
	$text['cpage']    = 'Comment Page %s'; // text 'Comment Page N'

	$wrap_before    = '<ol class="breadcrumb">'; // the opening wrapper tag
	$wrap_after     = '</ol><!-- .breadcrumbs -->'; // the closing wrapper tag
	$sep            = ''; // separator between crumbs
	$sep_before     = '<li class="sep">'; // tag before separator
	$sep_after      = '</li>'; // tag after separator
	$show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
	$show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$show_current   = 1; // 1 - show current page title, 0 - don't show
	$before         = '<li class="current">'; // tag before the current crumb
	$after          = '</li>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post;
	$home_link      = pll_home_url(LANG);
	$link_before    = '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
	$link_after     = '</span>';
	$link_attr      = ' itemprop="url"';
	$link_in_before = '<span itemprop="title">';
	$link_in_after  = '</span>';
	$link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
	$frontpage_id   = get_option('page_on_front');

	if(is_page()){
		$parent_id = $post->post_parent;
	}else{
		$parent_id = have_posts() ? $post->post_parent : 0;
	}
	$sep            = ' ' . $sep_before . $sep . $sep_after . ' ';

	$return_data = array();

	if (is_home() || is_front_page()) {

		if ($show_on_home) {
			//echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;
			$return_data[] = array(
				'url' => $home_link,
				'title' => $text['home']
			);
		}

	} else {

		//echo $wrap_before;
		if ($show_home_link) {
			//echo sprintf($link, $home_link, $text['home']);
			$return_data[] = array(
				'url' => $home_link,
				'title' => $text['home']
			);
		}

		if ( is_category() ) {
			$cat = get_category(get_query_var('cat'), false);
			$cat_tmp = array();

			if(count($cat)>0){
				$has_parent = true;
				while($has_parent){
					$cat_tmp[] = array(
						'url' => get_category_link($cat->term_id),
						'title' => $cat->name
					);

					if($cat->parent != 0){
						$cat = get_term($cat->parent, $cat->taxonomy);
					}else{
						$has_parent = false;
					}
				}

				$return_data = array_merge($return_data, array_reverse($cat_tmp));
			}

		} elseif ( is_search() ) {
			/*
			if (have_posts()) {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
			} else {
				if ($show_home_link) echo $sep;
				echo $before . sprintf($text['search'], get_search_query()) . $after;
			}
			*/
		} elseif ( is_day() ) {
			/*
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
			echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
			if ($show_current) echo $sep . $before . get_the_time('d') . $after;
			*/

		} elseif ( is_month() ) {
			/*
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
			if ($show_current) echo $sep . $before . get_the_time('F') . $after;
			*/
		} elseif ( is_year() ) {
			/*
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . get_the_time('Y') . $after;
			*/
		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type();
				$acf_field_type = 'c_global_'.$post_type.'_breadcrumb_'.LANG;
				$acf_field_page = get_field($acf_field_type, 'option');
				if($acf_field_page != false){
					$return_data[] = array(
						'url' => get_permalink($acf_field_page[0]->ID),
						'title' => $acf_field_page[0]->post_title
					);
				}

				/*
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($show_current) echo $sep . $before . get_the_title() . $after;
				*/
			} else {
				$cat_tmp = array();
				$cat = get_the_category(); 
				$cat = $cat[0];
				if(count($cat)>0){
					$has_parent = true;
					while($has_parent){
						$cat_tmp[] = array(
							'url' => get_category_link($cat->term_id),
							'title' => $cat->name
						);

						if($cat->parent != 0){
							$cat = get_term($cat->parent, $cat->taxonomy);
						}else{
							$has_parent = false;
						}
					}
					/* NO REGRESAR LA CATEGORIA */
					/*$return_data = array_merge($return_data, array_reverse($cat_tmp));*/
				}
				

				/*
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $sep);
				if (!$show_current || get_query_var('cpage')) 
					$cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
				if ( get_query_var('cpage') ) {
					echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
				} else {
					if ($show_current) echo $before . get_the_title() . $after;
				}
				*/
			}
			
			/*if ($show_home_link) echo $sep;
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($show_current) echo $sep . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $sep);
				if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
				
				if ( get_query_var('cpage') ) {
					echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
				} else {
					if ($show_current) echo $before . get_the_title() . $after;
				}
				*/
				if($show_current){
					$return_data[] = array(
						'url' => get_the_permalink(),
						'title' => get_the_title()
					);
				}
			/* } */
			
		}elseif ( is_attachment() ) {
			/*
			if ($show_home_link) echo $sep;
			$parent = get_post($parent_id);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			if ($cat) {
				$cats = get_category_parents($cat, TRUE, $sep);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
			}
			printf($link, get_permalink($parent), $parent->post_title);
			if ($show_current) echo $sep . $before . get_the_title() . $after;
			*/
		} elseif ( is_page() && !$parent_id ) {
			$return_data[] = array(
				'url' => get_the_permalink(),
				'title' => get_the_title()
			);
			//if ($show_current) echo $sep . $before . get_the_title() . $after;

		} elseif ( is_page() && $parent_id ) {
			if($parent_id != $frontpage_id){
				$page_tmp = array();
				while($parent_id){
					$page = get_page($parent_id);
					if ($parent_id != $frontpage_id) {
						$page_tmp[] = array(
							'url' => get_permalink($page->ID),
							'title' => get_the_title($page->ID)
						);
						//$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$return_data = array_merge($return_data, array_reverse($page_tmp));
			}

			
			if($show_current){
				$return_data[] = array(
					'url' => get_the_permalink(),
					'title' => get_the_title()
				);
			}

			/*
			if ($show_home_link) echo $sep;
			if ($parent_id != $frontpage_id) {
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					if ($parent_id != $frontpage_id) {
						$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ($i != count($breadcrumbs)-1) echo $sep;
				}
			}
			if ($show_current) echo $sep . $before . get_the_title() . $after;
			*/
		} elseif ( is_tag() ) {
			/*
			if ( get_query_var('paged') ) {
				$tag_id = get_queried_object_id();
				$tag = get_tag($tag_id);
				echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
			}
			*/
		} elseif ( is_author() ) {
            global $author;
            $author = get_userdata($author);
            $return_data[] = array(
                'url' => get_author_posts_url($author->ID),
                'title' => $author->display_name
            );
			/*
			global $author;
			$author = get_userdata($author);
			if ( get_query_var('paged') ) {
				if ($show_home_link) echo $sep;
				echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
			}
			*/
		} elseif ( is_404() ) {
			/*
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . $text['404'] . $after;
			*/
		} elseif ( has_post_format() && !is_singular() ) {
			/*
			if ($show_home_link) echo $sep;
			echo get_post_format_string( get_post_format() );
			*/
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            // custom post type
            /*
            $post_type = get_post_type_object(get_post_type());
            if ( get_query_var('paged') ) {
                echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $sep . $before . $post_type->label . $after;
            }
            */
        } 

		//echo $wrap_after;

	}

	return $return_data;
} // end of dimox_breadcrumbs()