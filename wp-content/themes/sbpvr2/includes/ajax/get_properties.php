<?php

require '../../../../../wp-load.php';

// WP_QUERY SALES

ini_set('display_errors', 1);
wp_cache_flush();

$properties = array();

$args = array(
    'posts_per_page' => 10,//-1
    'post_type' => 'property',
    'lang' => 'en,es',
    'post_status' => 'publish'
);


$query = new WP_Query($args);
if(count($query->posts) > 0){
    $n = 0;
    foreach($query->posts as $k=>$post){
        setup_postdata($post);

        $map = get_field('c_google_maps');
        $gallery = get_field('c_gallery');

        if($map && $gallery){
            

            $properties[$n] = array(
                'permalink' => get_permalink(),
                'title' => get_the_title(),
                'description' => get_substring($property->listing_description,150),

                'images' => $gallery[0]['sizes']['gallery-900x892'],

                'lat' => $map['lat'],
                'lng' => $map['lng'],
                'price' => get_field('c_price'),

                'lang' => pll_get_post_language(get_the_ID()),
                'property_id' => get_the_ID(),
                'update_at' => get_post_modified_time(),
            );

            

            $baths = get_field('c_bathrooms');
            if($baths){
                $properties[$n]['baths'] = $baths;              
            }

            $beds = get_field('c_bedrooms');
            if($beds){
                $properties[$n]['beds'] = $beds;              
            }

            $parking = get_field('c_parking');
            if($parking){
                $properties[$n]['parking'] = $baths;              
            }

            $properties[$n]['status_id'] = ($properties[$n]['lang'] == 'en') ? 1 : 2; //id according to api status table
            
            $location = get_field('c_location');
            if($location){
                $properties[$n]['location'] = $location->slug; //slug location              
            }

            $propertyType = get_field('c_type');
            if($propertyType){
                $properties[$n]['propertyType'] = $propertyType->slug; //slug property type "property, rent"              
            }
        }

        $n++;
        wp_reset_postdata();
    }
}


$args = array(
    'posts_per_page' => 10,//-1
    'post_type' => 'rent',
    'lang' => 'en,es',
    'post_status' => 'publish'
);
$query = new WP_Query($args);
if(count($query->posts) > 0){
    $n = 0;
    foreach($query->posts as $k=>$post){
        setup_postdata($post);

        $map = get_field('c_google_maps');
        $gallery = get_field('c_gallery');

        if($map && $gallery){

            $properties[$n] = array(
                'permalink' => get_permalink(),
                'title' => get_the_title(),
                'description' => get_substring($property->listing_description,150),

                'images' => $gallery[0]['sizes']['gallery-900x892'],

                'lat' => $map['lat'],
                'lng' => $map['lng'],


                'lang' => pll_get_post_language(get_the_ID()),
                'property_id' => get_the_ID(),
                'update_at' => get_post_modified_time(),
            );

            $lowerPriceData = get_lower_price_rent(get_field('c_pricing_rent_range_loop'));            
            $properties[$n]['price'] => $lowerPriceData['price'];
            $properties[$n]['price_type'] => $lowerPriceData['type'];

            $baths = get_field('c_bathrooms');
            if($baths){
                $properties[$n]['baths'] = $baths;              
            }

            $beds = get_field('c_bedrooms');
            if($beds){
                $properties[$n]['beds'] = $beds;              
            }

            $parking = get_field('c_parking');
            if($parking){
                $properties[$n]['parking'] = $baths;              
            }

            $properties[$n]['status_id'] = ($properties[$n]['lang'] == 'en') ? 3 : 4; //id according to api status table
            
            $location = get_field('c_location');
            if($location){
                $properties[$n]['location'] = $location->slug; //slug location              
            }

            $propertyType = get_field('c_type');
            if($propertyType){
                $properties[$n]['propertyType'] = $propertyType->slug; //slug property type "property, rent"              
            }
        }

        $n++;
        wp_reset_postdata();
    }
}


// END WP_QUERY SALES

// WP_QUERY RENTS

/*
$args_rents = array(
    'posts_per_page' => -1,//-1
    'post_type' => 'rent',
    'lang' => 'en,es',
    'post_status' => 'publish'
);

$query_rents = new WP_Query($args_rents);
$rents = array();
$acf_fields_rents = get_acf_fields(array('general_property_fields', 'prices_for_rentals'));

if(count($query_rents->posts) > 0){
    $rents = $query_rents->posts;
}

foreach($rents as $j=>$w){
    unset($w->post_content);
    $rents[$j]->acf = get_acf_data($w->ID, $acf_fields_rents);
    $rents[$j]->permalink = get_permalink($w->ID);
    $rents[$j]->lang_slug = pll_get_post_language($w->ID);
    $rents[$j]->price_property = get_price_rent($rents[$j]->acf['c_pricing_rent_range_loop']);
}*/

/*function clean_special_characters($s) {
    $s = preg_replace('/(\\\u00f1)+/m','á',$s);
    $s = preg_replace('/(\\\u00c1)+/m','Á',$s);
    $s = preg_replace('/(\\\u00e9)+/m','é',$s);
    $s = preg_replace('/(\\\u00c9)+/m','É',$s);
    $s = preg_replace('/(\\\u00ed)+/m','í',$s);
    $s = preg_replace('/(\\\u00CD)+/m','Í',$s);
    $s = preg_replace('/(\\\u00f3)+/m','ó',$s);
    $s = preg_replace('/(\\\u00d3)+/m','Ó',$s);
    $s = preg_replace('/(\\\u00fa)+/m','ú',$s);
    $s = preg_replace('/(\\\u00da)+/m','Ú',$s);
    $s = preg_replace('/(\\\u00fc)+/m','ü',$s);
    $s = preg_replace('/(\\\u00dc)+/m','Ü',$s);
    $s = preg_replace('/(\\\u00f1)+/m','ñ',$s);
    $s = preg_replace('/(\\\u00d1)+/m','Ñ',$s);
    $s = preg_replace('/(\\\n)+/m','',$s);
    $s = preg_replace('/(\\\r)+/m','',$s);
    return $s;
}*/



//$find = array('/(\\\n)+/m', '/(\\\r)+/m');

// END WP_QUERY RENTS
//echo preg_replace('/(\\\u0026quot;)+/m',"'",preg_replace($find,'', json_encode(array_merge($sales, $rents), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)));

exit;