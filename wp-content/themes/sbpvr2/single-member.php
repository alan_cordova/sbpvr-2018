<?php

    $imgMember        = get_field('c_image_member');
    $jobMember        = get_field('c_job_member');
    $emailMember      = get_field('c_email_member');

    $propertiesMember = get_posts(array(
        'post_type' => 'property',
        'meta_query' => array(
            array(
                'key' => 'c_agents_of_property', // name of custom field
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE'
            )
        )
    ));

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '7764') ){
            exit;
        }
        $body = '';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        $body .= "<b>".pll__('Message').":</b> " . nl2br(htmlspecialchars($_POST['f_message'])) . '<br>';
        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';

        $subject = 'SBPVR - Contact Property';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        if(LIVE_SITE){
            $to = $emailMember;
            wp_mail( $to, $subject, $body, $headers );
        }

        $to2 = 'alan.cordova@mexmags.com';
        wp_mail( $to2, $subject, $body, $headers );
        echo json_encode(array('success'=>true));
        exit;
    }

?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>

<div class="v2-modal-form-agent">
    <div class="v2-form-agent">
        <div class="btn-close-form-agent">
            <div class="top-line-mobil-agent"></div>
            <div class="bottom-line-mobil-agent"></div>
        </div>
        <h3><?=pll__('Ask This Agent')?></h3>
        <div class="v2-container-form-agent form-block-2">
            <form id="contact_form" action="<?=get_permalink()?>" class="form-2">
                <input type="hidden" name="f_cmd" class="cmd" value="" />
                <div class="div-block-9">
                    <input class="v2-contact-form-input-agent w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                    <input class="v2-contact-form-input-agent w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                </div>
                <input class="v2-contact-form-input-lg-agent w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                <textarea class="v2-contact-form-input-lg-agent w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                <input class="btn-black w-button" type="submit" value="<?=pll__('Send')?>">
            </form>
            <div class="w-form-done">
                <div><?=pll__('Contact success response')?></div>
            </div>
            <div class="w-form-fail">
                <div><?=pll__('Contact error response')?></div>
            </div>
        </div>
    </div>
</div>
<div class="result-section">
    <div class="side-search">
        <?php require_once 'includes/modules/breadcrumbs.php';?>
        <div class="agent-units-profile">
            <div class="slide-team-item-picture" style="background-image: url(<?=$imgMember['sizes']['img-member-300x300']?>)">
                <img src="<?=THEME_PATH?>/images/image-placeholder.svg" class="v2-img-profile">
            </div>
            <div class="slide-team-item-name"><?php the_title(); ?></strong></div>
            <div class="slide-team-item-contact"><?=$jobMember?> | <?=$emailMember?></div>
            <div class="v2-text-agent"><?php the_content(); ?></div>
            <a href="#" class="v2-btn-get-touch-agent w-button"><?=pll__('GET IN TOUCH')?></a>
        </div>
    </div>
    <div class="agent-units">
        <div class="v2-properties-movil-agent">
            <?php if($propertiesMember) { ?>
                <?php foreach($propertiesMember as $item) { ?>
                    <?php $property = get_property_info_query(true, $item);?>
                    <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['img-property-agent-330x250'] :  false; ?>
                    <a href="<?=get_permalink($item->ID)?>" class="v2-property-agent w-inline-block">
                        <div class="v2-information-hover-property">
                            <h3 class="v2-hover-title-property-list"><?=$property->post_title?></h3>
                            <?php if($property->location):?>
                                <h4 class="v2-hover-location-property-list"><?=$property->location->name?></h4>
                            <?php endif;?>
                            <div class="v2-hover-specs-property-list">
                                <?php if($property->beds):?>
                                    <div class="v2-hover-specs-numbers"><?=$property->beds?></div>
                                    <div class="v2-hover-specs"><?=pll__('Beds')?></div>
                                <?php endif;?>
                                <?php if($property->baths):?>
                                    <div class="v2-hover-specs-numbers"><?=$property->baths?></div>
                                    <div class="v2-hover-specs"><?=pll__('Baths')?></div>
                                <?php endif;?>
                            </div>
                            <div class="v2-hover-container-btn-more">
                                <div class="v2-hover-btn-more">
                                    <div class="v2-icon-more">+</div>
                                </div>
                            </div>
                        </div>
                        <div class="v2-img-property-list" style="background-image: url(<?=$featured?>)"> 
                            <img src="<?=THEME_PATH?>/images/img_default.png">
                        </div>
                        <div class="v2-information-property-text">
                            <h3 class="v2-title-property-list"><?=$property->post_title?></h3>
                            <?php if($property->location):?>
                                <h4 class="heading-8"><?=$property->location->name?></h4>
                            <?php endif;?>
                        </div>
                    </a>
                <?php } ?>
            <?php }else{ ?>
                <div class="v2-agent-properties-text">
                    <?=pll__('Properties Not Found')?>
                </div>
            <?php } ?>
        </div>
        <div class="agent-units-item">
            <?php if($propertiesMember) { ?>
                <?php foreach($propertiesMember as $item) { ?>
                    <?php $property = get_property_info_query(true, $item);?>
                    <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['img-property-agent-1350x280'] :  false; ?>
                    <div class="v2-item-unity-agent">
                        <div class="image-5" style="background-image: url(<?=$featured?>)">
                        </div>
                        <div class="rental-detail-title">
                            <h1 class="v2-title-property-agent"><?=$property->post_title?></h1>
                            <div class="v2-information-specs-btn-property">
                                <div class="v2-property-item-specs">
                                    <?php if($property->beds):?>
                                        <div class="specs-numbers"><?=$property->beds?></div>
                                        <div class="specs"><?=pll__('Beds')?></div>
                                    <?php endif;?>
                                    <?php if($property->baths):?>
                                        <div class="specs-numbers"><?=$property->baths?></div>
                                        <div class="specs"><?=pll__('Baths')?></div>
                                    <?php endif;?>
                                    <?php if($property->parking):?>
                                        <div class="specs-numbers"><?=$property->parking?></div>
                                        <div class="specs"><?=pll__('Parking Space')?></div>
                                    <?php endif;?>
                                    <?php if($property->sqft):?>
                                        <div class="specs-numbers"><?=number_format($property->sqft)?></div>
                                        <div class="specs">sq ft</div>
                                    <?php endif;?>
                                    <?php if($property->m2):?>
                                        <div class="specs-numbers"><?=number_format($property->m2)?></div>
                                        <div class="specs">sq m</div>
                                    <?php endif;?>
                                    </div>
                                <div class="check-availability">
                                    <a href="<?=get_permalink($item->ID)?>" class="btn-more w-button"><em class="italic-text-2"></em></a>
                                </div>
                            </div>
                        </div>
                        <p class="v2-description-item-agent"><?=$property->listing_description?></p>
                    </div>
                <?php } ?>
            <?php }else{ ?>
                <div class="v2-agent-properties-text">
                    <?=pll__('Properties Not Found')?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php get_footer();?>