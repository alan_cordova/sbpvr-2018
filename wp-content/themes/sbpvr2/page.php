<?php
    the_post();
?>
<?php global $body_padding; $body_padding = true; ?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>

<div class="section">
    <div class="container">
        <div class="page-container">
            <h1><?php the_title()?></h1>
            
            <div class="dinamic-container">
                <?php the_content()?>
            </div>
        </div>
    </div>
</div>


<?php get_footer();?>