<?php
/*
Template Name: MLS Flex
*/
?>
<?php
    the_post();

    $mlsAreas = array('Aramara', 'Bucerias', 'Centro South', 'Centro North', 'Flamingos', 'Francisco Villa East', 'Francisco Villa West', 'Hotel Zone', 'Jarretaderas', 'La Cruz de Huanacaxtle', 'Marina', 'Nuevo Vallarta East', 'Nuevo Vallarta West', 'Riviera Nayarit', 'Sayulita', 'South Shore', 'Tomatlan');

    $mlsTypes = array(
        'A' => 'Condos',
        'B' => 'Houses',
        'E' => 'Land',
        'F' => 'Commercial',
        'G' => 'Business',
        'H' => 'Fractional'
    );

    $PropertyType = isset($_GET['PropertyType']) ? htmlspecialchars($_GET['PropertyType']) : '';
    $MLSAreaMinor = isset($_GET['MLSAreaMinor']) ? htmlspecialchars($_GET['MLSAreaMinor']) : '';
    $MinPrice = isset($_GET['MinPrice']) ? htmlspecialchars($_GET['MinPrice']) : '';
    $MaxPrice = isset($_GET['MaxPrice']) ? htmlspecialchars($_GET['MaxPrice']) : '';
    $MaxBeds = isset($_GET['MaxBeds']) ? htmlspecialchars($_GET['MaxBeds']) : '';
    $MaxBaths = isset($_GET['MaxBaths']) ? htmlspecialchars($_GET['MaxBaths']) : '';
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>
<div class="sbflex-container">
    <div class="sbflex-list">
        <?php the_content();?>
    </div>
    <div class="sbflex-sidebar">

        <form class="sb2-mls-search-sidebar" action="/mls/search" method="GET">
            <h3><?=pll__('MLS Search')?></h3>
            <div class="sb2-cont-select-mls in-sidebar">
                <select id="property-type" name="PropertyType" class="sb2-select-mls w-select">
                    <option value="" selected><?= pll__('Type') ?></option>
                    <?php foreach($mlsTypes as $val => $type):?>
                        <option value="<?=$val?>" <?=$PropertyType == $val ? 'selected="selected"':''?> ><?=pll__($type)?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="sb2-cont-select-mls in-sidebar">
                <select id="property-type" name="MLSAreaMinor" class="sb2-select-mls w-select">
                    <option value="" selected><?= pll__('Location') ?></option>
                    <?php foreach($mlsAreas as $area):?>
                        <option value="<?=$area?>" <?=$MLSAreaMinor == $area ? 'selected="selected"':''?>  ><?=pll__($area)?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="sb2-cont-select-mls in-sidebar">
                <label for="min-price" class="sb2-hide-label"><?= pll__('Min Price') ?></label>
                <input type="text" class="sb2-input w-input" maxlength="256" name="MinPrice" value="<?=$MinPrice?>"
                       placeholder="<?= pll__('Min Price') ?>" id="min-price">
            </div>
            <div class="sb2-cont-select-mls in-sidebar">
                <label for="max-price" class="sb2-hide-label"><?= pll__('Max Price') ?></label>
                <input type="text" class="sb2-input w-input" maxlength="256" name="MaxPrice" value="<?=$MaxPrice?>"
                       placeholder="<?= pll__('Max Price') ?>" id="max-price">
            </div>
            <div class="sb2-cont-select-mls in-sidebar">
                <label for="max-price" class="sb2-hide-label"><?= pll__('Beds') ?></label>
                <input type="hidden" maxlength="256" name="MinBeds" id="mls-min-beds">
                <input type="text" class="sb2-input w-input" maxlength="256" name="MaxBeds" value="<?=$MaxBeds?>" placeholder="<?=pll__('Beds')?>" id="mls-max-beds">
            </div>
            <div class="sb2-cont-select-mls in-sidebar">
                <label for="location" class="sb2-hide-label"><?= pll__('Baths') ?></label>
                <input type="hidden" maxlength="256" name="MinBaths" id="mls-min-baths">
                <input type="text" class="sb2-input w-input" maxlength="256" name="MaxBaths" value="<?=$MaxBaths?>" placeholder="<?= pll__('Baths') ?>" id="mls-max-baths">
            </div>
            <input type="submit" value="<?= pll__('Search') ?>" class="sb2-search-mls in-sidebar w-button">
        </form>

    </div>
</div>
<?php get_footer();?>