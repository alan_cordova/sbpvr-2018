<?php
/*
Template Name: Services
*/
?>
<?php
    the_post();

    $pageID = get_queried_object_id();

    if ($pageID == $cms_ids['service-prorperty-management']) {
        $titleSubject = pll__('Service Management');
    }elseif ($pageID == $cms_ids['service-monthly-services']) {
        $titleSubject = pll__('Commercial Monthly Services');
    }else{
        $titleSubject = pll__('Service Maintenance');
    }

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['cmd']) && $_POST['cmd'] == '765476543a') ){
            exit;
        }
        $body = '';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        $body .= "<b>".pll__('Special Request').":</b> " . htmlspecialchars($_POST['f_message']) . '<br>';
        $body .= "--------------------------------------------------------- <br>";

        if(isset($_POST['checkbox']) && count($_POST['checkbox'])>0){
            $body .= "<h4>".pll__('Requested Services')."</h4>";
            $body .= '<ul>';
            foreach($_POST['checkbox'] as $service){
                $body .= '<li>' . $service . '</li>';
            }
            $body .= '</ul>';
        }

        $subject = $titleSubject.' - SBPVR';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $headers[] = "From: " . $_POST['f_name'] . ' <' . $_POST['f_email'] . '>';
        //$headers[] = "Cc: Tyan <tyan@sbpvr.com>";
        //$headers[] = "Bcc: José Faro <jose@vallartalifestyles.com>";

        $to = 'alan.cordova@mexmags.com'; //get_field('c_global_email','option');
        wp_mail( $to, $subject, $body, $headers );

        echo json_encode(array());
        exit;
        
    }

    $logo = get_field('c_logo');
    $mainImage = get_field('c_main_image');
    $iconServices = get_field('c_icon_services');
    $iconServicesSmallRight = get_field('c_icon_services_small_right');

    $mainTextFirst = get_field('c_main_text_first');
    $mainTextLast = get_field('c_main_text_last');

    $imgServices = get_field('c_image_services');
    $services = get_field('c_services');

    $clients = get_field('c_clients');

    $mainTextContact = get_field('c_main_text_contact');
    $subTextContact = get_field('c_sub_text_contact');

    $interestLeft = get_field('c_column_left_interests');
    $interestRight = get_field('c_column_right_interests');

    $typeMenu = get_field('c_type_of_menu');

    $i = 0;
    $j = 0;
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu-services.php';?>
<?php require_once 'includes/modules/slider-services.php';?>
<?php if($mainTextFirst || $mainTextLast) { ?>
    <div id="our" class="intro-wrapper">
        <div class="intro-inner">
            <?php if($iconServices) { ?>
                <img src="<?=$iconServices?>" class="intro-left">
            <?php } ?>
            <div class="intro-right" style="<?=(!$iconServices)?'padding-left: 0':''?>">
                <?php if($mainTextFirst) { ?>
                    <div class="intro-text" style="<?=(!$iconServices)?'text-align: center;':''?>"><?=$mainTextFirst?></div>
                <?php } ?>
                <?php if($mainTextLast) { ?>
                    <div class="intro-text last" style="<?=(!$iconServices)?'text-align: center;':''?>"><?=$mainTextLast?></div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<?php if($services) { ?>
    <div id="services" class="services-wrapper">
        <img src="<?=$imgServices['sizes']['img-services-one-1024x690']?>" class="services-img" alt="<?=$imgServices['alt']?>" title="<?=$imgServices['title']?>">
        <div class="services-right">
            <div class="services-right-wrapper">
                <div class="services-right-inner">
                    <div class="services-title-wrapper">
                        <h1 class="service-title" style="background-image: url(<?=$iconServicesSmallRight?>)"><?=pll__('Our Services')?></h1>
                    </div>
                    <div class="services-list-wrapper">
                        <ul class="services-list w-clearfix">
                            <?php foreach($services as $s) { ?>
                                <li class="services-list-item">
                                    <div class="services-list-text"><?=$s['c_title_service']?></div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if($clients) { ?>
    <div id="clients" class="clients-wrapper">
        <div class="clients-inner">
            <h2 class="clients-title"><?=pll__('Some of our customers')?></h2>
            <div class="clients-list">
                <?php foreach($clients as $c) { ?>
                    <a href="<?=$c['c_link_client']?>" class="client-item w-inline-block" target="_blank">
                        <img src="<?=$c['c_image_client']['sizes']['img-client-400x400']?>" class="client-img" alt="<?=$c['c_image_client']['alt']?>" title="<?=$c['c_image_client']['title']?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<div id="contact" class="contact">
    <h2 class="clients-slogan"><?=$mainTextContact?></h2>
    <p class="clients-slogan-2"><?=$subTextContact?></p>
    <div class="form-wrapper">
        <form action="" id="contact_form_service" method="post" class="form">
            <div class="input-wrapper">
                <input type="text" class="input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>:" required="">
                <div class="input-separator"></div>
                <input type="text" placeholder="<?=pll__('Phone')?>: " maxlength="256" name="f_phone" required="" class="input w-input">
            </div>
            <input type="email" class="input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>: " required="">
            <?php if($interestLeft && $interestRight) { ?>
                <h2 class="im-interest-call"><?=pll__('I am interested')?>:</h2>
                <div class="checkbox-list-outter-wrapper">
                    <div class="checkbox-list-wrapper">
                        <div class="checkbox-list-left">
                            <?php 
                                foreach($interestLeft as $il) {
                                $i++; 
                            ?>
                                <div class="checkbox-list-item w-checkbox">
                                    <input type="checkbox" id="checkbox-l<?=$i?>" name="checkbox[]" class="checkbox-input w-checkbox-input" value="<?=$il['c_title_interest']?>">
                                    <label for="checkbox-l<?=$i?>" class="w-form-label"><?=$il['c_title_interest']?></label>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="checkbox-list-right">
                            <?php
                                foreach($interestRight as $ir) {
                                $j++; 
                            ?>
                                <div class="checkbox-list-item w-checkbox">
                                    <input type="checkbox" id="checkbox-r<?=$j?>" name="checkbox[]" class="checkbox-input w-checkbox-input" value="<?=$ir['c_title_interest']?>">
                                    <label for="checkbox-r<?=$j?>" class="w-form-label"><?=$ir['c_title_interest']?></label>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <label for="field" class="label"><strong><?=pll__('Let us know if you have a special request')?>:</strong></label>
            <textarea name="f_message" maxlength="5000" class="input textarea w-input"></textarea>
            <input type="submit" value="<?=pll__('Send')?>" class="submit w-button">
        </form>
        <div class="w-form-done">
            <?=pll__('Your request')?>
        </div>
    </div>
</div>

<?php get_footer();?>