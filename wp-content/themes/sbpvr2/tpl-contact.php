<?php
/*
Template Name: Contact
*/
?>
<?php
    the_post();

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '7764') ){
            exit;
        }
        $body = '';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        $body .= "<b>".pll__('Message').":</b> " . nl2br(htmlspecialchars($_POST['f_message'])) . '<br>';
        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';

        $subject = 'SBPVR - Contact';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        if(LIVE_SITE){
            $to = get_field('c_global_email','option');
            wp_mail( $to, $subject, $body, $headers );
        }

        $to2 = 'jose@vallartalifestyles.com';
        wp_mail( $to2, $subject, $body, $headers );
        
        echo json_encode(array('success'=>true));
        exit;
    }
?>
<?php global $body_padding; $body_padding = true; ?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>

<div class="section">
    <div class="container">
        <div class="contact-container">
            <div class="contact-col">
                <div class="contact-main-container">
                    <h1 class="no-visible"><?php the_title()?></h1>
                    <h2 class="heading-1 heading-small"><?=pll__('Get in touch')?>:</h2>
                    
                    <?php the_content()?>

                    <p class="contact-address-content">
                        Paseo de la Marina 355-20, Marina Vallarta
                        <br>Puerto Vallarta, Jalisco, México <?=pll__('On the right of the lighthouse');?>
                    </p>
                    <p class="contact-mail-container">
                        <b><?=eeb_email(get_field('c_global_email', 'option'))?></b>
                        | 
                        <a href="tel:<?=get_field('c_global_phone', 'option')?>" class="link">
                            <?=get_field('c_global_phone', 'option')?>
                        </a>
                    </p>
                </div>
            </div>
            <div class="contact-col">
                <div class="contact-form-wrapper form-wrapper">
                    <form id="contact_form" action="<?=get_permalink()?>">
                        <input class="input input-half w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                        <input type="text" name="f_cmd" class="cmd" value="" />
                        <input class="input input-half right w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                        <input class="input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                        <textarea class="contact-textarea textarea w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                        <div class="contact-btn-container w-clearfix">
                            <input class="btn-contact button lowercase w-button" type="submit" value="<?=pll__('Send')?>">
                        </div>
                    </form>
                    <div class="w-form-done">
                        <div><?=pll__('Contact success response')?></div>
                    </div>
                    <div class="w-form-fail">
                        <div><?=pll__('Contact error response')?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $map = get_field('c_global_address_location', 'option');?>
<div class="map-container map-block" 
    data-lat="<?=$map['lat']?>" 
    data-lng="<?=$map['lng']?>" 
    data-marker="<?=THEME_PATH?>/images/icon-location-SB.png" 
    data-marker-title="Silva Brisset Realtors" 
    id="map-container">
</div>

<?php get_footer();?>