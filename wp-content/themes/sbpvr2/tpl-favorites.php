<?php
/*
Template Name: Favorites
*/
?>
<?php
    the_post();

?>
<?php global $body_padding; $body_padding = true; ?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
    
<div class="property-container">
    <div class="container">
        <div class="favorites-container">
            <?php require_once 'includes/modules/breadcrumbs.php'; ?>
            <h1><?php the_title()?></h1>
            
            <div class="dinamic-container justify">
                <?php the_content()?>
            </div>
        </div>
        
        <?php $cookies = (isset($_COOKIE['favsArr'])) ? $_COOKIE['favsArr'] : '';?>
        <?php 
            $cookies = json_decode($cookies, 1);
            if(is_array($cookies) && count($cookies)>0){
                $args = array(
                    'post_type' => array('property', 'rent'),
                    'posts_per_page' => -1,
                    'post__in' => $cookies
                );
                $query = new WP_Query($args);
                $posts = $query->posts;
            }else{
                $posts = false;
            }
        ?>
        <?php if(isset($posts) && is_array($posts) && count($posts)>0):?>
            <div class="favorites-grid-container">
            <?php foreach($posts as $post):?>
                <?php setup_postdata($post);?>
                <?php $property = get_property_info(true);?>
                <?php if($post->post_type=='rent'):?>
                    <?php $pricing = get_rental_pricing($post->ID);?>
                <?php endif;?>
                <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['gallery-850x500'] :  false; ?>
                <?php $featured = ($featured) ? 'style="background-image:url('.$featured.');"' : ''; ?>
                <div class="property-grid-item-container favorites-grid">
                    <a class="property-grid-item w-inline-block" target="_blank" href="<?=get_permalink()?>" <?=$featured?> >
                        <div class="property-grid-content-title w-hidden-small w-hidden-tiny">
                            <h2 class="property-grid-title"><?php the_title()?></h2>
                            <?php if($property->location):?>
                                <div class="property-grid-location"><?=$property->location->name?></div>
                            <?php endif;?>
                            <div class="property-grid-price w-hidden-main">
                                <?php if($post->post_type == 'property'):?>
                                    <?php if($property->price_mxn):?>
                                        $ <?=number_format($property->c_price_mxn,2)?> <span class="currency-small">MXN</span>
                                    <?php else:?>
                                        $ <?=number_format($property->c_price,2)?> <span class="currency-small">USD</span>
                                    <?php endif;?>
                                <?php endif;?>
                                <?php if($post->post_type == 'rent'):?>
                                    <?php if($pricing):?>
                                        <?php $has_one = false;?>
                                        <?php $has_two = false;?>
                                        <?php if($pricing['has_night']):?>
                                            $ <?=number_format($pricing['ranges'][0]['night'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Nightly')?></span>
                                            <?php $has_one = true; ?>
                                        <?php endif;?>
                                        <?php if($pricing['has_week']):?>
                                            <?php if($has_one):?>
                                                <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <?php endif;?>
                                            $ <?=number_format($pricing['ranges'][0]['week'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Weekly')?></span>
                                            <?php $has_one = true; ?>
                                            <?php $has_two = true; ?>
                                        <?php endif;?>
                                        <?php if($pricing['has_month'] && !$has_two):?>
                                            <?php if($has_one):?>
                                                <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <?php endif;?>
                                            $ <?=number_format($pricing['ranges'][0]['month'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Monthly')?></span>
                                        <?php endif;?>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="property-grid-content-hover w-hidden-medium w-hidden-small w-hidden-tiny">
                            <p class="property-grid-content-text">
                                <?=get_substring($property->listing_description,150)?>
                            </p>
                            <div class="property-grid-details">
                                <?php if($property->beds):?>
                                    <div class="property-grid-detail">
                                        <div class="property-detail-quantity"><?=$property->beds?></div>
                                        <img class="property-icon-detail" src="<?=THEME_PATH?>/images/icon-bed-white.svg" alt="">
                                    </div>
                                <?php endif;?>
                                <?php if($property->baths):?>
                                    <div class="property-grid-detail">
                                        <div class="property-detail-quantity"><?=$property->baths?></div>
                                        <img class="property-icon-detail" src="<?=THEME_PATH?>/images/icon-bath-white.svg" alt="">
                                    </div>
                                <?php endif;?>
                                <?php if($property->sqft):?>
                                    <div class="property-grid-detail">
                                        <div class="property-detail-quantity"><?=number_format($property->sqft)?> sq ft</div>
                                        <img class="property-icon-detail" src="<?=THEME_PATH?>/images/icon-size-white.svg" alt="">
                                    </div>
                                <?php endif;?>
                            </div>
                            <div class="property-grid-price">
                                <?php if($post->post_type == 'property'):?>
                                    <?php if($property->price_mxn):?>
                                        $ <?=number_format($property->c_price_mxn,2)?> <span class="currency-small">MXN</span>
                                    <?php else:?>
                                        $ <?=number_format($property->c_price,2)?> <span class="currency-small">USD</span>
                                    <?php endif;?>
                                <?php endif;?>
                                <?php if($post->post_type == 'rent'):?>
                                    <?php if($pricing):?>
                                        <?php $has_one = false;?>
                                        <?php $has_two = false;?>
                                        <?php if($pricing['has_night']):?>
                                            $ <?=number_format($pricing['ranges'][0]['night'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Nightly')?></span>
                                            <?php $has_one = true; ?>
                                        <?php endif;?>
                                        <?php if($pricing['has_week']):?>
                                            <?php if($has_one):?>
                                                <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <?php endif;?>
                                            $ <?=number_format($pricing['ranges'][0]['week'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Weekly')?></span>
                                            <?php $has_one = true; ?>
                                            <?php $has_two = true; ?>
                                        <?php endif;?>
                                        <?php if($pricing['has_month'] && !$has_two):?>
                                            <?php if($has_one):?>
                                                <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <?php endif;?>
                                            $ <?=number_format($pricing['ranges'][0]['month'],0)?> 
                                            <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Monthly')?></span>
                                        <?php endif;?>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                            <div class="property-grid-see-more"><?=pll__('see more details')?></div>
                        </div>
                    </a>
                    <a class="property-grid-content-title w-hidden-main w-hidden-medium w-inline-block" target="_blank" href="<?=get_permalink()?>">
                        <h2 class="property-grid-title"><?php the_title()?></h2>
                        <?php if($property->location):?>
                            <div class="property-grid-location"><?=$property->location->name?></div>
                        <?php endif;?>
                        <div class="property-grid-price">
                            <?php if($post->post_type == 'property'):?>
                                <?php if($property->price_mxn):?>
                                    $ <?=number_format($property->c_price_mxn,2)?> <span class="currency-small">MXN</span>
                                <?php else:?>
                                    $ <?=number_format($property->c_price,2)?> <span class="currency-small">USD</span>
                                <?php endif;?>
                            <?php endif;?>
                            <?php if($post->post_type == 'rent'):?>
                                <?php if($pricing):?>
                                    <?php $has_one = false;?>
                                    <?php $has_two = false;?>
                                    <?php if($pricing['has_night']):?>
                                        $ <?=number_format($pricing['ranges'][0]['night'],0)?> 
                                        <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Nightly')?></span>
                                        <?php $has_one = true; ?>
                                    <?php endif;?>
                                    <?php if($pricing['has_week']):?>
                                        <?php if($has_one):?>
                                            <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <?php endif;?>
                                        $ <?=number_format($pricing['ranges'][0]['week'],0)?> 
                                        <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Weekly')?></span>
                                        <?php $has_one = true; ?>
                                        <?php $has_two = true; ?>
                                    <?php endif;?>
                                    <?php if($pricing['has_month'] && !$has_two):?>
                                        <?php if($has_one):?>
                                            <span class="rentals-price-spacing">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <?php endif;?>
                                        $ <?=number_format($pricing['ranges'][0]['month'],0)?> 
                                        <span class="currency-small"><?=$pricing['currency']?> / <?=pll__('Monthly')?></span>
                                    <?php endif;?>
                                <?php endif;?>
                            <?php endif;?>
                        </div>
                    </a>
                    <a class="remove-favorites-box fav-remove-list w-inline-block" href="#" data-post-id="<?=get_the_ID()?>">
                        <div class="remove-favorites-text"><?=pll__('Remove from favorites')?></div>
                    </a>
                </div>
            <?php endforeach;?>
            </div>
        <?php else:?>
            <h3><?=pll__('You don\'t have favorite properties yet');?></h3>
        <?php endif;?>

    </div>
</div>


<?php get_footer();?>