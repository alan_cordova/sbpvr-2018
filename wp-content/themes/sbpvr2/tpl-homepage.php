<?php
/*
Template Name: Home
*/
?>
<?php

    if(isset($_GET['mapData'])){ //return map data as json if mapData is true
        require_once 'includes/modules/mapData.php'; exit;
    }

    the_post();

    $tresmares_rentals = get_post_type_archive_link('rent') . '?devID=' . pll_get_term(417, LANG);
    $shangrila_rentals = get_post_type_archive_link('rent') . '?devID=' . pll_get_term(485, LANG);

    $featured_properties = get_field('home_featured_properties');
    $featured_rents = get_field('home_featured_rents');

    /* MEET OUR TEAM SECTION */
    $textTeam     = get_field('c_text_section_team');
    $titleTeam    = get_field('c_title_section_team');
    $subTitleTeam = get_field('c_sub_title_section_team');
    $membersTeam  = get_field('c_featured_members');

    /* PATH LYFESTYLE SECTION */
    $textPath     = get_field('c_text_section_path');
    $titlePath    = get_field('c_title_section_path');
    $subTitlePath = get_field('c_sub_title_section_path');

    /* CONTACT SECTION */
    $textContact  = get_field('c_text_section_contact', 'option');
    $titleContact = get_field('c_title_section_contact', 'option');

?>
<?php require_once 'includes/modules/process-form.php';?>
<?php get_header();?>
<div class="modal-bg">
    <div class="modal-content">
        <div class="modal-container w-clearfix">
            <a href="#" class="v2-close-modal btn-close w-button"><em class="italic-text-2"></em></a>
            <div class="v2-contact-form">
                <h3><?=$titleContact?></h3>
                <p><?=$textContact?></p>
                <div class="form-block-2">
                    <form id="contact_form" action="<?=get_permalink()?>" class="form-2">
                        <input type="hidden" name="f_cmd" class="cmd" value="" />
                        <div class="div-block-9">
                            <input class="contact-form-input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                            <input class="contact-form-input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                        </div>
                        <input class="contact-form-input-lg w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                        <textarea class="contact-form-input-lg w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                        <input class="btn-black w-button" type="submit" value="<?=pll__('Send')?>">
                    </form>
                    <div class="w-form-done">
                        <div><?=pll__('Contact success response')?></div>
                    </div>
                    <div class="w-form-fail">
                        <div><?=pll__('Contact error response')?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'includes/modules/menu.php';?>

<div class="grid-section">
    <div class="row-grid-1">
        <a href="<?=get_permalink($cms_ids['about'])?>" class="grid-slides-sbrealtors grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('SB REALTORS')?></h2>
                <p class="grid-p"><?=pll__('About Us and Contact')?></p>
            </div>
        </a>
        <a href="<?=get_permalink($cms_ids['developments'])?>" class="grid-slides-developments grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('DEVELOPMENTS')?></h2>
                <p class="grid-p"><?=pll__('Discover what the bay has to offer')?></p>
            </div>
        </a>
        <a href="<?=get_post_type_archive_link('property')?>" class="grid-slides-properties grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('PROPERTIES')?></h2>
                <p class="grid-p"><?=pll__('Discover our Amazing Property portfolio')?></p>
            </div>
        </a>
    </div>
    <div class="row-grid-1">
        <a href="<?=get_post_type_archive_link('rent')?>" class="grid-slides-rentals grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('RENTALS')?></h2>
                <p class="grid-p"><?=pll__('Discover our Amazing Vacation rental portfolio')?></p>
            </div>
        </a>
        <a href="<?=get_permalink($cms_ids['concierge'])?>" class="grid-slides-concierge grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('CONCIERGE')?></h2>
                <p class="grid-p"><?=pll__('Our qualify team and services best in the bay')?></p>
            </div>
        </a>
        <a href="/mls/search" class="grid-slides-mlssearch grid-slide w-inline-block">
            <div class="grid-text">
                <h2 class="grid-title"><?=pll__('MLS SEARCH')?></h2>
                <p class="grid-p"><?=pll__('Discover our Amazing Property portfolio')?></p>
            </div>
        </a>
    </div>
</div>

<?php if(count($membersTeam) > 1) { ?>
    <div class="team-section">
        <div class="container w-container">
            <h3 class="heading"><?=$titleTeam?></h3>
            <p class="paragraph"><?=$subTitleTeam?></p>
            <p class="paragraph-2"><?=$textTeam?></p>
            <div class="v2-container-btn-meet-team">
                <a href="<?=get_post_type_archive_link('member')?>" class="v2-btn-meet-team w-button"><?=pll__('VIEW ALL MEMBERS')?></a>
            </div>
            <div class="div-block w-hidden-small w-hidden-tiny">
                <div data-animation="slide" data-duration="500" data-infinite="1" class="slider w-slider">
                    <div class="mask w-slider-mask">
                        <?php
                            $counter = 1;
                            foreach($membersTeam as $member) {
                        ?>
                            <?php
                                $imgMember   = get_field('c_image_member', $member->ID);
                                $jobMember   = get_field('c_job_member', $member->ID);
                                $emailMember = get_field('c_email_member', $member->ID);
                            ?>
                            <?php if($counter % 3 == 1){ ?>
                                <div class='slide w-slide'>
                                    <div class='slide-team'>
                            <?php } ?>
                                <a href="<?=get_permalink($member->ID)?>" class="slide-team-item w-inline-block">
                                    <div class="slide-team-item-picture-home" style="background-image: url(<?=$imgMember['sizes']['img-member-300x300']?>)">
                                        <img src="<?=THEME_PATH?>/images/image-placeholder.svg" class="v2-img-profile">
                                    </div>
                                    <div class="slide-team-item-name"><?=$member->post_title?></div>
                                    <div class="slide-team-item-contact"><?=$jobMember?> | <?=$emailMember?></div>
                                </a>
                            <?php if($counter % 3 == 0){ ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                                $counter++;
                            ?>
                        <?php } ?>
                        <?php if ($counter % 3 != 1){ ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="left-arrow w-slider-arrow-left">
                        <div class="w-icon-slider-left"></div>
                    </div>
                    <div class="right-arrow w-slider-arrow-right">
                        <div class="w-icon-slider-right"></div>
                    </div>
                    <div class="slide-nav w-slider-nav w-round"></div>
                </div>
            </div>
            <div class="div-block w-hidden-main w-hidden-medium">
                <div data-animation="slide" data-duration="500" data-infinite="1" class="slider w-slider">
                    <div class="mask w-slider-mask">
                        <?php
                            foreach($membersTeam as $member) {
                                $imgMember   = get_field('c_image_member', $member->ID);
                                $jobMember   = get_field('c_job_member', $member->ID);
                                $emailMember = get_field('c_email_member', $member->ID);
                        ?>
                            <div class="slide w-slide">
                                <div class="slide-team">
                                    <a href="<?=get_permalink($member->ID)?>" class="slide-team-item w-inline-block">
                                        <div class="slide-team-item-picture-home" style="background-image: url(<?=$imgMember['sizes']['img-member-300x300']?>)">
                                            <img src="<?=THEME_PATH?>/images/image-placeholder.svg" class="v2-img-profile">
                                        </div>
                                        <div class="slide-team-item-name"><?=$member->post_title?></div>
                                        <div class="slide-team-item-contact"><?=$jobMember?> | <?=$emailMember?></div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="left-arrow w-slider-arrow-left">
                        <div class="w-icon-slider-left"></div>
                    </div>
                    <div class="right-arrow w-slider-arrow-right">
                        <div class="w-icon-slider-right"></div>
                    </div>
                    <div class="slide-nav w-slider-nav w-round"></div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="about-section">
    <div class="container-2 w-container">
        <h3 class="heading"><?=$titlePath?></h3>
        <p class="paragraph"><?=$subTitlePath?></p>
        <p class="paragraph-2"><?=$textPath?></p>
        <a href="#" class="v2-btn-get-in-touch w-button"><?=pll__('GET IN TOUCH')?></a>
    </div>
</div>
<?php get_footer();?>