<?php

    $mandarina = get_field('c_mandarina_property');
    $map = get_field('c_google_maps');

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '33551') ){
            exit;
        }

        /* CUERPO DE MENSAJE NORMAL DE CONTACTO */
        $body = '';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
        if ($mandarina) {
            if ($_POST['f_address']) {
                $body .= "<b>".pll__('Address').":</b> " . htmlspecialchars($_POST['f_address']) . '<br>';
            }
            if ($_POST['f_city']) {
                $body .= "<b>".pll__('City').":</b> " . htmlspecialchars($_POST['f_city']) . '<br>';
            }
            if ($_POST['f_cp']) {
                $body .= "<b>".pll__('CP').":</b> " . htmlspecialchars($_POST['f_cp']) . '<br>';
            }
            if ($_POST['f_date']) {
                $body .= "<b>".pll__('Date').":</b> " . htmlspecialchars($_POST['f_date']) . '<br>';
            }
        }
        $body .= "<b>".pll__("I'm interested in").":</b> " . htmlspecialchars(get_the_title()) . '<br>';
        $body .= "<b>".pll__('Message').":</b> " . nl2br(htmlspecialchars($_POST['f_message'])) . '<br>';
        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';

        /* CUERPO DE MENSAJE MANDARINA DE CONTACTO */
        if ($mandarina) {
            $body2 = '';
            $body2 .= "<b>".pll__('Prospect SB Realtors').":</b> <br>";
            $body2 .= "--------------------------------------------------------- <br>";
            $body2 .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
            $body2 .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
            $body2 .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
            if ($_POST['f_address']) {
                $body2 .= "<b>".pll__('Address').":</b> " . htmlspecialchars($_POST['f_address']) . '<br>';
            }
            if ($_POST['f_city']) {
                $body2 .= "<b>".pll__('City').":</b> " . htmlspecialchars($_POST['f_city']) . '<br>';
            }
            if ($_POST['f_cp']) {
                $body2 .= "<b>".pll__('CP').":</b> " . htmlspecialchars($_POST['f_cp']) . '<br>';
            }
            if ($_POST['f_date']) {
                $body2 .= "<b>".pll__('Date').":</b> " . htmlspecialchars($_POST['f_date']) . '<br>';
            }
            $body2 .= "<b>".pll__("I'm interested in").":</b> " . htmlspecialchars(get_the_title()) . '<br>';
            $body2 .= "<b>".pll__('Message').":</b> " . nl2br(htmlspecialchars($_POST['f_message'])) . '<br><br>';
            $body2 .= "--------------------------------------------------------- <br><br>";
            $body2 .= "<b>REALTY FIRM:</b> Silva Brisset Realtors<br>";
            $body2 .= "<b>ADDRESS:</b> Paseo de la Marina 355-20, Marina Vallarta<br>";
            $body2 .= "<b>CITY & ZIP CODE:</b> Puerto Vallarta 48335<br>";
            $body2 .= "<b>DAYTIME PHONE:</b> 322 221 0051<br>";
            $body2 .= "<b>EVENING PHONE:</b> 322 221 0051<br>";
            $body2 .= "<b>REFERRING AGENT:</b> Silva Brisset Realtors<br>";
            $body2 .= "<b>SALES ASSOCIATE:</b> Silva Brisset Realtors<br>";
            $body2 .= "--------------------------------------------------------- <br>";
            $body2 .= "<b>Website language:</b> ";
            $body2 .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';

            $subject2 = pll__('Prospect SB Realtors').' - SBPVR';
            $headers2 = array('Content-Type: text/html; charset=UTF-8');

            $to3 = 'info@discovermandarina.com';
            wp_mail( $to3, $subject2, $body2, $headers2 );

        }

        $subject = 'SBPVR - Properties for sale contact';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        if(LIVE_SITE){
            $to = get_field('c_global_email','option');
            wp_mail( $to, $subject, $body, $headers );
        }

        $to2 = 'alan.cordova@mexmags.com';
        wp_mail( $to2, $subject, $body, $headers );
        echo json_encode(array('success'=>true));
        exit;
    }


    $post_type = get_post_type();
    $is_ajax = isset($_GET['ajax_search']) ? true : false;

    $property = get_property_info(true);
    $property_pin = get_property_pin($property->ID);
    $price_reduced = get_property_price_reduced($property->ID);

    if(isset($_GET['download'])){
        if (count($property->gallery) > 1) {
            require_once 'includes/modules/property-pdf-horizontal.php';
            exit;
        }else{
            require_once 'includes/modules/property-pdf-vertical.php';
            exit;
        }
    }

    $featured = ($property->gallery) ? $property->gallery[0]['sizes']['gallery-1500x850'] :  false;

    if(!$is_ajax){
        $args = array(
            'post_type' => 'property',
            'posts_per_page' => 3,
            'orderby' => 'rand',
            'post__not_in' => array(get_the_ID())
        );
        $more_properties =  new WP_Query($args);
        $more_properties = $more_properties->posts;
    }
?>
<?php if(!$is_ajax):?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>
    <div class="v2-features-modal-bg">
        <div class="modal-content">
            <div class="modal-container w-clearfix">
                <a href="#" class="v2-close-modal btn-close w-button <?=($mandarina)?'v2-close-btn-modal-mandarina':''?>"><em class="italic-text-2"></em></a>
                <div class="v2-content-modal">
                    <?php if($property->features):?>
                        <h3 class="v2-title-modal"><?=pll__('Amenities &amp; Specs')?></h3>
                        <div class="features-popup-list">
                            <?php $count = 1; ?>
                            <?php foreach($property->features as $feature):
                                if ($count%10 == 1)
                                {
                                    echo "<ul class='features-list'>";
                                }
                                ?> 
                                <li class="list-item">
                                    <div><?=$feature->name?></div>
                                </li>
                                <?php
                                if ($count%10 == 0)
                                {
                                    echo "</ul>";
                                }
                                $count++;
                            ?>
                            <?php endforeach;?>
                            <?php if ($count%10 != 1) echo "</ul>"; ?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <div class="v2-contact-agent-modal-bg">
        <div class="modal-content">
            <div class="modal-container w-clearfix">
                <a href="#" class="v2-close-modal btn-close w-button <?=($mandarina)?'v2-close-btn-modal-mandarina':''?>"><em class="italic-text-2"></em></a>
                <div class="v2-content-modal">
                    <div class="v2-contact-form">
                        <h3><?=pll__('Ask our agents')?></h3>
                        <div class="form-block-2">
                            <form id="contact_from_property" action="<?=get_permalink()?>" class="form-2">
                                <input type="hidden" name="f_cmd" class="cmd" value="" />
                                <div class="div-block-9">
                                    <input class="contact-form-input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                                    <input class="contact-form-input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                                </div>
                                <input class="contact-form-input-lg w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                                <?php if($mandarina) { ?>
                                    <input id="mandarina_property" type="hidden" name="f_mandarina_contact" value="true">
                                    <input class="contact-form-input-lg w-input" maxlength="256" name="f_address" placeholder="<?=pll__('Address')?>" type="text">
                                    <div class="div-block-9">
                                        <input class="contact-form-input w-input" maxlength="256" name="f_city" placeholder="<?=pll__('City')?>" type="text">
                                        <input class="contact-form-input w-input" maxlength="256" name="f_cp" placeholder="<?=pll__('CP')?>" required="required" type="text">
                                    </div>
                                    <input class="contact-form-input w-input" maxlength="256" name="f_date" type="hidden" id="printDate" value="">
                                <?php } ?>
                                <input type="hidden" name="f_interest" value="<?=pll__("I'm interested in")?> <?php the_title()?>" />
                                <div class="property-interest">
                                    <?=pll__("I'm interested in")?> <?php the_title()?>
                                </div>
                                <textarea class="contact-form-input-lg w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                                <?php if($mandarina) { ?>
                                    <div class="row">
                                        <p style="font-size: 8pt; line-height: 18px;"><input type="checkbox" class="f_accept" name="f_accept" required="required"></input> <?=pll__('Terms Property One')?></p>

                                        <p style="font-size: 7pt; line-height: 16px;"><?=pll__('Terms Property Two')?></p>
                                    </div>
                                <?php } ?>
                                <input class="btn-black w-button" type="submit" value="<?=pll__('Send')?>">
                            </form>
                            <div class="w-form-done">
                                <div><?=pll__('Contact success response')?></div>
                            </div>
                            <div class="w-form-fail">
                                <div><?=pll__('Contact error response')?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rental-detail">
        <div class="container-11 w-container">
            <?php require_once 'includes/modules/breadcrumbs-property.php';?>
            <a class="property-gallery-lightbox w-inline-block w-lightbox" href="#">
                <img class="property-hero-img" src="<?=$featured?>" <?=img_metadata($property->gallery[0], $post, '')?> >
                <script class="w-json" type="application/json">
                    {
                        "group": "property",
                        "items": [{
                            "type": "image",
                            "_id": "property_id_0",
                            "url": "<?=$property->gallery[0]['sizes']['gallery-1500x850']?>"
                        }]
                    }
                </script>
            </a>
            <?php for($i = 1; $i < count($property->gallery); $i++):?>
                <div class="property-gallery-slide w-slide">
                    <a class="property-gallery-lightbox w-inline-block w-lightbox" href="#">
                        <img class="property-gallery-lightbox-img" src="<?=$property->gallery[$i]['sizes']['gallery-250x250']?>" <?=img_metadata($property->gallery[$i], $post, '', ($i+1))?>>
                        <script class="w-json" type="application/json">
                            {
                                "group": "property",
                                "items": [{
                                    "type": "image",
                                    "_id": "property_id_<?=$i?>",
                                    "url": "<?=$property->gallery[$i]['sizes']['gallery-1500x850']?>"
                                }]
                            }
                        </script>
                    </a>
                </div>
            <?php endfor;?>
            <div class="property-detail-title">
                <div class="v2-container-title-property-detail">
                    <h1 class="heading-10"><?php the_title(); ?></h1>
                    <div class="rental-item-specs">
                        <?php if($property->beds):?>
                            <div class="v2-container-specs">
                                <div class="specs-numbers"><?=$property->beds?></div>
                                <div class="specs"><?=pll__('Beds')?></div>
                            </div>
                        <?php endif;?>

                        <?php if($property->baths):?>
                            <div class="v2-container-specs">
                                <div class="specs-numbers"><?=$property->baths?></div>
                                <div class="specs"><?=pll__('Baths')?></div>
                            </div>
                        <?php endif;?>

                        <?php if($property->parking):?>
                            <div class="v2-container-specs">
                                <div class="specs-numbers"><?=$property->parking?></div>
                                <div class="specs"><?=pll__('Parking Space')?></div>
                            </div>
                        <?php endif;?>

                        <?php if($property->sqft):?>
                            <div class="v2-container-specs">
                                <div class="specs-numbers"><?=number_format($property->sqft)?></div>
                                <div class="specs">sq ft</div>
                            </div>
                        <?php endif;?>

                        <?php if($property->m2):?>
                            <div class="v2-container-specs">
                                <div class="specs-numbers"><?=number_format($property->m2)?></div>
                                <div class="specs">sq m</div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <div class="v2-container-price-property-detail">
                <?php if(!$mandarina) { ?>
                    <?php if($property->sold): ?>
                        <div class="v2-label-sold">
                            <strong><?=strtoupper(pll__('Sold Property'))?></strong>
                        </div>
                    <?php endif;?>
                    <?php if($price_reduced !== false):?>
                        <div class="v2-property-reduced">
                            <?=pll__('Reduced from')?> $ <?=number_format($price_reduced, 2)?> USD <?=pll__('to')?>
                        </div>
                    <?php endif;?>
                    <div class="v2-price-property-detail">$ <?=number_format($property->c_price,2)?></div>
                    <div class="v2-currency-property-detail">USD</div>
                <?php } ?>
                </div>
            </div>
            <div class="rental-detail-description property-detail-description">
                <div class="description">
                    <div class="paragraph-10"><?=$post->post_content?></div>
                    <div class="rental-detail-description-btn">
                        <?php if($property->features):?>
                            <a href="#" class="v2-btn-amenities w-button"><?=pll__('Amenities &amp; Specs')?></a>
                        <?php endif;?>
                        <a class="v2-btn-pdf w-button" href="<?=get_permalink()?>?download=1" target="_blank"><?=pll__('Get PDF')?></a>
                        <?php if(!$mandarina) { ?>
                            <a href="#" class="v2-btn-contact-agent w-button"><?=pll__('Contact Agent')?></a>
                        <?php }else{ ?>
                            <a href="#" class="v2-btn-contact-agent w-button"><?=pll__('For Sales and Inquiries')?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="rental-detail-location-black">
                <div class="development-address-map-black-desktop">
                    <div class="text-block-2"><em class="italic-text-3 text-black"></em></div>
                    <a href="#" data-lat="<?=$map['lat']?>" data-lng="<?=$map['lng']?>" class="btn-details-map-black-movil w-button"><?=pll__('Take Me There')?></a>
                    <div class="v2-address-property"><?=$property->address?></div>
                </div>
                <div class="development-address-map-black-movil">
                    <a href="#" data-lat="<?=$map['lat']?>" data-lng="<?=$map['lng']?>" class="btn-details-map-black w-button" target="_blank"><?=pll__('Show me this property in map')?></a>
                </div>
            </div>
            <?php if(!$is_ajax && isset($more_properties)):?>
                <div class="similar-properties">
                    <h3 class="similar-properties-title"><?=pll__('Related Properties')?></h3>
                    <div class="similar-properties-items w-hidden-small w-hidden-tiny">
                        <?php foreach($more_properties as $post):?>
                            <?php setup_postdata($post)?>
                            <?php $price_reduced = get_property_price_reduced(get_the_ID());?>
                            <?php $property = get_property_info(true);?>
                            <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['img-property-agent-330x250'] :  false; ?>
                            <a href="<?=get_permalink()?>" class="v2-similar-property w-inline-block">
                                <div class="v2-information-hover-property">
                                    <h3 class="v2-hover-title-property-list"><?php the_title()?></h3>
                                    <?php if($property->location):?>
                                        <h4 class="v2-hover-location-property-list">
                                            <?=$property->location->name?>
                                        </h4>
                                    <?php endif;?>
                                    <div class="v2-hover-specs-property-list">
                                        <?php if($property->beds):?>
                                            <div class="v2-hover-specs-numbers"><?=$property->beds?></div>
                                            <div class="v2-hover-specs"><?=pll__('Beds')?></div>
                                        <?php endif;?>

                                        <?php if($property->baths):?>
                                            <div class="v2-hover-specs-numbers"><?=$property->baths?></div>
                                            <div class="v2-hover-specs"><?=pll__('Baths')?></div>
                                        <?php endif;?>
                                    </div>
                                    <div class="v2-hover-container-btn-more">
                                        <div class="v2-hover-btn-more">
                                            <div class="v2-icon-more">+</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="v2-img-property-list" style="background-image: url(<?=$featured?>)"> 
                                    <img src="<?=THEME_PATH?>/images/img_default.png">
                                </div>
                                <div class="v2-information-property-text">
                                    <h3 class="v2-title-property-list"><?php the_title()?></h3>
                                    <?php if($property->location):?>
                                        <h4 class="heading-8">
                                            <?=$property->location->name?>
                                        </h4>
                                    <?php endif;?>
                                </div>
                            </a>
                        <?php endforeach;?>
                    </div>
                    <div class="similar-properties-items-movil">
                        <?php foreach($more_properties as $post):?>
                            <?php setup_postdata($post)?>
                            <?php $price_reduced = get_property_price_reduced(get_the_ID());?>
                            <?php $property = get_property_info(true);?>
                            <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['img-property-agent-330x250'] :  false; ?>
                            <a href="<?=get_permalink()?>" class="result-item w-inline-block">
                                <div class="v2-information-hover-property">
                                    <h3 class="v2-hover-title-property-list"><?php the_title()?></h3>
                                    <?php if($property->location):?>
                                        <h4 class="v2-hover-location-property-list">
                                            <?=$property->location->name?>
                                        </h4>
                                    <?php endif;?>
                                    <div class="v2-hover-specs-property-list">
                                        <?php if($property->beds):?>
                                            <div class="v2-hover-specs-numbers"><?=$property->beds?></div>
                                            <div class="v2-hover-specs"><?=pll__('Beds')?></div>
                                        <?php endif;?>

                                        <?php if($property->baths):?>
                                            <div class="v2-hover-specs-numbers"><?=$property->baths?></div>
                                            <div class="v2-hover-specs"><?=pll__('Baths')?></div>
                                        <?php endif;?>
                                    </div>
                                    <div class="v2-hover-container-btn-more">
                                        <div class="v2-hover-btn-more">
                                            <div class="v2-icon-more">+</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="v2-img-property-list" style="background-image: url(<?=$featured?>)"> 
                                    <img src="<?=THEME_PATH?>/images/img_default.png">
                                </div>
                                <div class="v2-information-property">
                                    <div class="v2-information-property-text">
                                        <h3 class="v2-title-property-list"><?php the_title()?></h3>
                                        <?php if($property->location):?>
                                            <h4 class="heading-8">
                                                <?=$property->location->name?>
                                            </h4>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
<?php get_footer();?>
<?php endif; //end ajax filter?>