<?php
/*
Template Name: Developments List
*/
?>
<?php
    the_post();

    $featured_img_marina_tower = get_field('c_development_featured', $cms_ids['marina-tower']);
    $featured_img_vmarina = get_field('c_development_featured_listing', $cms_ids['vmarina']);
    $featured_img_shangrila = get_field('c_development_featured', $cms_ids['shangrila']);
    $featured_img_tres_mares = get_field('c_development_featured', $cms_ids['tres-mares']);
    $featured_img_azulejos = get_field('c_development_featured', $cms_ids['azulejos']);
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>

<div class="title-developments-section">
    <div class="container-5 w-container">
        <?php require_once 'includes/modules/breadcrumbs.php';?>
        <h1 class="heading-2"><?=pll__('DEVELOPMENTS')?></h1>
        <div class="div-block-5">
            <h2 class="heading-3"><?=pll__('Developments Listing Title')?></h2>
            <div class="paragraph-4"><?php the_content()?></div>
        </div>
    </div>
</div>
<div class="principal-development-section w-hidden-small w-hidden-tiny">
    <div class="principal-development-image"></div>
    <div class="principal-development-description">
        <h2 class="heading-5">Marina Tower</h2>
        <p class="paragraph-7"><?=get_field('c_development_introduction', $cms_ids['marina-tower'])?></p>
        <a href="<?=get_permalink($cms_ids['marina-tower'])?>" class="title-section-btn w-button"><?=pll__('VIEW MORE')?></a>
    </div>
</div>
<div class="principal-development-section-movil w-hidden-main w-hidden-medium">
    <div class="principal-development-image"></div>
    <div class="principal-development-description">
        <h2 class="heading-5">Marina Tower</h2>
        <p class="paragraph-7"><?=get_field('c_development_introduction', $cms_ids['marina-tower'])?></p><a href="<?=get_permalink($cms_ids['marina-tower'])?>" class="title-section-btn w-button"><?=pll__('VIEW MORE')?></a>
    </div>
</div>
<div class="list-developments">
    <a href="<?=get_permalink($cms_ids['vmarina'])?>" class="list-development-item w-inline-block" target="_blank">
        <img src="<?=$featured_img_vmarina['sizes']['gallery-850x500']?>" class="v2-img-property-list">
        <div class="v2-information-property-text">
            <h3 class="v2-title-property-list">V MARINA</h3>
            <p class="paragraph-8"><?=pll__('Vmarina listing description')?></p>
        </div>
        <div class="v2-container-btn-more">
            <div class="v2-btn-more">
                <div class="v2-icon-more">+</div>
            </div>
        </div>
    </a>
    <a href="<?=get_permalink($cms_ids['shangrila'])?>" class="list-development-item w-inline-block">
        <img src="<?=$featured_img_shangrila['sizes']['gallery-850x500']?>" class="v2-img-property-list">
        <div class="v2-information-property-text">
            <h3 class="v2-title-property-list">SHANGRI-LA</h3>
            <p class="paragraph-8"><?=get_field('c_development_introduction', $cms_ids['shangrila'])?></p>
        </div>
        <div class="v2-container-btn-more">
            <div class="v2-btn-more">
                <div class="v2-icon-more">+</div>
            </div>
        </div>
    </a>
    <a href="<?=get_permalink($cms_ids['tres-mares'])?>" class="list-development-item w-inline-block">
        <img src="<?=$featured_img_tres_mares['sizes']['gallery-850x500']?>" class="v2-img-property-list">
        <div class="v2-information-property-text">
            <h3 class="v2-title-property-list">TRES MARES</h3>
            <p class="paragraph-8"><?=get_field('c_development_introduction', $cms_ids['tres-mares'])?></p>
        </div>
        <div class="v2-container-btn-more">
            <div class="v2-btn-more">
                <div class="v2-icon-more">+</div>
            </div>
        </div>
    </a>
    <a href="<?=get_permalink($cms_ids['azulejos'])?>" class="list-development-item w-inline-block">
        <img src="<?=$featured_img_azulejos['sizes']['gallery-850x500']?>" class="v2-img-property-list">
        <div class="v2-information-property-text">
            <h3 class="v2-title-property-list">AZULEJOS RIVIERA LIVING</h3>
            <p class="paragraph-8"><?=get_field('c_development_introduction', $cms_ids['azulejos'])?></p>
        </div>
        <div class="v2-container-btn-more">
            <div class="v2-btn-more">
                <div class="v2-icon-more">+</div>
            </div>
        </div>
    </a>
</div>

<?php get_footer();?>