<!DOCTYPE html>
<html lang="<?=LANG?>">
<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <?php wp_head();?>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
    <link href="<?=THEME_PATH?>/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="<?=THEME_PATH?>/css/webflow.css" rel="stylesheet" type="text/css">
    <link href="<?=THEME_PATH?>/css/sb-realtors.webflow.css" rel="stylesheet" type="text/css">
    <link href="<?=THEME_PATH?>/css/styles-v2.css?v=<?=THEME_VERSION?>" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Raleway:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
    <meta name="mobile-web-app-capable" content="yes">
    <!--<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>-->
  

    <!--<script src="<?=THEME_PATH?>/js/modernizr.js" type="text/javascript"></script>-->
  
    <link rel="stylesheet" href="<?=THEME_PATH?>/js/jrange/jquery.range.css">
    <link rel="stylesheet" href="<?=THEME_PATH?>/vendor/calendar/css/bootstrap-year-calendar.min.css">



    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Silva Brisset Realtors"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?=THEME_PATH?>/images/fav/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?=THEME_PATH?>/images/fav/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?=THEME_PATH?>/images/fav/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?=THEME_PATH?>/images/fav/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?=THEME_PATH?>/images/fav/mstile-310x310.png" />
    
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
</head>
<body class="body">
    <?php if(LIVE_SITE){?>
        <div id="code1" style="display:none;">82</div>
        <div id="code2" style="display:none;">97863</div>
        <div id="code3" style="display:none;">8</div>
        <?php //Analitics ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            var a = document.getElementById('code1').innerHTML;
            var b = document.getElementById('code2').innerHTML;
            var c = document.getElementById('code3').innerHTML;
            ga('create', 'UA' + '-'+(c+b+a)+'-1', 'auto');
            ga('send', 'pageview');
        </script>
    <?php } ?>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1798357530493392',
          xfbml      : true,
          version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    }(document, "script", "twitter-wjs"));</script>