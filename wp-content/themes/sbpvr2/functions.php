<?php

date_default_timezone_set('America/Mexico_City');
define('THEME_PATH', get_template_directory_uri());
define('LIVE_SITE', ($_SERVER['HTTP_HOST']=='sbpvr.com') ? true : false );
define('SITE_DOMAIN', (LIVE_SITE) ? 'https://sbpvr.com' : 'http://dev.sbpvr.com');
define('THEME_VERSION', (LIVE_SITE) ? 193 : strtotime('now'));
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
define('THEME_ROOT', SITE_ROOT . 'wp-content/themes/sbpvr2/');

/** APPEND GET PARAMETERS */
    function append_get_params(){
        $q = trim($_SERVER['QUERY_STRING']);
        return (!empty($q)) ? '?' . urlencode($q) : '';
    }
/** END **/

/** Disable W3TC footer comment for everyone but Admins (single site) / Super Admins (network mode) **/
    add_filter( 'w3tc_can_print_comment', '__return_false', 10, 1 );
/** END **/

/** SUPPORT TO SVG IMAGES **/
function add_svg_to_upload_mimes( $upload_mimes ) {
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

add_filter( 'posts_orderby' , 'custom_cpt_order' );
function custom_cpt_order( $orderby ) {
    global $wpdb;
    
    // Check if the query is for an archive
    if ( is_archive() && get_query_var("post_type") == "member" ) {
        // Query was for archive, then set order
        return "$wpdb->posts.post_title ASC";
    }
    
    return $orderby;
}

/* CLEAN UP HEAD OPTIONS */

    function clean_up_head () {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );

        remove_action('wp_head', 'wp_generator');                // #1
        remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.

        //removes margin top heading on html !important 32px
        remove_action('wp_head', '_admin_bar_bump_cb');

        //removes json links
        remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
    }
    add_action('after_setup_theme', 'clean_up_head');

    function wpb_imagelink_setup() {
        $image_set = get_option( 'image_default_link_type' );
        
        if ($image_set !== 'none') {
            update_option('image_default_link_type', 'none');
        }
    }
    add_action('admin_init', 'wpb_imagelink_setup', 10);

/** END **/

/** Resize uploaded image to fit scale media image sizes **/
    function binary_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
        if ( !$crop ) return null; // let the WordPress default function handle this
         
        $aspect_ratio = $orig_w / $orig_h;
        $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
         
        $crop_w = round($new_w / $size_ratio);
        $crop_h = round($new_h / $size_ratio);
         
        $s_x = floor( ($orig_w - $crop_w) / 2 );
        $s_y = floor( ($orig_h - $crop_h) / 2 );
         
        return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
    }
    add_filter( 'image_resize_dimensions', 'binary_thumbnail_upscale', 10, 6 );
/** END **/


/** POLYLANG FILTERS **/

    // Removes unnecesary trailing slash "/" except home page
    add_filter('pll_translation_url', 'check_archive_translation', 10, 2);
    function check_archive_translation($url, $lang) {
        $url = rtrim($url, '/');
        if($url == SITE_DOMAIN){
            $url = $url . '/';
        }
        $url .= append_get_params();
    
        return $url;
    }

    //archive url 
    add_filter('pll_translated_post_type_rewrite_slugs', function($post_type_translated_slugs) {
        // Add translation for "product" post type.
        $post_type_translated_slugs = array(
            'property' => array(
                'en' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'properties-for-sale',
                    ),
                ),
                'es' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'propiedades-en-venta',
                    ),
                ),
            ),
            'rent' => array(
                'en' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'vacation-rentals',
                    ),
                ),
                'es' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'rentas-vacacionales',
                    ),
                ),
            ),
            'member' => array(
                'en' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'meet-our-team',
                    ),
                ),
                'es' => array(
                    'has_archive' => true,
                    'rewrite' => array(
                        'slug' => 'conoce-al-equipo',
                    ),
                ),
            ),
        );
        return $post_type_translated_slugs;
    });

/** END POLYLANG FILTER **/


/* MENU OPTIONS */
    function register_main_menu_en() {
      register_nav_menu('main-menu-en',__( 'main menu en' ));
    }
    add_action( 'init', 'register_main_menu_en' );
/* END */

/** YOAST IMAGE OPENGRAPH OVERRIDE **/
    function filter_wpseo_opengraph_image( $img ) { 
        return $img; 
    };         
    add_filter( 'wpseo_opengraph_image', 'filter_wpseo_opengraph_image', 10, 1 ); 

/** END **/

/** YOAST SEO OVERRIDE **/

    add_filter('wpseo_title', function($title) {
        return pll__($title);
    });
    add_filter('wpseo_metadesc', function($desc) {
        return pll__($desc);
    });
    add_filter('wpseo_canonical', function($canonical) {
        if(!is_home()){
            $canonical = rtrim($canonical, '/');
        }
        return $canonical . append_get_params();
    });
    add_filter('wpseo_prev_rel_link', function($prev) {
        if(!is_home()){
            $prev = str_replace('/"','"',$prev);
        }
        $q = append_get_params();
        return str_replace('" />', $q . '" />' , $prev);
    }); 
    add_filter('wpseo_next_rel_link', function($next) {
        if(!is_home()){
            $next = str_replace('/"','"',$next);
        }
        $q = append_get_params();
        return str_replace('" />', $q . '" />' , $next);
    });

/** YOAST END **/

function custom_widgets_init() {
    register_sidebar( array(
        'name'          => __('Flex Listing', 'flex-listing'),
        'id'            => 'sbpvr-flex-widget',
    ) );
}
add_action( 'widgets_init', 'custom_widgets_init' );

if(function_exists('pll__')){
    require_once 'includes/polylang-translations.php';
    
    if(is_admin()){
        function synchronize_rents_ocupancy( $post_id ) {
            $p = get_post($post_id);
            if($p->post_type == 'rent'){

                //synchronize ocupancy between rents 
                /*$lang_slug = pll_get_post_language($post_id, 'slug');
                $reverse_lang_slug = ($lang_slug == 'en') ? 'es' : 'en';
                $post_id_reversed_lang = pll_get_post($post_id, $reverse_lang_slug);

                if($post_id_reversed_lang){
                    $ocupancy = get_field('c_ro_ocupancy', $post_id);
                    if(!is_array($ocupancy) || (is_array($ocupancy) && count($ocupancy) == 0) ){
                        $ocupancy = null;
                    }

                    update_field('c_ro_ocupancy', $ocupancy, $post_id_reversed_lang);
                }*/
                
                //synchronize rentals price for filtering
                //c_price_rentals_filter
                $prices = get_field('c_pricing_rent_range_loop', $post_id);
                if($prices){
                    $lower = false;
                    foreach($prices as $row){
                        $price = $row['c_range_night'];
                        if(!$price){
                            $price = $row['c_range_week'];
                        }
                        if(!$price) {
                            $price = $row['c_range_month'];
                        }

                        if($lower === false){
                            $lower = $price;
                        }
                        if($price < $lower){
                            $lower = $price;
                        }
                    }
                    if($lower === false){
                        $lower = 0;
                    }

                    update_field('c_price_rentals_filter', $lower, $post_id);
                }

            }
        }
        add_action( 'save_post', 'synchronize_rents_ocupancy' );

        function clear_map_cache(){
            $file = __DIR__ . '/includes/modules/mapCache/mapData.txt';
            if(file_exists($file)){
                unlink($file);
            }
        }
        add_action( 'save_post', 'clear_map_cache' );
    }
}

// ACF Maps API KEY filter
if(is_admin()){
    function my_acf_google_map_api( $api ){ 
        $api['key'] = 'AIzaSyBKF1GvQF4ZJdoUtDFe-nmLe0v8FCl5HtA'; 
        return $api;  
    }
    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
}


if(!is_admin()){
    //show_admin_bar(false);
    require_once 'includes/config.php';

    /** OVERRIDES ARCHIVE WORDPRESS QUERY TO CUSTOM IF NEEDED **/
        add_action( 'pre_get_posts', 'archive_custom_query_vars' );
        function archive_custom_query_vars( $query ) {
            if ( !is_admin() && $query->is_main_query()) {

                //override query for properties listing
                if(isset($query->query['post_type']) && $query->query['post_type'] == 'property' && !isset($query->query['page'])){
                    get_post_type_property($query);
                }else if(isset($query->query['post_type']) && $query->query['post_type'] == 'rent' && !isset($query->query['page'])){
                    get_post_type_rent($query);
                }

                //override query for rents listing
                /*if(isset($query->query['post_type']) && $query->query['post_type'] == 'rent' && !isset($query->query['page'])){
                    get_post_type_rents($query);
                }*/
            }
            return $query;
        }
    /** END **/


    /** PREPARES MENU FROM WORDPRESS TO ARRAY (MAX: 3 LEVELS) **/
        function c_prepare_menu_items($menu_tmp){
            $menu = array();           
            foreach($menu_tmp as $v){
                if( $v->menu_item_parent == 0){
                    $menu[$v->ID] = $v;
                    if(!isset($menu[$v->ID]->menu_childs)){
                        $menu[$v->ID]->menu_childs = array();
                    }
                }
                if(isset($menu[$v->menu_item_parent])){
                    $menu[$v->menu_item_parent]->menu_childs[$v->ID] = $v;
                    if(!isset($menu[$v->menu_item_parent]->menu_childs[$v->ID]->menu_childs)){
                        $menu[$v->menu_item_parent]->menu_childs[$v->ID]->menu_childs = array();                        
                    }
                }else{
                    foreach($menu as $k_level_1 => $menu_level_1){
                        if(!empty($menu_level_1->menu_childs)){
                            foreach($menu_level_1->menu_childs as $menu_child){
                                if($v->menu_item_parent != 0 && $v->menu_item_parent == $menu_child->ID){
                                    $menu[$k_level_1]->menu_childs[$v->menu_item_parent]->menu_childs[$v->ID] = $v;
                                    $menu[$k_level_1]->menu_childs[$v->menu_item_parent]->menu_childs[$v->ID]->menu_childs = array();
                                }
                            }
                        }
                    }
                }
            }
            return $menu;
        }
    /** END **/

}