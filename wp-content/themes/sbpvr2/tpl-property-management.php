<?php
/*
Template Name: Property Management
*/
?>
<?php
    the_post();

    //catch property management form ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '3341') ){
            exit;
        }

        $body = '<h3>' . get_the_title() . '</h3>';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>'; 
        if(isset($_POST['f_interest'])){
            $interest = '';
            foreach($_POST['f_interest'] as $v){
                $interest .= htmlspecialchars($v) . ', ';
            }
            $interest = rtrim($interest, ', ');
            $body .= "<b>".pll__("I'm interested in").":</b> " . $interest . '<br>';
        }
        $body .= "<b>".pll__('Message').":</b> " . htmlspecialchars($_POST['f_message']) . '<br>';


        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';


        $subject = 'SBPVR - Property Management';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        $to2 = 'jose@vallartalifestyles.com';
        wp_mail( $to2, $subject, $body, $headers );
        
        if(LIVE_SITE){
            $headers[] = 'Cc: mariana@sbpvr.com'; 
            $to = get_field('c_global_email','option');
            wp_mail( $to, $subject, $body, $headers );
        }

        echo json_encode(array('success'=>true));
        exit;
    }
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>


<div class="page-heading-container property-management">
    <div class="container">
        <div class="page-hero-box">
            <div class="page-hero-box-inner">
                <h1 class="style-1 title"><?php the_title()?></h1>
                <h2 class="small subtitle" id="subtitle-animation"><?=get_field('c_pm_subtitle')?></h2>
            </div>
        </div>
    </div>
</div>
<div class="animated-list item-11" id="animated-list-1">
    <div class="container full">
        <div class="list-flex">
            <div class="bottom list-flex-item">
                <div class="bottom list-item-title-container">
                    <div class="list-title-container-flex">
                        <h2 class="list-item-title"><?=get_field('c_pm_management_title')?></h2>
                        <h3 class="list-item-subtitle"><?=get_field('c_pm_management_subtitle')?></h3>
                    </div>
                </div>
            </div>
            <div class="list-flex-item">
                <div class="list-item-content short-padding" id="animated-list-target-1">
                    <h3 class="list-item-subtitle-2"><?=get_field('c_pm_management_content_title_1')?></h3>
                    <p><?=get_field('c_pm_management_content_1')?></p>
                    <h3 class="list-item-subtitle-2"><?=get_field('c_pm_management_content_title_2')?></h3>
                    <p><?=get_field('c_pm_management_content_2')?></p>
                    <div class="concierge-action-box">
                        <a class="button style-1 w-button" href="#" id="open-popup-form">
                            <?=pll__('Request service')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="animated-list item-12" id="animated-list-2">
    <div class="container full">
        <div class="list-flex">
            <div class="list-flex-item">
                <div class="center list-item-title-container">
                    <div class="center list-title-container-flex">
                        <h2 class="list-item-title"><?=get_field('c_pm_rents_title')?></h2>
                        <h3 class="list-item-subtitle"><?=get_field('c_pm_rents_subtitle')?></h3>
                    </div>
                </div>
            </div>
            <div class="list-flex-item">
                <div class="list-item-content right short-padding" id="animated-list-target-2">
                    <h2 class="list-item-subtitle-2"><?=get_field('c_pm_rents_content_title_1')?></h2>
                    <p><?=get_field('c_pm_rents_content_1')?></p>
                    <h2 class="list-item-subtitle-2"><?=get_field('c_pm_rents_content_title_2')?></h2>
                    <p><?=get_field('c_pm_rents_content_2')?></p>
                    <div class="concierge-action-box">
                        <?php $cvFile = THEME_PATH . '/files/property-management-cv-' . LANG . '.pdf';?>
                        <a class="button style-1 w-button" href="<?=$cvFile?>" target="_blank">
                            <?=pll__('See Our Curriculum')?>
                        </a>

                        <a class="button style-1 w-button" href="#" id="open-popup-form-2">
                            <?=pll__('Request service')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="popup" id="popup-form">
    <div class="popup-container">
        <div class="popup-form-container">
            <a class="popup-close w-inline-block" href="#" id="popup-close">
                <img src="<?=THEME_PATH?>/images/icon-close-black.svg" alt="">
            </a>
            <h4 class="popup-form-title"><?php the_title()?></h4>
            <div class="popup-form-subtitle"><?=get_field('c_pm_subtitle')?></div>
            <div class="form-wrapper">
                <form class="form-popup w-clearfix" id="form_service" action="<?=get_permalink()?>">
                    <input class="input input-half w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                    <input class="input input-half right w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                    <input type="text" name="f_cmd" class="cmd" value="">
                    <input class="input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                    <div class="checkbox-container">
                        <label class="label"><?=pll__("I'm interested in")?>:</label>
                        <div class="checkbox w-checkbox w-clearfix">
                            <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_1" name="f_interest[]" type="checkbox" value="<?=pll__('Management Services')?>">
                            <label class="checkbox-text w-form-label" for="checkbox_1"><?=pll__('Management Services')?></label>
                        </div>
                        <div class="checkbox w-checkbox w-clearfix">
                            <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_2" name="f_interest[]" type="checkbox" value="<?=pll__('Rents Department')?>">
                            <label class="checkbox-text w-form-label" for="checkbox_2"><?=pll__('Rents Department')?></label>
                        </div>
                        <div class="checkbox w-checkbox w-clearfix">
                            <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_3" name="f_interest[]" type="checkbox" value="<?=pll__('Stay Services')?>">
                            <label class="checkbox-text w-form-label" for="checkbox_3"><?=pll__('Stay Services')?></label>
                        </div>
                        <div class="checkbox w-checkbox w-clearfix">
                            <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_4" name="f_interest[]" type="checkbox" value="<?=pll__('Maintenance')?>">
                            <label class="checkbox-text w-form-label" for="checkbox_4"><?=pll__('Maintenance')?></label>
                        </div>
                        <div class="checkbox w-checkbox w-clearfix">
                            <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_5" name="f_interest[]" type="checkbox" value="<?=pll__('Others')?>">
                            <label class="checkbox-text w-form-label" for="checkbox_5"><?=pll__('Others')?></label>
                        </div>
                    </div>
                    <label class="label" for="field-2"><?=pll__('Let us know if you have a special request')?></label>
                    <textarea class="regular-textarea textarea w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>" required="required"></textarea>
                    <div class="contact-btn-container w-clearfix">
                        <input class="btn-contact button w-button" type="submit" value="<?=pll__('Send')?>">
                    </div>
                </form>
                <div class="w-form-done">
                    <div><?=pll__('Property management success')?></div>
                </div>
                <div class="w-form-fail">
                    <div><?=pll__('Property management fail')?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer();?>