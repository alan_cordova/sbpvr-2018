<?php
/*
Template Name: Landing page VMARINE
*/
?>
<?php 
    the_post();
    show_admin_bar(false);

    //catch contact ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '28283s') ){
            exit;
        }
        $body = '<h3>PRESALE VMARINA</h3>';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';

        $subject = 'PRESALE VMARINA - SBPVR';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $headers[] = 'From: '.$_POST['f_name'].' <'.$_POST['f_email'].'>';
        //$headers[] = 'Reply-To: '.$_POST['f_name'].' <'.$_POST['f_email'].'>';

        $to2 = 'jose@vallartalifestyles.com';
        wp_mail( $to2, $subject, $body, $headers );

        $headers[] = 'Cc: <diego@sbpvr.com>';
        $headers[] = 'Cc: <juliansb@me.com>';
        wp_mail( 'info@sbpvr.com', $subject, $body, $headers );
        
        echo json_encode(array('success'=>true));
        exit;
    }
?>
<!DOCTYPE html>
<html lang="<?=LANG?>">
<head>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <?php wp_head();?>
    <link href="<?=THEME_PATH?>/vmarina/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="<?=THEME_PATH?>/vmarina/css/webflow.css" rel="stylesheet" type="text/css">
    <link href="<?=THEME_PATH?>/vmarina/css/styles.css?v=<?=THEME_VERSION?>" rel="stylesheet" type="text/css">  


    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({google: {families: ["Nunito:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic","Tangerine:regular"]}});</script>


    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=THEME_PATH?>/images/fav/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?=THEME_PATH?>/images/fav/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Silva Brisset Realtors"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?=THEME_PATH?>/images/fav/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?=THEME_PATH?>/images/fav/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?=THEME_PATH?>/images/fav/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?=THEME_PATH?>/images/fav/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?=THEME_PATH?>/images/fav/mstile-310x310.png" />

      <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
</head>
<body class="body">

<div id="code1" style="display:none;">850</div>
<div id="code2" style="display:none;">756</div>
<div id="code3" style="display:none;">104</div>
<?php //Analitics ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    var a = document.getElementById('code1').innerHTML;
    var b = document.getElementById('code2').innerHTML;
    var c = document.getElementById('code3').innerHTML;
    ga('create', 'UA' + '-'+(c+b+a)+'-1', 'auto');
    ga('send', 'pageview');
</script>


<div class="navbar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
    <div class="navbar-container w-container">
        <a class="brand w-nav-brand ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Link VMARINA header">
            <img class="brand-img" src="<?=THEME_PATH?>/vmarina/images/logo-vmarina.svg">
        </a>
        <nav class="nav-menu w-nav-menu" role="navigation">
            <a class="nav-link w-nav-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Menu Units" href="#units">Units</a>
            <a class="nav-link w-nav-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Menu Features &amp; Specs" href="#features">Features &amp; Design</a>
            <a class="nav-link w-nav-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Menu Amenities" href="#amenities">Amenities</a>
            <a class="featured reserve-now-btn nav-link w-nav-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Menu Reserve Now" href="#reserve-now">View Complete Information</a>
        </nav>
        <div class="menu-button w-nav-button">
            <div class="menu-button-icon w-icon-nav-menu"></div>
        </div>
    </div>
</div>
<div class="hero-img-wrapper"><img class="hero-img" sizes="100vw" src="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-1.jpg" srcset="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-1-p-500.jpeg 500w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-1-p-800.jpeg 800w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-1-p-1080.jpeg 1080w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-1.jpg 1600w"></div>
<div class="hero-content-wrapper" data-ix="hero-scroll-animation">
    <div class="hero-content-inner">
        <h2 class="hero-title">A hole in one project!</h2>
        <h3 class="hero-subtitle">MARINA - GOLF - EXCLUSIVENESS &amp; LIFESTYLE</h3>
        <p class="hero-content">Its privileged location in Marina Vallarta will allow residents easy access to a broad variety of entertainment options, including gourmet restaurants, recreational and cultural activities, boutiques, spas and art galleries. A beautiful, 18-hole golf course is located within walking distance.</p>
    </div>
</div>
<div class="units-container-wrapper" id="units">
    <div class="units-container-inner">
        <div class="units-slider w-slider" data-animation="fade" data-autoplay="1" data-delay="4000" data-duration="750" data-infinite="1">
            <div class="units-slider-mask w-slider-mask">
                <div class="units-slider-item w-slide">
                    <img class="units-slider-img" sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 96vw, 82vw" src="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-2.jpg" srcset="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-2-p-500.jpeg 500w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-2.jpg 1400w">
                </div>
                <div class="units-slider-item w-slide">
                    <img class="units-slider-img" sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 96vw, 82vw" src="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-6.jpg" srcset="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-6.jpg 500w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-6.jpg 1400w">
                </div>
                <div class="units-slider-item w-slide">
                    <img class="units-slider-img" sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 96vw, 82vw" src="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-4.jpg" srcset="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-4.jpg 500w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-4.jpg 1400w">
                </div>
            </div>
            <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-left">
                <div class="w-icon-slider-left"></div>
            </div>
            <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-arrow-right">
                <div class="w-icon-slider-right"></div>
            </div>
            <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-round w-slider-nav"></div>
        </div>
        <div class="units-container-content-wrapper">
            <div class="units-container-content-inner" data-ix="scroll-animation">
                <div class="units-container-content-inner-2">
                    <h2 class="heading-subtitle">V MARINE UNITS</h2>
                    <p class="units-container-paragraph">Two Towers A &amp; B featuring a privileged location in Marina Vallarta will offer 1 to 3 bedroom units from 1,073.84 to 3,476.95 square feet.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="specs-wrapper" id="features">
    <div class="amenities-details-wrapper">
        <h2 class="heading-amenities">Features &amp; Design</h2>
        <h3 class="heading-amenities-subtitle">V Marina reinvents the concept of exclusive Marina Vallarta living with next level features that make your home feel both comfortable and stylish, including high-quality furnishing and finishings in all units.</h3>
        <div class="amenities-list-wrapper">
            <div class="amenities-list-left">
                <ul class="amenities-list small">
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Porcelanato floors</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Fine wood doors, kitchens and closets</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Granite Kitchen countertops</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Stainless steel Whirlpool appliances</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Designer lighted ceiling details</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Ceiling fan at Master Bedrooms</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Ensuite bathrooms in all bedrooms</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Marble Bath countertops with high quality sinks and faucets</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Tempered glass shower doors</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Tinted glass aluminum windows and sliding doors</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Sliding door access to terrace</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Tinted glass terrace railings</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Prewired for TV and satellite</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Deeded parking spaces included*</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">Bodegas available*</div>
                    </li>
                    <!--<li class="amenities-list-item small">
                        <div class="amenities-list-text">2 and 3 bedroom units include: Wrap around terracesCorner sliding doors to main terrace</div>
                    </li>
                    <li class="amenities-list-item small">
                        <div class="amenities-list-text">1 bedroom units include: 2 full bathrooms</div>
                    </li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="amenities-gallery-wrapper"><img class="amenities-img" sizes="(max-width: 767px) 100vw, 46vw" src="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-3.jpg" srcset="<?=THEME_PATH?>/vmarina/images/bg-Vmarina-3-p-500.jpeg 500w, <?=THEME_PATH?>/vmarina/images/bg-Vmarina-3.jpg 800w"></div>
</div>

<div class="amenities-wrapper" id="amenities">
    <div class="specs-gallery-wrapper">
        <img class="amenities-img" src="<?=THEME_PATH?>/vmarina/images/amenities-vmarina.jpg"></div>
    <div class="specs-details-wrapper">
        <h2 class="heading-amenities">Common Area Amenities</h2>
        <h3 class="heading-amenities-subtitle">Beautifully designed common spaces with modern amenities for a quiet yet lively and stylish community living. Our staff provides full time cleaning and maintenance of common areas, gardening and security.</h3>
        <div class="amenities-list-wrapper">
            <div class="amenities-list-left">
                <ul class="amenities-list">
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">2 spectacular entry lobbies</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Covered motor lobby entrances</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Natural Stone Pool Terrace</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Resort style pool area</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Chef attended Pool Bar &amp; Grill</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Fully equipped Gym</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Game room w/ pool table and seating area</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">24 Hour security personnel</div>
                    </li>
                    <li class="amenities-list-item">
                        <div class="amenities-list-text">Controlled access and 24 Hour monitoring</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="reserve-wrapper" id="reserve-now" style="margin-top: 45px; margin-bottom: 10px;">
    <div class="reserve-inner">
        <h3 class="reserve-heading" data-ix="scroll-animation">PRESALE</h3>
        <h1 class="reserve-subtitle" data-ix="scroll-animation">Get Complete Information</h1>
        <div class="reserve-form-wrapper" data-ix="scroll-animation">
            <form id="contact_form" action="<?=get_permalink()?>" method="post" autocomplete="off" class="ga-track-submit" data-ga-category="Website" data-ga-action="form" data-ga-label="Presale">
                <label class="input-label" for="name">Your Name *</label>
                <input id="reserve-now-input" class="form-input w-input" maxlength="256" name="f_name" required="required" type="text">
                <label class="input-label" for="email">Your Email *</label>
                <input class="form-input w-input"  maxlength="256" name="f_email" required="required" type="email">
                <input class="cmd" type="hidden" name="f_cmd" value="" />
                <div class="form-input-wrapper">
                    <input class="form-submit w-button w-hidden-tiny" data-wait="Please wait..." type="submit" value="Full Access Availability and Prices">
                    <input class="form-submit w-button w-hidden-main w-hidden-medium w-hidden-small" data-wait="Please wait..." type="submit" value="Full Access">
                </div>
            </form>
            <div class="form-success w-form-done">
                <div>Thank you! We will contact you soon!</div>
            </div>
        </div>
    </div>
</div>
<?php /*<div class="info-wrapper">
    <a class="info-action ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Link VMARINA" href="http://www.vmarina.mx" target="_blank">FOR MORE INFORMATION VISIT VMARINA.MX</a>
</div> */ ?>
<div class="footer-wrapper">
    <div class="footer-wrapper-inner">
        <div class="footer-col">
            <a class="brand-footer w-inline-block ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Link VMARINA footer">
                <img class="brand-footer-img" src="<?=THEME_PATH?>/vmarina/images/logo-vmarina-white.svg">
            </a>
        </div>
        <?php /* <div class="footer-col">
            <div class="footer-label">Call us at</div>
            <div class="footer-label yellow">
                <a href="tel:3221801616" class="footer-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Phone VMARINA" >(322) 180 16 16</a>
            </div>
            <div class="footer-spacer"></div>
            <div class="footer-label">Or email us</div>
            <div class="footer-label">
                <a class="footer-link ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Email VMARINA" href="mailto:sales@vmarina.mx">sales@vmarina.mx</a>
            </div>
        </div> */ ?>
        <div class="footer-col" style="text-align: center;">
            <div class="footer-label">Address</div>
            <div class="footer-label short yellow" style="text-align: center;">Av. Paseo de la Marina 121, Marina Vallarta, 48450 Puerto Vallarta Jalisco, México.</div>
        </div>
        <div class="footer-col last">
            <a class="brand-footer-sb w-inline-block ga-track" data-ga-category="Website" data-ga-action="click" data-ga-label="Link SBPVR">
                <img class="brand-footer-img" src="<?=THEME_PATH?>/vmarina/images/logo-sb-white.svg">
            </a>
        </div>
    </div>
    <div class="footer-copyright">The developer reserves the right to modify colors, materials, features, amenities, floor plans, prices, conditions and services at any time without notice.</div>
</div>


    


<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<script src="<?=THEME_PATH?>/vmarina/js/webflow.js" type="text/javascript"></script>
<!-- google maps -->
<script type="text/javascript">
    var base_url = '<?=THEME_PATH?>/';
    var _IS_ADMIN = <?=is_user_logged_in() ? "true":"false";?>;
</script>    
<script src="<?=THEME_PATH?>/vmarina/js/scripts.js?v=<?=THEME_VERSION?>"></script>


</body>
</html>