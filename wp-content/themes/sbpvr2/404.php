<?php
/*
Template Name: SB Concierge
*/
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>

<div class="animated-list item-1 item-404" id="animated-list-1">
    <div class="container full w-clearfix">
        <div class="section-container-404 centered404">
            <div class="section-404">
                    <h2 class="list-item-title center">
                        <span>404</span>
                        <?=pll__('Page not found')?><br>
                        <a href="<?=pll_home_url()?>" class="to-home-404">
                            <?=pll__('Take me back to home')?>
                        </a>
                    </h2>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>