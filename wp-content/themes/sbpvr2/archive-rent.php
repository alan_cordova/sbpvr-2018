<?php
    $property_types = get_taxonomy_list('property-type', 'rent');
    $development_types = get_taxonomy_list('development', 'rent');
    $location_types = get_taxonomy_list('location', 'rent');

    $post_type = get_post_type();
    $title = pll__($post_type);

    $is_ajax = isset($_GET['ajax_search']) ? true : false;

    //lower price
    $args = array(
        'post_type' => 'rent',
        'posts_per_page' => 1,
        'orderby' => 'meta_value_num',
        'meta_key' => 'c_price',
        'order' => 'ASC'
    );
    $lower_query = new WP_Query($args);
    $lower = 0;
    if($lower_query->post){
        $lower = get_field('c_price', $lower_query->post->ID);
    }

    //higher price
    $args = array(
        'post_type' => 'rent',
        'posts_per_page' => 1,
        'orderby' => 'meta_value_num',
        'meta_key' => 'c_price',
        'order' => 'DESC'
    );
    $higher_query = new WP_Query($args);
    $higher = 0;
    if($higher_query->post){
        $higher = get_field('c_price', $higher_query->post->ID);
    }


    $paged = get_query_var('paged');  
    $paged = ($paged == 0) ? 1 : $paged;
?>
<?php if(!$is_ajax):?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>
<?php endif;?>
<?php if(!$is_ajax): //ajax filter?>
<div id="properties-search-container">
    <div class="result-section">
        <div class="side-search w-hidden-small w-hidden-tiny">
            <img src="<?=THEME_PATH?>/images/sb_1.svg" height="120" class="image-3">
            <div class="div-block-11">
                <div class="prop-search">
                    <form class="properties-search-form" data-current-page="<?=$paged?>" action="<?=get_post_type_archive_link('rent');?>">
                        <div class="property-input-container">
                            <input id="f_search" class="property-input w-input" maxlength="256" name="search" placeholder="<?=pll__('Search')?>..." type="text">
                        </div>
                        <div class="property-select-container">
                            <a class="property-select-button w-inline-block" href="#">
                                <div class="property-default-option"><?=pll__('Property Type')?></div>
                                <div id="f_property_type" class="property-selected-option"
                                    data-term-id="">
                                    <?=pll__('All types')?>
                                </div>
                                <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                            </a>
                            <div class="select-options-container">
                                <a class="property-select-option w-button" href="#" 
                                    data-term-id="">
                                    <?=pll__('All types')?>
                                </a>
                                <?php foreach($property_types as $k=>$term):?>
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="<?=$term->term_id?>">
                                        <?=$term->name?>
                                    </a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="property-select-container">
                            <a class="property-select-button w-inline-block" href="#">
                                <div class="property-default-option"><?=pll__('Development')?></div>
                                <div id="f_development" class="property-selected-option"
                                    data-term-id="<?=isset($_GET['devID'])? htmlspecialchars($_GET['devID']):''?>">
                                    <?=pll__('All developments')?>
                                </div>
                                <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                            </a>
                            <div class="select-options-container">
                                <a class="property-select-option w-button" href="#" 
                                    data-term-id="">
                                    <?=pll__('All developments')?>
                                </a>
                                <?php foreach($development_types as $k=>$term):?>
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="<?=$term->term_id?>">
                                        <?=$term->name?>
                                    </a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="property-select-container">
                            <a class="property-select-button w-inline-block" href="#">
                                <div class="property-default-option"><?=pll__('Location')?></div>
                                <div id="f_location" class="property-selected-option"
                                    data-term-id=""><?=pll__('All locations')?></div>
                                <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                            </a>
                            <div class="select-options-container">
                                <a class="property-select-option w-button" href="#" 
                                    data-term-id="">
                                    <?=pll__('All locations')?>
                                </a>
                                <?php foreach($location_types as $k=>$term):?>
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="<?=$term->term_id?>">
                                        <?=$term->name?>
                                    </a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <input class="properties-search-btn button search w-button" type="submit" value="<?=pll__('Search')?>">
                    </form>
                </div>
            </div>
        </div>
        <div class="side-search-mobil">
            <div class="v2-container-elements-search-mobil">
                <div class="header-search-mobil w-hidden-main w-hidden-medium">
                    <div class="title-header-search-mobil"><?=pll__('Search')?></div>
                    <div class="btn-close-search-mobil">
                        <div class="top-line-mobil"></div>
                        <div class="bottom-line-mobil"></div>
                    </div>
                </div>
                <img src="<?=THEME_PATH?>/images/sb_1.svg" height="120" class="image-3">
                <div class="div-block-11">
                    <div class="prop-search">
                        <form class="properties-search-form" data-current-page="<?=$paged?>" action="<?=get_post_type_archive_link('property');?>">
                            <div class="property-input-container">
                                <input id="f_search" class="property-input w-input" maxlength="256" name="search" placeholder="<?=pll__('Search')?>..." type="text">
                            </div>
                            <div class="property-select-container">
                                <a class="property-select-button w-inline-block" href="#">
                                    <div class="property-default-option"><?=pll__('Property Type')?></div>
                                    <div id="f_property_type" class="property-selected-option"
                                        data-term-id="">
                                        <?=pll__('All types')?>
                                    </div>
                                    <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                                </a>
                                <div class="select-options-container">
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="">
                                        <?=pll__('All types')?>
                                    </a>
                                    <?php foreach($property_types as $k=>$term):?>
                                        <a class="property-select-option w-button" href="#" 
                                            data-term-id="<?=$term->term_id?>">
                                            <?=$term->name?>
                                        </a>
                                    <?php endforeach;?>
                                </div>
                            </div>
                            <div class="property-select-container">
                                <a class="property-select-button w-inline-block" href="#">
                                    <div class="property-default-option"><?=pll__('Development')?></div>
                                    <div id="f_development" class="property-selected-option"
                                        data-term-id="<?=isset($_GET['devID'])? htmlspecialchars($_GET['devID']):''?>">
                                        <?=pll__('All developments')?>
                                    </div>
                                    <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                                </a>
                                <div class="select-options-container">
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="">
                                        <?=pll__('All developments')?>
                                    </a>
                                    <?php foreach($development_types as $k=>$term):?>
                                        <a class="property-select-option w-button" href="#" 
                                            data-term-id="<?=$term->term_id?>">
                                            <?=$term->name?>
                                        </a>
                                    <?php endforeach;?>
                                </div>
                            </div>
                            <div class="property-select-container">
                                <a class="property-select-button w-inline-block" href="#">
                                    <div class="property-default-option"><?=pll__('Location')?></div>
                                    <div id="f_location" class="property-selected-option"
                                        data-term-id=""><?=pll__('All locations')?></div>
                                    <img class="select-arrow" src="<?=THEME_PATH?>/images/icon-arrow-down-white.svg" alt="">
                                </a>
                                <div class="select-options-container">
                                    <a class="property-select-option w-button" href="#" 
                                        data-term-id="">
                                        <?=pll__('All locations')?>
                                    </a>
                                    <?php foreach($location_types as $k=>$term):?>
                                        <a class="property-select-option w-button" href="#" 
                                            data-term-id="<?=$term->term_id?>">
                                            <?=$term->name?>
                                        </a>
                                    <?php endforeach;?>
                                </div>
                            </div>
                            <div class="property-slider-container w-hidden-medium w-hidden-small w-hidden-tiny">
                                <label class="property-slider-label" for="node"><?=pll__('Price Range (USD)')?></label>
                                <input id="f_price_range" 
                                    class="property-search-hidden-input w-input" 
                                    id="node" 
                                    maxlength="256" 
                                    data-lower-price="<?=$lower?>"
                                    data-higher-price="<?=$higher?>"
                                    type="text">
                            </div>
                            <input class="properties-search-btn button search w-button" type="submit" value="<?=pll__('Search')?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-items" id="properties-grid">
            <div class="header-results-properties">
                <div class="result-title">
                    <h1 class="heading-7"><?=$title?></h1>
                </div>
                <div class="v2-container-filters-search">
                    <div class="v2-btn-search-mobil">
                        <div><?=pll__('Search')?></div>
                    </div>
                    <div class="btn-select-properties">  
                        <select name="f_price_filter" class="v2-filter-properties w-select prop-order-filter">
                            <option value="price-asc" class="v2-item-filter" selected="selected"><?=pll__('Highest price')?></option>
                            <option value="price-desc" class="v2-item-filter"><?=pll__('Lowest price')?></option>
                            <option value="alphabetic-asc" class="v2-item-filter"><?=pll__('Alphabetic A-Z')?></option>
                            <option value="alphabetic-desc" class="v2-item-filter"><?=pll__('Alphabetic Z-A')?></option>
                            <option value="recent" class="v2-item-filter"><?=pll__('Recently added')?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="properties-renderer" class="result-items-row">
    <?php endif; //ajax filter end?>
                <?php foreach($posts as $post):?>
                    <?php setup_postdata($post);?>
                    <?php $property = get_property_info(true);?>
                    <?php $featured = ($property->gallery) ? $property->gallery[0]['sizes']['img-property-agent-330x250'] :  false; ?>
                    <a href="<?=get_permalink()?>" class="result-item w-inline-block">
                        <div class="v2-information-hover-property">
                            <h3 class="v2-hover-title-property-list"><?php the_title()?></h3>
                            <?php if($property->location):?>
                                <h4 class="v2-hover-location-property-list"><?=$property->location->name?></h4>
                            <?php endif;?>
                            <div class="v2-hover-specs-property-list">
                                <?php if($property->beds):?>
                                    <div class="v2-hover-specs-numbers"><?=$property->beds?></div>
                                    <div class="v2-hover-specs"><?=pll__('Beds')?></div>
                                <?php endif;?>
                                <?php if($property->baths):?>
                                    <div class="v2-hover-specs-numbers"><?=$property->baths?></div>
                                    <div class="v2-hover-specs"><?=pll__('Baths')?></div>
                                <?php endif;?>
                                <?php if($property->sqft):?>
                                    <div class="v2-hover-specs-numbers"><?=number_format($property->sqft)?></div>
                                    <div class="v2-hover-specs">sq ft</div>
                                <?php endif;?>
                            </div>
                            <div class="v2-hover-container-btn-more">
                                <div class="v2-hover-btn-more">
                                    <div class="v2-icon-more">+</div>
                                </div>
                            </div>
                        </div>
                        <div class="v2-img-property-list" style="background-image: url(<?=$featured?>)"> 
                            <img src="<?=THEME_PATH?>/images/img_default.png">
                        </div>
                        <div class="v2-information-property-text">
                            <h3 class="v2-title-property-list"><?php the_title()?></h3>
                            <?php if($property->location):?>
                                <h4 class="heading-8"><?=$property->location->name?></h4>
                            <?php endif;?>
                        </div>
                    </a>
                <?php endforeach;?>
    <?php if(!$is_ajax): //ajax filter?>
            </div>
            <a id="load-properties-mobile" class="link-4" href="javascript:;">
                <div><?=pll__("Load more properties")?></div>
            </a>

            <div id="properties-loader" class="content-load-more">
                <div class="properties-loader-container">
                    <?=pll__('Loading properties')?> <img src="<?=THEME_PATH?>/images/loading.gif" alt="" />
                </div>
            </div>

            <div id="properties-not-found" class="content-load-more">
                <div
                    data-not-found-start="<?=pll__('0 properties found')?>"
                    data-not-found-final="<?=pll__('No more properties to show')?>">
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
<?php endif; //end ajax filter?>