var initMaps;

var Webflow = Webflow || [];
Webflow.push(function() {

    //webflow interactions
    Webflow.require('ix').init([
        {"slug":"show-menu","name":"show-menu","value":{"style":{},"triggers":[{"type":"click","selector":".side-menu","preserve3d":true,"stepsA":[{"display":"flex","transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[{"transition":"transform 200 ease 0","x":"-400px","y":"0px","z":"0px"},{"display":"none"}]}]}},
        {"slug":"ocultarmenu","name":"ocultarmenu","value":{"style":{"display":"none","x":"-100px","y":"0px","z":"0px"},"triggers":[]}},
        {"slug":"menu-transform","name":"Menu transform","value":{"style":{},"triggers":[{"type":"click","selector":".top-line","preserve3d":true,"stepsA":[{"title":"Top line move","transition":"transform 200 ease 0","x":"0px","y":"3px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"45deg"}],"stepsB":[{"transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"0deg"}]},{"type":"click","selector":".middle-line","stepsA":[{"display":"none"}],"stepsB":[{"display":"inline"}]},{"type":"click","selector":".bottom-line","preserve3d":true,"stepsA":[{"transition":"transform 200 ease 0","x":"0px","y":"-3px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"-45deg"}],"stepsB":[{"transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"0deg"}]},{"type":"click","selector":".side-menu","preserve3d":true,"stepsA":[{"display":"flex","transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[{"transition":"transform 200 ease 0","x":"-400px","y":"0px","z":"0px"},{"display":"none"}]}]}},
        {"slug":"menu-transform-movil","name":"Menu transform movil","value":{"style":{},"triggers":[{"type":"click","selector":".top-line","preserve3d":true,"stepsA":[{"title":"Top line move","transition":"transform 200 ease 0","x":"0px","y":"4px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"45deg"}],"stepsB":[{"transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"0deg"}]},{"type":"click","selector":".middle-line","stepsA":[{"display":"none"}],"stepsB":[{"display":"inline"}]},{"type":"click","selector":".bottom-line","preserve3d":true,"stepsA":[{"transition":"transform 200 ease 0","x":"0px","y":"-2px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"-45deg"}],"stepsB":[{"transition":"transform 200 ease 0","x":"0px","y":"0px","z":"0px","rotateX":"0deg","rotateY":"0deg","rotateZ":"0deg"}]}]}},
        {"slug":"show-modal","name":"Show-modal","value":{"style":{},"triggers":[{"type":"click","selector":".modal-bg","stepsA":[{"display":"flex","opacity":1,"transition":"opacity 200 ease 0"}],"stepsB":[]}]}},
        {"slug":"close-modal","name":"close-modal","value":{"style":{},"triggers":[{"type":"click","selector":".modal-bg","stepsA":[{"display":"none"}],"stepsB":[]}]}}
    ]); 
    //end webflow interations

    // browser specific
    var viewport, viewport_p;
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    
    var isMobile = false; //initiate as false
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        isMobile = true;
    }

    /*** SMOOTH SCROLLING ***/
    var $window = $(window);
    var scrollTime = 1.2;
    var scrollDistance = 400;
    var bodyScroll = true;
    var scrollable_element = $window;

    function set_scrollable_element(elem) {
        scrollable_element = elem;
    }

    function unset_scrollable_element() {
        scrollable_element = $window;
    }

    var scrollTween;
    function event_scroll_move(elem, event){
        var delta = event.originalEvent.wheelDelta / 70 || -event.originalEvent.detail / 3;
        var scrollTop = elem.scrollTop();
        var finalScroll = scrollTop - parseInt(delta * scrollDistance);
        TweenMax.to(elem, scrollTime, {
            scrollTo: {
                y: finalScroll,
                autoKill: true
            },
            ease: Power1.easeOut,
            overwrite: 5
        });
    }

    // mouse scroll events
    var outsideSearch = true;
    $("body").on("mousewheel DOMMouseScroll", function(event) {
        //event.preventDefault();
        if (!bodyScroll) { 
            event.preventDefault();
            event_scroll_move(scrollable_element, event);
        }
        outsideSearch = true;
    });

    //properties scroll handler
    
    var scrolling_properties = false;
    var scrollSearchEnabled = false;
    var scrollingToSearch = false;

    function get_viewport() {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        return {
            w: w,
            h: h
        };
    }

    viewport = get_viewport();
    Webflow.resize.on(function() {
        viewport = get_viewport();
        if ($('.nav-menu-2').hasClass('w--nav-menu-open')) {
            $('.top-line').css({
                'transform' : 'translateX(0px) translateY(0px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg)',
            });
            $('.middle-line').css({
                'display' : 'inline',
                'transform' : 'translateX(0px) translateY(0px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg)',
            });
            $('.bottom-line').css({
                'transform' : 'translateX(0px) translateY(0px) translateZ(0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg)',
            });
        }
        $(".side-menu").css({
            'display' : 'none',
            'transform' : 'translateX(-400px) translateY(0px) translateZ(0px)',
        });
        $('.nav-menu-2').removeClass('w--nav-menu-open');
    });

    $('.side-menu').height($(document).height() - 72);
    $('.btn-custom-menu').on('click', function(){
        $('.side-menu').toggleClass('v2-hide-menu');
    });
    $('.v2-btn-search-mobil').on('click', function(){
        $('.v2-container-elements-search-mobil').fadeIn();
        $('.side-search-mobil').animate({
          width: '100%'
        }, 300);
    });
    $('.btn-close-search-mobil').on('click', function(){
        $('.v2-container-elements-search-mobil').fadeOut();
        $('.side-search-mobil').animate({ 
          width: '0'
        }, 300);
    });
    $('.v2-btn-get-touch-agent').on('click', function(){
        $('.v2-modal-form-agent').css({opacity: 0, display: 'flex'}).animate({
            opacity: 1
        }, 300);
    });
    $('.btn-close-form-agent').on('click', function(){
        $('.v2-modal-form-agent').fadeOut();
    });
    $('.result-item').mouseenter(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).css({opacity: 0, display: 'flex'}).animate({
        opacity: 1
      }, 300);
    }).mouseleave(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).fadeOut();
    });
    $('.v2-similar-property').mouseenter(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).css({opacity: 0, display: 'flex'}).animate({
        opacity: 1
      }, 300);
    }).mouseleave(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).fadeOut();
    });
    $('.v2-property-agent').mouseenter(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).css({opacity: 0, display: 'flex'}).animate({
        opacity: 1
      }, 300);
    }).mouseleave(function(){
      var infoHover = $(this).find('.v2-information-hover-property');
      $(infoHover).fadeOut();
    });
    $('.v2-btn-get-in-touch').on('click', function(e){
        e.preventDefault();
        $('.modal-bg').fadeIn();
    });
    $('.v2-contact-member').on('click', function(e){
        e.preventDefault();
        var emailMember = $(this).data('email');
        $('.modal-bg').fadeIn('fast', function() {
            $(this).find('.v2-member-email').val(emailMember);
        });
    });
    $('.v2-btn-amenities').on('click', function(e){
        e.preventDefault();
        var features = $('.v2-features-modal').html();
        $('.v2-features-modal-bg').fadeIn();
    });
    $('.v2-btn-contact-agent').on('click', function(e){
        e.preventDefault();
        var formAgent = $('.v2-form-agent').html();
        $('.v2-contact-agent-modal-bg').fadeIn();
    });
    $('.v2-close-modal').on('click', function(){
        $('.modal-bg').fadeOut();
        $('.v2-features-modal-bg').fadeOut();
        $('.v2-contact-agent-modal-bg').fadeOut();
        $('.v2-container-iframe-video').html('');
        $('.w-form-done').fadeOut();
        $('.w-form-fail').fadeOut();
    });

    $('.v2-virtual-tour').on('click', function(){
        $('.modal-bg').fadeIn();
    });
    $('.v2-show-specs').on('click', function(){
        $('.modal-bg').fadeIn();
        if (_LANG == 'en') {
            var videoURL = '55725258';
        }else{
            var videoURL = '55725257';
        }
        $('.v2-container-iframe-video').html("<iframe src='https://player.vimeo.com/video/"+videoURL+"?autoplay=1' width='100%' height='550' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");
    });

    var dropdown_timeouts = [];
    $(".v2-dropdown-item-menu").mouseenter(function(e) {
        if(typeof dropdown_timeouts[$(this).index()] != "undefined"){
            clearTimeout(dropdown_timeouts[$(this).index()]);
        }

        $(this).find('.dropdown-toggle').addClass('w--open');
        $(this).find('.dropdown-list').addClass('w--open');
        dropdown_list_position_handler();
    });
    $(".v2-dropdown-item-menu").mouseleave(function(e){
        var _this = $(this);
        dropdown_timeouts[$(this).index()] = setTimeout(function(){
            _this.find('.dropdown-toggle').removeClass('w--open');
            _this.find('.dropdown-list').removeClass('w--open');
        }, 150);
    });

    //submenu space on small windows
    function dropdown_list_position_handler() {
        $(".dropdown-list").each(function() {
            var width = calculate_dropdown_list_width($(this));
            var dropdown_link_width = $(this).closest('.v2-dropdown-item-menu').outerWidth();
            var dropdown_link_offset = $(this).closest('.v2-dropdown-item-menu').offset();
            var repositioned = false;
            if (viewport.w > 991) {
                //sets dropdown width
                $(this).css('width', width + 'px');
                //sets new offset according to dropdown position
                var new_offset = (width / 2) - (dropdown_link_width / 2);
                $(this).css('left', '-' + new_offset + 'px');
                repositioned = true;
                var viewport_with_dropdown = dropdown_link_offset.left + (width / 2) + 1 + (dropdown_link_width / 2);
                if (viewport.w < viewport_with_dropdown) {
                    var remaining = viewport_with_dropdown - viewport.w;
                    $(this).css('left', '-' + (new_offset + remaining + 15) + 'px');
                }
            }
            if (!repositioned) {
                $(this).css('left', '');
                $(this).css('width', '');
            }
        });
    }

    function calculate_dropdown_list_width(dropdown) {
        var width = 5;
        dropdown.find('.dropdown-link').each(function() {
            width += $(this).outerWidth();
        });
        return Math.ceil(width) + 1;
    }

    // contact form
    var sending_contact_form = false;
    $("#contact_form").submit(function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.cmd').val('7764');
        if(!sending_contact_form){
            sending_contact_form = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-block-2').find(".w-form-done, .w-form-fail").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.closest('.form-block-2').find('.w-form-done').fadeIn();
                }else{
                    form.closest('.form-block-2').find('.w-form-fail').fadeIn();
                }
                sending_contact_form = false;
                form.trigger("reset");
            },'json');
        }
    });

    // contact from property
    var sending_property_contact = false;
    $("#contact_from_property").submit(function(e){
        e.preventDefault();
        var form = $(this);
        var mandarina = $(form).find(".mandarina_property").val();
        form.find(".cmd").val('33551');
        if (mandarina) {
            if(!sending_property_contact){
                if ($(".f_accept").is(':checked')) {
                    sending_property_contact = true;
                    var action = $(this).attr('action');
                    var req = $(this).serializeArray();

                    form.closest('.form-block-2').find(".w-form-done, .w-form-fail").hide();
                    $.post(action, req, function(data){
                        if(data.success){
                            form.closest('.form-block-2').find('.w-form-done').fadeIn();
                        }else{
                            form.closest('.form-block-2').find('.w-form-fail').fadeIn();
                        }
                        sending_property_contact = false;
                        form.trigger("reset");
                    },'json');
                }else{
                    $('.w-form-done').fadeOut();
                    $('.w-form-fail').fadeIn();
                    setTimeout(function() {
                        $('.w-form-fail').fadeOut();
                    }, 3000 );
                    return false;
                }
            }
        }else{
            if(!sending_property_contact){
                sending_property_contact = true;
                var action = $(this).attr('action');
                var req = $(this).serializeArray();

                form.closest('.form-block-2').find(".w-form-done, .w-form-fail").hide();
                $.post(action, req, function(data){
                    if(data.success){
                        form.closest('.form-block-2').find('.w-form-done').fadeIn();
                    }else{
                        form.closest('.form-block-2').find('.w-form-fail').fadeIn();
                    }
                    sending_property_contact = false;
                    form.trigger("reset");
                },'json');
            }else{
                $('.w-form-done').fadeOut();
                $('.w-form-fail').fadeIn();
                setTimeout(function() {
                    $('.w-form-fail').fadeOut();
                }, 3000 );
                return false;
            }
        }
    });

    //constant contact suscribe newsletter
    var suscribing = false;
    $("#newsletter-form").submit(function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.cmd').val('9871');
        if(!suscribing){
            suscribing = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();
            $.post(action, req, function(data){
                if(data.success){
                    form.fadeOut(function(){
                        $(".newsletter-success").fadeIn();
                    });
                    suscribing = false;
                    form.trigger("reset");
                }
            },'json');
        }
    });

    /** CONTACT MAP **/
    initMaps = function() {
        var map_elem = $("#map-container");
        if (map_elem.length < 1) {
            return 0;
        }

        var title = map_elem.data('marker-title');

        var marker_url = map_elem.data('marker');
        var latLng = {
            lat: parseFloat(map_elem.data('lat')),
            lng: parseFloat(map_elem.data('lng'))
        };
        var pos = new google.maps.LatLng(latLng.lat, latLng.lng);
        var icon = {
            url: marker_url, // url
            scaledSize: new google.maps.Size(100, 100), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(50, 100) // anchor
        };
        map = new google.maps.Map(document.getElementById('map-container'), {
            center: pos,
            position: pos,
            zoom: 15,
            styles: map_styles,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: pos,
            title: title,
            icon: icon
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    };
    /** END CONTACT MAP **/

    /*** TAKE ME THERE MAP ***/
    $('.v2-link-take-me').on('click', function(){
        if((navigator.platform.indexOf("iPhone") != -1) || (navigator.platform.indexOf("iPad") != -1) || (navigator.platform.indexOf("iPod") != -1)){
            window.open("maps://maps.google.com/maps?daddr=20.6628591,-105.2522796");
        }else{
            window.open("https://maps.google.com/maps?daddr=20.6628591,-105.2522796");
        }
    });
    $('.btn-details-map, .btn-details-map-black, .btn-details-map-black-movil').on('click', function(){
        var lat = $(this).data('lat');
        var lng = $(this).data('lng');
        if((navigator.platform.indexOf("iPhone") != -1) || (navigator.platform.indexOf("iPad") != -1) || (navigator.platform.indexOf("iPod") != -1)){
            window.open("maps://maps.google.com/maps?daddr="+lat+","+lng);
        }else{
            window.open("https://maps.google.com/maps?daddr="+lat+","+lng);
        }
    });

    /** SEARCH INTERACTIONS **/
    function close_all_select_buttons() {
        $('.property-select-container').each(function() {
            close_select_button($(this));
        });
    }

    function open_select_button(select_container) {
        var button = select_container.find('.property-select-button');
        if (button.data('open') && button.data('open') == 1) {
            close_select_button(select_container);
        } else {
            close_all_select_buttons();
            button.data('open', 1);
            button.find('.select-arrow').addClass('open');
            button.closest('div').find('.select-options-container').fadeIn('fast');
        }
    }

    function close_select_button(select_container) {
        var button = select_container.find('.property-select-button');
        button.data('open', '');
        button.find('.select-arrow').removeClass('open');
        button.closest('div').find('.select-options-container').fadeOut('fast');
    }

    $(".property-select-button").click(function() {
        var select_container = $(this).closest('.property-select-container');
        open_select_button(select_container);
    });

    $(".property-select-option").click(function() {
        var term_text = $(this).text();
        var taxonomy = $(this).data('term-taxonomy');
        var taxonomy_id = $(this).data('term-id');

        var select_container = $(this).closest('.property-select-container');
        select_container.find('.property-default-option').css('visibility', 'hidden');
        select_container.find('.property-selected-option').text(term_text);
        select_container.find('.property-selected-option').attr('data-term-taxonomy', taxonomy)
        select_container.find('.property-selected-option').attr('data-term-id', taxonomy_id);
        select_container.find('.property-selected-option').show();
        close_select_button(select_container);
    });

    if($("#f_development").length > 0){
        //if devID is sent select option for development choose
        var f_dev_term_id = $("#f_development").data('term-id');
        if(f_dev_term_id != ""){
            $("#f_development").closest('.property-select-container').find('.property-select-option').each(function(){
                if(f_dev_term_id == $(this).data('term-id')){
                    $(this).trigger('click');
                }
            });
        }
    }

    function search_prepare_parameters(form){
        var req = {
            f_search: $("#f_search").val(),
            f_property_type_tax_id: form.find("#f_property_type").attr('data-term-id'),
            f_development_tax_id: form.find("#f_development").attr('data-term-id'),
            f_location_tax_id: form.find("#f_location").attr('data-term-id'),
            f_price_range: form.find('#f_price_range').val()
        }
        return req;
    }

    function search_reset_pagination(){
        $(".properties-search-form").data('current-page', 0);
    }

    function search_increase_pagination(){
        var next = parseInt($(".properties-search-form").data('current-page')) + 1;
        $(".properties-search-form").data('current-page', next);
    }

    function search_render(res){
        if(parseInt($(".properties-search-form").data('current-page')) == 1){
            $("#properties-renderer").html(res);
        }else{
            $("#properties-renderer").append(res);
        }
    }

    $(".properties-search-form").on('submit', function(e){
        e.preventDefault();
        search_reset_pagination();
        $('.v2-container-elements-search-mobil').fadeOut();
        var form = $(this);
        search_not_found = false;
        $("#properties-renderer").html('');
        $('#properties-renderer').stop(true, true).animate({
            scrollTop: 0
        }, 0);
        show_search_loader();
        show_load_properties_button();
        properties_form_request(form);
        if(viewport.w <= 991){
            $("#mobile-properties-close-trigger").trigger('click');
        }
    });

    if($(".prop-order-filter").length > 0){
        $(".prop-order-filter").change(function(){
            var val = $(this).val();
            $("#properties-renderer").html('');
            $(".properties-search-form").trigger('submit');
            search_increase_pagination();
        });
    }

    var search_not_found = false;
    var requesting_form_properties = false;
    function properties_form_request(form){
        if(!requesting_form_properties && !search_not_found){
            show_search_loader();
            requesting_form_properties = true;
            search_increase_pagination();
            var req = search_prepare_parameters(form);

            //if has ordering filter
            if($(".prop-order-filter").length > 0){
                var order_filter = $(".prop-order-filter").val();
                req.f_order_filter = order_filter;
            }

            req.ajax_search = true;

            var url = form.prop('action');
            var page = form.data('current-page');
            var request_url = url + '/page/' + page;

            //bodyScroll = false;

            $.get(request_url, req, function(res){
                search_render(res);
                requesting_form_properties = false;
                hide_search_loader();
                $('.result-item').mouseenter(function(){
                  var infoHover = $(this).find('.v2-information-hover-property');
                  $(infoHover).css({opacity: 0, display: 'flex'}).animate({
                    opacity: 1
                  }, 300);
                }).mouseleave(function(){
                  var infoHover = $(this).find('.v2-information-hover-property');
                  $(infoHover).fadeOut();
                });
                //setTimeout(function(){ bodyScroll = true; }, 650);

                if(res.trim() == ''){
                    properties_not_found_msg(page);
                    hide_load_properties_button();
                }

                resize_calculate_duration();
            }).fail(function(){
                requesting_form_properties = false;
                //setTimeout(function(){ bodyScroll = true; }, 650);
                search_not_found = true;
                hide_search_loader();
                properties_not_found_msg(page);
                hide_load_properties_button();

                resize_calculate_duration();
            });
        }
    }

    function properties_not_found_msg(page){
        //shows the not found text
        not_found_text = '';
        if(page == 1){
            not_found_text = $("#properties-not-found div").data('not-found-start');
        }else{
            not_found_text = $("#properties-not-found div").data('not-found-final');
        }
        setTimeout(function(){
            $("#properties-not-found div").text(not_found_text);
            $("#properties-not-found").show(function(){
                resize_calculate_duration();
                var elem = $(this);
                setTimeout(function(){
                    elem.fadeOut('slow', function(){
                        resize_calculate_duration();
                    });
                }, 3000);
            });
        }, 1)
    }

    function set_search_loader_opacity(opacity){
        TweenMax.to("#properties-loader", 1, {css: {opacity: opacity}});
    }
    function show_search_loader(){
        set_search_loader_opacity(1);
    }
    function hide_search_loader(){
        set_search_loader_opacity(0);
    }
    function hide_load_properties_button(){
        $("#load-properties-mobile").hide();
    }
    function show_load_properties_button(){
        if(viewport.w <= 991){
            $("#load-properties-mobile").css('display','block');
        }
    }

    function properties_request_next(){
        var form = $(".properties-search-form");
        properties_form_request(form);
    }
    $("#load-properties-mobile").click(function(){
        properties_request_next();
    });

    var magicController = new ScrollMagic.Controller();
    var scrollingToSearch = false;
    if ($("#properties-search-container").length > 0) {
        var searchBoxScene = new ScrollMagic.Scene({
            triggerElement: "#properties-grid",
            triggerHook: 0,
            duration: ($("#properties-grid").outerHeight() - viewport.h) + 'px'
        })
        .on('leave', function(event){
            if(event.scrollDirection == "FORWARD"){
                properties_request_next();
            }
        })
        .on('update', function(event){
            ;  
        })
        //.addIndicators()
        .addTo(magicController);
            
        var responsivePosition = (viewport.w <= 991);
        var responsiveRedirecting = false;
        if(responsivePosition){
            responsiveSearchInterval = setInterval(function(){
                if(viewport.w > 991){
                    if(!responsiveRedirecting){
                        document.location = document.location;
                    }
                    responsiveRedirecting = true;
                }
            }, 1000);   
            
        }
    }

    $(".property-search-hidden-input").each(function() {
        var lower_price = $(this).data('lower-price');
        var higher_price = $(this).data('higher-price');
        var scale = [];
        scale.push(lower_price);
        var range = higher_price - lower_price;
        var jumps = Math.round(parseInt(range / 5));
        for(var i=1; i < 5; i++){
            scale.push(lower_price + (jumps*i));
        }
        scale.push(higher_price);

        $(this).css('display', 'block');
        $(this).attr('type', 'hidden');
        $(this).jRange({
            from: 0,
            to: higher_price,
            step: 1,
            scale: scale,
            format: '%s',
            width: '100%',
            showLabels: true,
            isRange: true
        });

        var prices_interval = setInterval(function(){
            if($(".property-slider-container .scale > span").length > 0){
                clearInterval(prices_interval);
                $(".property-slider-container .scale > span").each(function(index){
                    if (index != 0) {
                        var text = (parseInt($(this).find('ins').text())/1000).toFixed(3).replace('.', ',');
                        $(this).find('ins').text(text + 'k');
                    }
                });
            }
        }, 250);
    });

    function resize_calculate_duration(){
        if(typeof searchBoxScene != "undefined"){
            searchBoxScene.duration(0);
            var duration = $("#properties-grid").outerHeight() - viewport.h;
            searchBoxScene.duration(duration);
        }
    }
    /** END SEARCH INTERACTIONS **/

    var itemSideMenu = $('.w--current').closest('.v2-item-side-menu');
    itemSideMenu.css({
        'color' : '#000',
        'background-color' : '#fff'
    });

    /*****************************/
    /*** SEARCH PROPERTIES MAP ***/
    /*****************************/

    var initSearchMap = function(){
        var mapStarted = false;
        var searchMap;
        var searchMapElem = $("#searchMap");
        var searchMapWrapper = $("#searchMapWrapper");
        var closeTrigger = $(".searchMapClose");
        var openTrigger = $(".searchMapOpen");
        var inputFilterSale = $("#mapSearchSale");
        var inputFilterRent = $("#mapSearchRent");
        var inputFilterDev = $("#mapSearchDev");
        var mapItems = null;
        var requestingData = false;
        
        var mapSearchFilter;
        var bounded = false;
        var infoWindow;

        if(searchMapElem.length > 0){

            getMapData();

            openTrigger.click(showMap);
            closeTrigger.click(hideMap);

            /*$(document).on('click', '.mapInnerImgWrapper a, .mapInnerTitle', function(){
                analyticsMapPropertyDetails(mapItems[$(this).data('index')].title);
            });*/

            $(document).on('click', '#mapSearchSale, #mapSearchRent, #mapSearchDev', function(){
                if($(this).hasClass('disabled')){
                    $(this).removeClass('disabled');
                }else{
                    $(this).addClass('disabled');
                }
                processMap();
            });

            //check if map viewer was clicked on menu
            $(document).on('click', '.w-dropdown-link', function(e){
                var val = $(this).attr('href');
                var regexp = /\B\#\w\w+\b/g
                var mapLink = val.match(regexp);

                if(mapLink == "#mapSales"){
                    e.preventDefault();
                    $("#mapSearchSale").removeClass('disabled');
                    $("#mapSearchRent").addClass('disabled');
                    bounded = false;
                    showMap();
                }else if(mapLink == "#mapRentals"){
                    e.preventDefault();
                    $("#mapSearchRent").removeClass('disabled');
                    $("#mapSearchSale").addClass('disabled');
                    bounded = false;
                    showMap();
                }
            });

            function showMap(){
                bodyScroll = false;
                set_scrollable_element(searchMapWrapper);

                searchMapWrapper.fadeIn();
                startMap();
                processMap();
                fixSearchMapHeight();

                analyticsMapOpened();
            }

            function hideMap(){
                searchMapWrapper.fadeOut();
                unset_scrollable_element();
                bodyScroll = true;
            }

            function startMap(){
                if(!mapStarted){
                    mapStarted = true;

                    var latLng = {
                        lat: parseFloat(20.664348833879615),
                        lng: parseFloat(-105.25233127176762)
                    };
                    var pos = new google.maps.LatLng(latLng.lat, latLng.lng);
                    searchMap = new google.maps.Map(document.getElementById('searchMap'), {
                        //center: pos,
                        //position: pos,
                        zoom: 11,
                        scrollwheel: true,
                        gestureHandling: 'greedy'
                    });

                    infoWindow = new google.maps.InfoWindow();
            
                }
            }

            function processMap(){
                if(mapItems){
                    renderMap();
                }else{
                    getMapData().then(function(){
                        renderMap();
                    });
                }
            }

            function renderMap(){
                clearMap();
        
                var bounds = new google.maps.LatLngBounds();
                var resultsFound = false;

                mapSetSearchFilter();

                for(var i=0; i < mapItems.length; i++){
                    if(mapItems[i].show){
                        resultsFound = true;
                        mapItems[i].gmapPosition = new google.maps.LatLng(mapItems[i].position.lat, mapItems[i].position.lng);

                        mapItems[i].marker = new google.maps.Marker({
                            position: mapItems[i].gmapPosition,
                            icon: _THEME_PATH + '/images/map/' + mapItems[i].type + '-pin.png',
                            map: searchMap
                        });

                        addInfoWindow(i);

                        bounds.extend(mapItems[i].marker.getPosition());
                    }
                }

                if(!bounded && resultsFound){
                    bounded = true;
                    searchMap.fitBounds(bounds);
                }
                
            }

            function addInfoWindow(i) {
                google.maps.event.addListener(mapItems[i].marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '';
                        if(mapItems[i].img){
                            content +=  '<div class="mapInnerImgWrapper">' +
                                            '<a target="_blank" data-index="'+i+'" href="'+mapItems[i].permalink+'"><img src="' + mapItems[i].img + '" />' + '</a>' +
                                        '</div>';
                        }
                        content += '<a target="_blank" class="mapInnerTitle" data-index="'+i+'" href="'+mapItems[i].permalink+'">' + 
                                        mapItems[i].title + 
                                    '</a>';
                        content += '<div class="mapInnerDescription">' + mapItems[i].description + '</div>';
                        if(mapItems[i].price){
                            content += '<div class="mapInnerPrice">$' + 
                                            mapItems[i].price + ' <small>USD</small>' +
                                            '<div class="mapColorCircle ' + mapItems[i].type + ' "></div>' + 
                                        '</div>';
                        }


                        $.when(mapImgLoads(i)).then(function(){
                            infoWindow.setContent(content);
                            infoWindow.setOptions({ maxWidth: 250});
                            infoWindow.open(searchMap, marker);
                        });

                        analyticsMapOpenPin(mapItems[i].title);

                    }
                })(mapItems[i].marker, i));
            }

            function mapImgLoads(i){
                var $d = $.Deferred();

                var imgPath = mapItems[i].img;
                if(imgPath){
                    var img = new Image();
                    img.onload = function(){
                        $d.resolve();
                    };
                    img.src = imgPath;
                }else{
                    $d.resolve();
                }

                return $d.promise();
            }


            function clearMap(){
                for (var i = 0; i < mapItems.length; i++) {
                    if(mapItems[i].marker){
                        mapItems[i].marker.setMap(null);
                    }
                }
            }

            function mapSetSearchFilter(){
                mapSearchFilter = {};
                mapSearchFilter.saleAllowed = inputFilterSale.hasClass('disabled') ? false : true;
                mapSearchFilter.rentAllowed = inputFilterRent.hasClass('disabled') ? false : true;
                mapSearchFilter.devAllowed = inputFilterDev.hasClass('disabled') ? false : true;

                for(var i=0; i < mapItems.length; i++){
                    if(mapItems[i].type == 'permanent-sb' || (mapSearchFilter.saleAllowed && mapItems[i].type == 'property') || 
                        (mapSearchFilter.rentAllowed && mapItems[i].type == 'rent') ||
                        (mapSearchFilter.devAllowed && mapItems[i].type == 'development')){
                        mapItems[i].show = true;
                    }else{
                        mapItems[i].show = false;
                    }
                }

            }

            function getMapData(usePromise){
                return $.getJSON(_HOME_PAGE, {mapData: true}).done(function(res){
                    mapItems = res;
                });
            }

            function analyticsMapOpened() {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map opened' });
                }
            }
            
            function analyticsMapOpenPin(label) {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map pin: ' + label });
                }
            }

            function analyticsMapPropertyDetails(label) {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map details: ' + label });
                }
            }
        }

    };

    function fixSearchMapHeight(){
        if($("#searchMap").length > 0){
            $("#searchMap").css('height', viewport.h - $(".sbmap-search-wrapper").outerHeight() + 'px');
        }
    }

    initSearchMap();
});