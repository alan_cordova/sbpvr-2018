var initMaps;

var Webflow = Webflow || [];
Webflow.push(function() {

    //webflow interactions
    Webflow.require('ix').init([
        {"slug":"listing-img-animation","name":"Listing  img animation","value":{"style":{},"triggers":[{"type":"hover","selector":".sb2-home-listing-img","descend":true,"preserve3d":true,"stepsA":[{"transition":"transform 500ms ease 0","scaleX":1.04,"scaleY":1.04,"scaleZ":1}],"stepsB":[{"transition":"transform 500ms ease 0","scaleX":1,"scaleY":1,"scaleZ":1,"rotateX":"0deg","rotateY":"0deg","rotateZ":"0deg"}]}]}},
        {"slug":"hv5-hover-img","name":"hv5 hover img","value":{"style":{},"triggers":[{"type":"hover","selector":".hv5-exp-item-circle-img","descend":true,"preserve3d":true,"stepsA":[{"transition":"transform 500ms ease-out 0","scaleX":1.05,"scaleY":1.05,"scaleZ":1}],"stepsB":[{"transition":"transform 500ms ease-out 0","scaleX":1,"scaleY":1,"scaleZ":1}]}]}},
        {"slug":"hv5-hover-thumbnail","name":"hv5 hover thumbnail","value":{"style":{},"triggers":[{"type":"hover","selector":".hv5-thumbnail-overlay","descend":true,"stepsA":[{"opacity":1,"transition":"opacity 350ms ease 0"}],"stepsB":[{"opacity":0,"transition":"opacity 350ms ease 0"}]}]}},
        {"slug":"scroll-animation","name":"Scroll animation","value":{"style":{"opacity":0,"x":"0px","y":"60px","z":"0px"},"triggers":[{"type":"scroll","offsetBot":"10%","stepsA":[{"opacity":1,"transition":"transform 500ms ease 0, opacity 500ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"scroll-animation-2","name":"Scroll animation 2","value":{"style":{"opacity":0,"x":"0px","y":"60px","z":"0px"},"triggers":[{"type":"scroll","offsetBot":"14%","stepsA":[{"opacity":1,"transition":"transform 500ms ease 0, opacity 500ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"scroll-animation-3","name":"Scroll animation 3","value":{"style":{"opacity":0,"x":"0px","y":"60px","z":"0px"},"triggers":[{"type":"scroll","offsetBot":"20%","stepsA":[{"opacity":1,"transition":"transform 500ms ease 0, opacity 500ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"img-hover-animation","name":"Img hover animation","value":{"style":{},"triggers":[{"type":"hover","stepsA":[{"transition":"transform 500ms ease 0","scaleX":1.04,"scaleY":1.04,"scaleZ":1}],"stepsB":[{"transition":"transform 500ms ease 0","scaleX":1,"scaleY":1,"scaleZ":1}]}]}},
        /*{"slug":"map-content-hover","name":"Map content hover","value":{"style":{},"triggers":[{"type":"hover","selector":".sb2-map-popover","descend":true,"stepsA":[{"wait":"400ms","display":"block","opacity":1,"transition":"opacity 400ms ease 0"}],"stepsB":[{"wait":"400ms","display":"none","opacity":0,"transition":"opacity 400ms ease 0"}]},{"type":"hover","selector":".sb2-map-pin","descend":true,"preserve3d":true,"stepsA":[{"transition":"transform 400ms ease 0","x":"0px","y":"-8px","z":"0px"}],"stepsB":[{"transition":"transform 400ms ease 0","x":"0px","y":"0px","z":"0px"}]}]}},*/
        {"slug":"intro-on-load","name":"Intro on load","value":{"style":{"display":"none","opacity":0,"x":"0px","y":"-80px","z":"0px"},"triggers":[{"type":"load","stepsA":[{"wait":"0ms"},{"display":"block","opacity":1,"transition":"transform 1000ms ease 0, opacity 1000ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"development-logo","name":"Development logo","value":{"style":{"opacity":0,"scaleX":0.8,"scaleY":0.8,"scaleZ":1},"triggers":[{"type":"scroll","offsetBot":"20%","stepsA":[{"opacity":1,"transition":"transform 1000ms ease 0, opacity 1000ms ease 0","scaleX":1,"scaleY":1,"scaleZ":1}],"stepsB":[]}]}},
        {"slug":"bubble","name":"bubble","value":{"style":{"opacity":0,"x":"0px","y":"-20px","z":"0px"},"triggers":[{"type":"load","stepsA":[{"opacity":1,"transition":"transform 800ms ease 0, opacity 800ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"bubble-2","name":"bubble 2","value":{"style":{"opacity":0,"x":"0px","y":"-20px","z":"0px"},"triggers":[{"type":"load","stepsA":[{"wait":"300ms"},{"opacity":1,"transition":"transform 800ms ease 0, opacity 800ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"bubble-3","name":"bubble 3","value":{"style":{"opacity":0,"x":"0px","y":"-20px","z":"0px"},"triggers":[{"type":"load","stepsA":[{"wait":"600ms"},{"opacity":1,"transition":"transform 800ms ease 0, opacity 800ms ease 0","x":"0px","y":"0px","z":"0px"}],"stepsB":[]}]}},
        {"slug":"bubble-end","name":"bubble end","value":{"style":{"opacity":0,"x":"0px","y":"10px","z":"0px","scaleX":0.9,"scaleY":0.9,"scaleZ":1},"triggers":[{"type":"load","stepsA":[{"wait":"900ms"},{"opacity":1,"transition":"transform 1000ms ease 0, opacity 1000ms ease 0","x":"0px","y":"0px","z":"0px","scaleX":1,"scaleY":1,"scaleZ":1}],"stepsB":[]}]}}
    ]); 
    //end webflow interations

    // browser specific
    var viewport, viewport_p;
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    
    var isMobile = false; //initiate as false
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        isMobile = true;
    }


    /*** SMOOTH SCROLLING ***/
    var $window = $(window);
    var scrollTime = 1.2;
    var scrollDistance = 400;
    var bodyScroll = true;
    var scrollable_element = $window;

    function set_scrollable_element(elem) {
        scrollable_element = elem;
    }

    function unset_scrollable_element() {
        scrollable_element = $window;
    }

    var scrollTween;
    function event_scroll_move(elem, event){
        var delta = event.originalEvent.wheelDelta / 70 || -event.originalEvent.detail / 3;
        var scrollTop = elem.scrollTop();
        var finalScroll = scrollTop - parseInt(delta * scrollDistance);
        TweenMax.to(elem, scrollTime, {
            scrollTo: {
                y: finalScroll,
                autoKill: true
            },
            ease: Power1.easeOut,
            overwrite: 5
        });
    }

    // mouse scroll events
    var outsideSearch = true;
    $("body").on("mousewheel DOMMouseScroll", function(event) {
        //event.preventDefault();
        if (!bodyScroll) { 
            event.preventDefault();
            event_scroll_move(scrollable_element, event);
        }
        outsideSearch = true;
    });

    //properties scroll handler
    
    var scrolling_properties = false;
    var scrollSearchEnabled = false;
    var scrollingToSearch = false;
    
    /*
    $("#properties-grid").on("mousewheel DOMMouseScroll", function(event) {
        
        //detect if scroll has finished
        var elem = $(this);
        var scroll_finished = false;
        if( (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) || elem.scrollTop() == 0) {
            scroll_finished = true;
        }

        //if not finished, scroll inside container and deny body scroll
        if (!scroll_finished) {
            event.stopPropagation();
            if (!scrolling_properties) {
                scrolling_properties = true;
            }
        }


        if (scroll_finished && elem.scrollTop() > 0) {
            scrolling_properties = false;
            properties_request_next();   
        }

        if(!scroll_finished){
            event.stopPropagation();
        }


        if(!scroll_finished && outsideSearch){
            scrollingToSearch = true;
            scroll_to($(this), 500, 0, function(){
                scrollingToSearch = false;
            });
            outsideSearch = false;            
        }

        if(scrollingToSearch){
            event.preventDefault();
        }
    });
    */
    
    

    //search container scroll
    /*
    var select_options_propagated = true;
    $("#search-container-parent").on("mousewheel DOMMouseScroll", function(event) {
        if(viewport.w > 991){
            event.preventDefault();
            event_scroll_move($(this), event);

            var elem = $(this);
            var scroll_finished = false;
            if( (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) || elem.scrollTop() == 0) {
                scroll_finished = true;
            }
            if(!scroll_finished){
                event.stopPropagation();
            }
        }
    });
    */

    //search select options scroll
    /*$(".select-options-container").on("mousewheel DOMMouseScroll", function(event) {
        event.preventDefault();
        event.stopPropagation();
        event_scroll_move($(this), event);
    });*/

    /*** END SMOOTH SCROLLING ***/


    function get_scroll_top() {
        return $(window).scrollTop();
    }

    function get_viewport_h_percentage(percentage) {
        return (percentage * viewport.h) / 100;
    }

    function get_viewport_w_percentage(percentage) {
        return (percentage * viewport.w) / 100;
    }

    function get_viewport() {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        return {
            w: w,
            h: h
        };
    }

    function scroll_to_pos(top, time, extra_offset) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (top + extra_offset)
        }, time);
    }

    function scroll_to(elem, time, extra_offset, callback) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (elem.offset().top + extra_offset)
        }, time,
        function() { //callback
            if (typeof callback != "undefined") {
                callback();
            }
        });
    }

    
    viewport = get_viewport();
    Webflow.resize.on(function() {
        viewport = get_viewport();
        calculate_list_item_height();

        if(viewport.w <= 991){
            show_load_properties_button();
        }

        //resize_hero_to_screen();

        //calculate map container for google maps element
        var map_container = $("#map-container");
        if(map_container.length > 0){
            var map_height = '300px';
            if(viewport.w > 991){
                map_height = map_container.closest('.property-contact-container').outerHeight() + 'px';
                
            }
            map_container.css('height', map_height);
        }

        //calculate duration (heigh) of the properties grid
        resize_calculate_duration();


        //recalculate video action button
        resize_video_button_calculation();

        //margin top on first element after menu 
        container_menu_top_margin();

        //video height calculation
        fix_homeVideoHeight();

        //home intro events for button on responsive
        homeIntroEvents();

        //home poster video
        fixVideoPoster();


        //fix map search height
        fixSearchMapHeight();


        //calculate sizes for the Map2
        map2_handler();

    });
    Webflow.scroll.on(function(e) {
        if(get_scroll_top() > 200){
            tawkInit();
        }

        //soundVolume();
    });

    $("#properties-search-container-arrow").click(function(){
        scroll_to($("#properties-search-container"), 750, 0);
    });

    /** CONTACT MAP **/
    initMaps = function() {
        var map_elem = $("#map-container");
        if (map_elem.length < 1) {
            return 0;
        }

        var title = map_elem.data('marker-title');

        var marker_url = map_elem.data('marker');
        var latLng = {
            lat: parseFloat(map_elem.data('lat')),
            lng: parseFloat(map_elem.data('lng'))
        };
        var pos = new google.maps.LatLng(latLng.lat, latLng.lng);
        var icon = {
            url: marker_url, // url
            scaledSize: new google.maps.Size(100, 100), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(50, 100) // anchor
        };
        map = new google.maps.Map(document.getElementById('map-container'), {
            center: pos,
            position: pos,
            zoom: 15,
            styles: map_styles,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: pos,
            title: title,
            icon: icon
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    };
    /** END CONTACT MAP **/

    /** MENU INTERACTIONS **/
    //dropdown menu interactions 
    var dropdown_timeouts = [];
    $(".nav-dropdown").mouseenter(function(e) {
        if(typeof dropdown_timeouts[$(this).index()] != "undefined"){
            clearTimeout(dropdown_timeouts[$(this).index()]);
        }

        $(this).find('.dropdown-toggle').addClass('w--open');
        $(this).find('.dropdown-list').addClass('w--open');
        dropdown_list_position_handler();
    });
    $(".nav-dropdown").mouseleave(function(e){
        var _this = $(this);
        dropdown_timeouts[$(this).index()] = setTimeout(function(){
            _this.find('.dropdown-toggle').removeClass('w--open');
            _this.find('.dropdown-list').removeClass('w--open');
        }, 150);
    });

    var lastTapIndex = -1;
    $(".nav-dropdown").on('click', function(e){
        var dropdown =  $(this);
        if(viewport.w <= 991){
            currentTapIndex = $(this).index();
            if(lastTapIndex != currentTapIndex && dropdown.find('.w-dropdown-list').length > 0){
                e.preventDefault();
                dropdown.closest('.nav-menu').find('.w--open').removeClass('w--open');
                dropdown.find('.w-dropdown-toggle').addClass('w--open');
                dropdown.find('.w-dropdown-list').addClass('w--open');
            }
            lastTapIndex = currentTapIndex;
        }
    });


    //submenu space on small windows
    function dropdown_list_position_handler() {
        $(".dropdown-list").each(function() {
            var width = calculate_dropdown_list_width($(this));
            var dropdown_link_width = $(this).closest('.nav-dropdown').outerWidth();
            var dropdown_link_offset = $(this).closest('.nav-dropdown').offset();
            var repositioned = false;
            if (viewport.w > 991) {
                //sets dropdown width
                $(this).css('width', width + 'px');
                //sets new offset according to dropdown position
                var new_offset = (width / 2) - (dropdown_link_width / 2);
                $(this).css('left', '-' + new_offset + 'px');
                repositioned = true;
                var viewport_with_dropdown = dropdown_link_offset.left + (width / 2) + 1 + (dropdown_link_width / 2);
                if (viewport.w < viewport_with_dropdown) {
                    var remaining = viewport_with_dropdown - viewport.w;
                    $(this).css('left', '-' + (new_offset + remaining + 15) + 'px');
                }
            }
            if (!repositioned) {
                $(this).css('left', '');
                $(this).css('width', '');
            }
        });
    }

    function calculate_dropdown_list_width(dropdown) {
        var width = 5;
        dropdown.find('.dropdown-link').each(function() {
            width += $(this).outerWidth();
        });
        return Math.ceil(width) + 1;
    }
    /** END MENU INTERACTIONS **/

    function calculate_list_item_height() {
        if ($(".list-flex").length > 0) {
            $(".list-flex").each(function() {
                var original_height = $(this).find(".list-item-content").outerHeight();
                var new_height = original_height;
                //calculate flex height for 992px or greater    
                if (viewport.w <= 991) {
                    new_height = '';
                }
                $(this).find(".list-item-title-container").css('height', new_height);
                //calculate margin bottom for text container for 991px and lower
                if (viewport.w <= 991) {
                    var flex_height = $(this).closest('.list-flex').outerHeight();
                    var container_height = $(this).closest('.animated-list').outerHeight();
                    var margin = flex_height - container_height + 20;
                    $(this).closest('.animated-list').css('margin-bottom', margin + 'px');
                } else {
                    $(this).closest('.animated-list').css('margin-bottom', '');
                }
            });
        }
    }

    /** POPUP **/
    function enable_form_popup() {
        var popup = $(".popup");
        popup.addClass('active');
        set_scrollable_element(popup);
        $("body").addClass("no-scroll");
        if (iOS) {
            popup.data('last-position', get_scroll_top());
            $("body").css('position', 'fixed');
        }
    }

    function disable_form_popup() {
        var popup = $(".popup");
        popup.removeClass('active');
        unset_scrollable_element();
        $("body").removeClass("no-scroll");
        if (iOS) {
            $("body").css('position', '');
            scroll_to_pos(popup.data('last-position'), 0);
        }

        if($("#frame-video-container").length > 0){
            $("#frame-video-container").html('');
            $("#virtual-tour-video-container").html('');
        }
    }

    function show_form_popup_loader(){
        var loading_img_url = _THEME_PATH + '/images/loading.gif';
        var loading_html = '<img src="'+loading_img_url+'" class="popup-loader">';
        $(".popup .properties-popup").html(loading_html);
    }

    //form popup
    if ($("#popup-form").length > 0) {
        $("#open-popup-form, #open-popup-form-2, #open-popup-features").click(enable_form_popup);
        $("#popup-close").click(disable_form_popup);
        $("#popup-form").click(function(e) {
            if (e.target.className == "popup-container" || e.target.className == "popup-form-flex" || e.target.id == "popup-form") {
                disable_form_popup();
            }
        });
    }

    //load property popup on click
    /*
    if ($("#properties-renderer .property-grid-item").length > 0) {
        $(document).on('click', "#properties-renderer .property-grid-item", function(e) {
            e.preventDefault();
            if (e.target.className == "popup-container" || e.target.className == "popup-form-flex" || e.target.id == "popup-form") {
                disable_form_popup();
            }else{
                var item = $(this);
                enable_form_popup();
                show_form_popup_loader();
                request_property(){
                    var property_url = item.find('.property-grid-item').attr('url');
                }
            }
            return false;
        });
    }
    */

    /** END POPUP **/


    /** SEARCH INTERACTIONS **/
    function close_all_select_buttons() {
        $('.property-select-container').each(function() {
            close_select_button($(this));
        });
    }

    function open_select_button(select_container) {
        var button = select_container.find('.property-select-button');
        if (button.data('open') && button.data('open') == 1) {
            close_select_button(select_container);
        } else {
            close_all_select_buttons();
            button.data('open', 1);
            button.find('.select-arrow').addClass('open');
            button.closest('div').find('.select-options-container').fadeIn('fast');
        }
    }

    function close_select_button(select_container) {
        var button = select_container.find('.property-select-button');
        button.data('open', '');
        button.find('.select-arrow').removeClass('open');
        button.closest('div').find('.select-options-container').fadeOut('fast');
    }

    $(".property-select-button").click(function() {
        var select_container = $(this).closest('.property-select-container');
        open_select_button(select_container);
    });

    $(".property-select-option").click(function() {
        var term_text = $(this).text();
        var taxonomy = $(this).data('term-taxonomy');
        var taxonomy_id = $(this).data('term-id');

        var select_container = $(this).closest('.property-select-container');
        select_container.find('.property-default-option').css('visibility', 'hidden');
        select_container.find('.property-selected-option').text(term_text);
        select_container.find('.property-selected-option').attr('data-term-taxonomy', taxonomy)
        select_container.find('.property-selected-option').attr('data-term-id', taxonomy_id);
        select_container.find('.property-selected-option').show();
        close_select_button(select_container);
    });

    if($("#f_development").length > 0){
        //if devID is sent select option for development choose
        var f_dev_term_id = $("#f_development").data('term-id');
        if(f_dev_term_id != ""){
            $("#f_development").closest('.property-select-container').find('.property-select-option').each(function(){
                if(f_dev_term_id == $(this).data('term-id')){
                    $(this).trigger('click');
                }
            });
        }
    }

    function search_prepare_parameters(form){
        var req = {
            f_search: $("#f_search").val(),
            f_property_type_tax_id: form.find("#f_property_type").attr('data-term-id'),
            f_development_tax_id: form.find("#f_development").attr('data-term-id'),
            f_location_tax_id: form.find("#f_location").attr('data-term-id'),
            f_price_range: form.find('#f_price_range').val()
        }
        return req;
    }

    function search_reset_pagination(){
        $(".properties-search-form").data('current-page', 0);
    }

    function search_increase_pagination(){
        var next = parseInt($(".properties-search-form").data('current-page')) + 1;
        $(".properties-search-form").data('current-page', next);
    }

    function search_render(res){
        if(parseInt($(".properties-search-form").data('current-page')) == 1){
            $("#properties-renderer").html(res);
        }else{
            $("#properties-renderer").append(res);
        }
    }

    $(".properties-search-form").on('submit', function(e){
        e.preventDefault();
        search_reset_pagination();
        var form = $(this);
        search_not_found = false;
        $("#properties-renderer").html('');
        $('#properties-renderer').stop(true, true).animate({
            scrollTop: 0
        }, 0);
        show_search_loader();
        show_load_properties_button();
        properties_form_request(form);
        if(viewport.w <= 991){
            $("#mobile-properties-close-trigger").trigger('click');
        }
    });

    if($(".prop-order-filter").length > 0){
        $(".prop-order-filter").change(function(){
            var val = $(this).val();
            $("#properties-renderer").html('');
            $(".properties-search-form").trigger('submit');
            search_increase_pagination();
        });
    }

    var search_not_found = false;
    var requesting_form_properties = false;
    function properties_form_request(form){
        if(!requesting_form_properties && !search_not_found){
            show_search_loader();
            requesting_form_properties = true;
            search_increase_pagination();
            var req = search_prepare_parameters(form);

            //if has ordering filter
            if($(".prop-order-filter").length > 0){
                var order_filter = $(".prop-order-filter").val();
                req.f_order_filter = order_filter;
            }

            req.ajax_search = true;

            var url = form.prop('action');
            var page = form.data('current-page');
            var request_url = url + '/page/' + page;

            //bodyScroll = false;

            $.get(request_url, req, function(res){
                search_render(res);
                requesting_form_properties = false;
                hide_search_loader();
                //setTimeout(function(){ bodyScroll = true; }, 650);

                if(res.trim() == ''){
                    properties_not_found_msg(page);
                    hide_load_properties_button();
                }

                resize_calculate_duration();
            }).fail(function(){
                requesting_form_properties = false;
                //setTimeout(function(){ bodyScroll = true; }, 650);
                search_not_found = true;
                hide_search_loader();
                properties_not_found_msg(page);
                hide_load_properties_button();

                resize_calculate_duration();
            });
        }
    }

    function properties_not_found_msg(page){
        //shows the not found text
        not_found_text = '';
        if(page == 1){
            not_found_text = $("#properties-not-found div").data('not-found-start');
        }else{
            not_found_text = $("#properties-not-found div").data('not-found-final');
        }
        setTimeout(function(){
            $("#properties-not-found div").text(not_found_text);
            $("#properties-not-found").show(function(){
                resize_calculate_duration();
                var elem = $(this);
                setTimeout(function(){
                    elem.fadeOut('slow', function(){
                        resize_calculate_duration();
                    });
                }, 3000);
            });
        }, 1)
    }

    function set_search_loader_opacity(opacity){
        TweenMax.to("#properties-loader", 1, {css: {opacity: opacity}});
    }
    function show_search_loader(){
        set_search_loader_opacity(1);
    }
    function hide_search_loader(){
        set_search_loader_opacity(0);
    }
    function hide_load_properties_button(){
        $("#load-properties-mobile").hide();
    }
    function show_load_properties_button(){
        if(viewport.w <= 991){
            $("#load-properties-mobile").css('display','block');
        }
    }

    function properties_request_next(){
        var form = $(".properties-search-form");
        properties_form_request(form);
    }
    $("#load-properties-mobile").click(function(){
        properties_request_next();
    });
    /** END SEARCH INTERACTIONS **/


    /*****************************/
    /** Scroll Magic Animations **/

    var magicController = new ScrollMagic.Controller();

    /*****************************/
    /** HOME ANIMATIONS **/
    var heroParallaxScene;
    var pinHeroScene;
    pinHeroScene = new ScrollMagic.Scene({
        triggerElement: '#hero-container',
        triggerHook: 0,
        offset: get_viewport_h_percentage(25),
        duration: (get_viewport_h_percentage(30) - 35) + 'px'
    })
    .setPin('#hero-container')
    .on('update', function(event) {
        if (viewport.w <= 991) {
            event.target.removePin(true);
        } else {
            event.target.setPin('#hero-container');
        }
    })
    .addTo(magicController);

    /** PIN VIDEO FROM HOME **/

        var pin3dVideoScene;
        pin3dVideoScene = new ScrollMagic.Scene({
            triggerElement: '#sb-info',
            triggerHook: 0,
            offset: get_viewport_h_percentage(35),
            duration: ($(".sb-info-container .container").outerHeight() - $("._3d-home-trigger").outerHeight() ) + 'px'
        })
        .setPin('._3d-home-trigger')
        .on('update', function(event) {
            if (viewport.w <= 860) {
                event.target.removePin(true);
                event.target.duration('1px');
            } else {
                var duration = ($(".sb-info-container .container").outerHeight() - $("._3d-home-trigger").outerHeight() ) + 'px';
                event.target.setPin('._3d-home-trigger');
                event.target.duration(duration);
            }
        })
        .addTo(magicController);

        var pin3dVideoScene2;
        pin3dVideoScene2 = new ScrollMagic.Scene({
            triggerElement: '#sb-info',
            triggerHook: 0,
            offset: '-150px'
        })
        .setTween(TweenMax.to("._3d-home-trigger", 0.4, {
            css: { opacity: 1 },
            ease: Power0.easeNone
        }))
        .addTo(magicController);

        if($("._3d-home-trigger").length > 0){
            function analytics_360_button() {
                ga('send', 'event', {
                    eventCategory: 'Buttons',
                    eventAction: 'click',
                    eventLabel: '360 tour button '
                });
            }
            $("._3d-home-trigger").click(function(e){
                e.preventDefault();
                var href= $(this).attr('href');
                setTimeout(function(){
                    analytics_360_button();
                    document.location = href;
                },250);
                return false;
            });
        }

    /** END PIN VIDEO FROM HOME **/

    if($("#tres-mares-video-action").length > 0){

        //video pin position
        var tresMaresPlay = new ScrollMagic.Scene({
            triggerElement: '.developments-introduction',
            triggerHook: 0.3,
            duration: ($(".developments-introduction").outerHeight()-30) + 'px'
        })
        .setPin("#tres-mares-video-action")
        .addTo(magicController);

        //video animation
        var tresMaresPlay2 = new ScrollMagic.Scene({
            triggerElement: '.developments-introduction',
            triggerHook: 0.3,
        })
        .setTween(TweenMax.to("#tres-mares-video-action", 0.4, {
            css: { opacity: 1 },
            ease: Power0.easeNone
        }))
        .addTo(magicController);

        resize_video_button_calculation();

        $(".tres-mares-video-trigger").click(function(){
            enable_form_popup();
            var iframeTpl = $('<iframe src="" width="100%" height="550" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            iframeTpl.attr('src', $("#frame-video-container").data('vimeo'));
            $("#frame-video-container").html('').append(iframeTpl);
        });
    }
    function resize_video_button_calculation(){
        if(typeof tresMaresPlay != "undefined"){
            if(viewport.w > 1330){
                tresMaresPlay.duration(($(".developments-introduction").outerHeight()-30) + 'px');
                tresMaresPlay.triggerHook(0.3);
                //tresMaresPlay2.triggerHook(0.3);
            }else if(viewport.w > 767){
                tresMaresPlay.duration(($(".developments-introduction").outerHeight()-100) + 'px');
                tresMaresPlay.triggerHook(0.5);
                //tresMaresPlay2.triggerHook(0.6);
            }
        }
    }


    /*****************************/
    /** DEVELOPMENTS ANIMATIONS **/
    var listPins = [];
    var listLogoTriggers = [];
    var listLogoTriggersLeave = [];
    var aListPins = [
        'development-marina-tower',
        'development-shangrila',
        'development-tres-mares',
        'development-azulejos',
        'development-base'
    ];
    for (var i = 0; i < aListPins.length; i++) {
        var list_id = '#' + aListPins[i];
        //developments pins
        listPins[i] = new ScrollMagic.Scene({
            triggerElement: list_id,
            triggerHook: 0,
            duration: '30%'
        })
        .setPin(list_id)
        .on('update', function(event) {
            if (viewport.w <= 767) {
                event.target.duration(1);
            } else {
                event.target.duration('30%');
            }
        })
        .addTo(magicController);

        //developments popup animations
        listLogoTriggers[i] = new ScrollMagic.Scene({
            triggerElement: list_id,
            triggerHook: 0,
            offset: -100
        })
        .setTween(
            TweenMax.fromTo(list_id + ' .d-animation-container-blue', 0.3, {
                css: {
                    opacity: 0,
                    scale: 1.05
                },
                ease: Power0.easeNone
            }, {
                css: {
                    opacity: 1,
                    scale: 1
                },
                ease: Power0.easeNone
            })
        )
        .on('update', function(event) {
            if (viewport.w <= 767) {
                event.target.offset(-300);
            } else {
                event.target.offset(-100);
            }
        })
        .addTo(magicController);
    }


    /*****************************/
    /** SINGLE DEVELOPMENT ANIMATIONS **/
    var subtitleTL = new TimelineMax();
    subtitleTL.fromTo("#subtitle-animation", 1, {
        css: {
            opacity: 0,
            color: '#0c1218'
        }
    }, {
        css: {
            opacity: 1
        }
    }, 0.3)
    .fromTo("#subtitle-animation", 0.8, {
        css: {
            color: '#0c1218'
        }
    }, {
        css: {
            color: '#fff'
        }
    }, '-=0.3');

    var developmentContentScene = new ScrollMagic.Scene({
        triggerElement: "#development-detail-content",
        triggerHook: 0.5
    })
    .on('update', function(event) {
        if (viewport.w <= 767) {
            event.target.triggerHook(1);
        } else {
            event.target.triggerHook(0.5);
        }
    })
    .setTween(TweenMax.fromTo("#development-detail-content", 0.8, {
        css: {
            opacity: 0,
            top: 50
        },
        ease: Back.easeOut.config(1)
    }, {
        css: {
            opacity: 1,
            top: 0
        }
    }))
    .addTo(magicController);

    var developmentActionsScene = new ScrollMagic.Scene({
        triggerElement: "#dev-call-to-action",
        triggerHook: 0.65
    })
    .setTween(TweenMax.fromTo("#dev-action-buttons-inner", 0.7, {
        css: {
            opacity: 0,
            top: 15
        }
    }, {
        css: {
            opacity: 1,
            top: 0
        }
    }))
    .addTo(magicController);


    /*******************************/
    /** LIST STYLE 1 - ANIMATIONS **/
    var animatedList = [];
    if ($(".animated-list").length > 0) {
        var animatedCount = 0;
        $(".animated-list").each(function() {
            var elem = $(this);
            animatedList[animatedCount] = {};
            animatedList[animatedCount].elem = elem;
            animatedList[animatedCount].triggerElem = '#' + $(this).attr('id');
            animatedList[animatedCount].targetElem = '#' + $(this).find('.list-item-content').attr('id');
            animatedList[animatedCount].mgScene = new ScrollMagic.Scene({
                    triggerElement: animatedList[animatedCount].triggerElem,
                    triggerHook: 0.5
                })
                .setTween(TweenMax.fromTo(animatedList[animatedCount].targetElem, 0.7, {
                    css: {
                        opacity: 0,
                        top: 15
                    }
                }, {
                    css: {
                        opacity: 1,
                        top: 0
                    }
                }))
                .addTo(magicController);
            animatedCount++;
        });
    }


    /*****************************/
    /** SEARCH PROPERTIES PAGE **/
    var scrollingToSearch = false;
    if ($("#properties-search-container").length > 0) {
        var searchBoxScene = new ScrollMagic.Scene({
            triggerElement: "#properties-grid",
            triggerHook: 0,
            duration: ($("#properties-grid").outerHeight() - viewport.h) + 'px'
        })
        .setPin('#search-container-parent', {pushFollowers: true})
        .on('leave', function(event){
            if(event.scrollDirection == "FORWARD"){
                properties_request_next();
            }
        })
        .on('update', function(event){
            ;  
        })
        //.addIndicators()
        .addTo(magicController);
            
        var responsivePosition = (viewport.w <= 991);
        var responsiveRedirecting = false;
        if(responsivePosition){
            responsiveSearchInterval = setInterval(function(){
                if(viewport.w > 991){
                    if(!responsiveRedirecting){
                        document.location = document.location;
                    }
                    responsiveRedirecting = true;
                }
            }, 1000);   
            
        }
    }

    function resize_calculate_duration(){
        if(typeof searchBoxScene != "undefined"){
            searchBoxScene.duration(0);
            var duration = $("#properties-grid").outerHeight() - viewport.h;
            searchBoxScene.duration(duration);
        }
    }

    if ($("#mobile-properties-search-trigger").length > 0) {
        $("#mobile-properties-search-trigger").click(function() {
            var searchContainerParent = $("#search-container-parent-responsive");
            TweenMax.to(searchContainerParent, 0.85, {
                css: {
                    left: '0',
                    opacity: 1
                },
                ease: Circ.easeOut
            });
        });
    }

    if ($("#mobile-properties-close-trigger").length > 0) {
        $(document).on('click', "#mobile-properties-close-trigger", function() {
            var searchContainerParent = $("#search-container-parent-responsive");
            TweenMax.to(searchContainerParent, 0.85, {
                css: {
                    left: '-100%',
                    opacity: 0.8
                },
                ease: Circ.easeOut
            });
        });
    }

    //property grid mouse enter
    $(document).on('mouseenter', ".property-grid-item", function() {
        var grid_hover = $(this).find('.property-grid-content-hover');
        TweenMax.to(grid_hover, 0.4, {
            css: {
                scale: 1,
                opacity: 1
            },
            ease: Circ.easeOut
        });
        if(viewport.w > 991){
            var grid_title = $(this).find('.property-grid-content-title');
            var grid_height = grid_title.outerHeight() + 10;
            TweenMax.to(grid_title, 0.4, {
                css: {
                    opacity: 0
                },
                ease: Circ.easeOut
            });
        }
    });

    //property grid mouse leave
    $(document).on('mouseleave', ".property-grid-item", function() {
        var grid_hover = $(this).find('.property-grid-content-hover');
        TweenMax.to(grid_hover, 0.4, {
            css: {
                scale: 0.8,
                opacity: 0
            },
            ease: Circ.easeOut
        });
        var grid_title = $(this).find('.property-grid-content-title');
        TweenMax.to(grid_title, 0.4, {
            css: {
                opacity: 1
            },
            ease: Circ.easeOut
        });
    });


    /*****************************/
    /** LAYOUT ANIMATIONS **/
    var newsletterFadeScene;
    newsletterFadeScene = new ScrollMagic.Scene({
        triggerElement: '#newsletter',
        triggerHook: 1,
        offset: 150
    })
    .setTween(
        TweenMax.fromTo('#newsletter-container', 0.5, {
            autoAlpha: 0,
            y: 40
        }, {
            autoAlpha: 1,
            ease: Power0.easeNone,
            y: 0
        })
    )
    .addTo(magicController);


    /** END Scroll Magic Animations **/
    /*********************************/
    $(".property-search-hidden-input").each(function() {
        var lower_price = $(this).data('lower-price');
        var higher_price = $(this).data('higher-price');
        var scale = [];
        scale.push(lower_price);
        var range = higher_price - lower_price;
        var jumps = Math.round(parseInt(range / 5));
        for(var i=1; i < 5; i++){
            scale.push(lower_price + (jumps*i));
        }
        scale.push(higher_price);

        $(this).css('display', 'block');
        $(this).attr('type', 'hidden');
        $(this).jRange({
            from: lower_price,
            to: higher_price,
            step: 1,
            scale: scale,
            format: '%s',
            width: '100%',
            showLabels: true,
            isRange: true
        });

        var prices_interval = setInterval(function(){
            if($(".slider-container .scale > span").length > 0){
                clearInterval(prices_interval);
                $(".slider-container .scale > span").each(function(){
                    var text = (parseInt($(this).find('ins').text())/1000)
                        .toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace('.0','');
                    $(this).find('ins').text(text + 'k');
                });
            }
        }, 250);
    });


    // contact form
    var sending_contact_form = false;
    $("#contact_form").submit(function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.cmd').val('7764');
        if(!sending_contact_form){
            sending_contact_form = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-wrapper').find(".w-form-done, .w-form-fail").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                }else{
                    form.closest('.form-wrapper').find('.w-form-fail').fadeIn();
                }
                sending_contact_form = false;
                form.trigger("reset");
            },'json');
        }
    });

    //constant contact suscribe newsletter
    var suscribing = false;
    $("#newsletter-form").submit(function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.cmd').val('9871');
        if(!suscribing){
            suscribing = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();
            $.post(action, req, function(data){
                if(data.success){
                    form.fadeOut(function(){
                        $(".newsletter-success").fadeIn();
                    });
                    suscribing = false;
                    form.trigger("reset");
                }
            },'json');
        }
    });

    // contact form
    var sending_form_service = false;
    $("#form_service").submit(function(e){
        e.preventDefault();
        var form = $(this);
        form.find('.cmd').val('3341');
        if(!sending_form_service){
            sending_form_service = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-wrapper').find(".w-form-done, .w-form-fail").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                }else{
                    form.closest('.form-wrapper').find('.w-form-fail').fadeIn();
                }
                sending_form_service = false;
                form.trigger("reset");
            },'json');
        }
    });

    // contact from property

    function contactProperty(form_id){
        var mandarina = $(form_id).find(".mandarina_property").val();
        var sending_property_contact = false;
        setTimeout(function(){
            $(form_id).find(".cmd").val('33551');
        }, 3000);
        $(form_id).submit(function(e){
            e.preventDefault();
            var form = $(this);
            if (mandarina) {
                if(!sending_property_contact){
                    if ($(".f_accept").is(':checked')) {
                        sending_property_contact = true;
                        var action = $(this).attr('action');
                        var req = $(this).serializeArray();

                        form.closest('.form-wrapper').find(".w-form-done, .w-form-fail").hide();
                        $.post(action, req, function(data){
                            if(data.success){
                                form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                            }else{
                                form.closest('.form-wrapper').find('.w-form-fail').fadeIn();
                            }
                            sending_property_contact = false;
                            form.trigger("reset");
                        },'json');
                    }else{
                        $('.w-form-done').fadeOut();
                        $('.w-form-fail').fadeIn();
                        setTimeout(function() {
                            $('.w-form-fail').fadeOut();
                        }, 3000 );
                        return false;
                    }
                }
            }else{
                if(!sending_property_contact){
                    sending_property_contact = true;
                    var action = $(this).attr('action');
                    var req = $(this).serializeArray();

                    form.closest('.form-wrapper').find(".w-form-done, .w-form-fail").hide();
                    $.post(action, req, function(data){
                        if(data.success){
                            form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                        }else{
                            form.closest('.form-wrapper').find('.w-form-fail').fadeIn();
                        }
                        sending_property_contact = false;
                        form.trigger("reset");
                    },'json');
                }else{
                    $('.w-form-done').fadeOut();
                    $('.w-form-fail').fadeIn();
                    setTimeout(function() {
                        $('.w-form-fail').fadeOut();
                    }, 3000 );
                    return false;
                }
            }
        });
    }

    contactProperty("#contact_from_property");

    //facebook share
    $(document).on('click', ".facebook-share", function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        FB.ui({
            method: 'share',
            display: 'popup',
            href: url,
        }, function(response){});
        return false;
    });

    if($("#favorite-post-id").length > 0 || $(".fav-remove-list").length > 0){
        function load_favorites(){
            var favs = Cookies.get('favsArr');
            if(!favs){
                favs = [];
            }else{
                favs = JSON.parse(favs);
            }  
            return favs;
        }
        function save_favorites(favs){
            Cookies.set('favsArr', JSON.stringify(favs));
        }
        function test_favorite(favs){
            var favs = load_favorites();
            var currentProp = parseInt($("#favorite-post-id").data('post-id'));
            var pass = false;
            for(var i=0; i < favs.length; i++){
                if(favs[i] == currentProp){
                    pass = true;
                }
            }
            
            if(pass){
                $(".favorite-controller.add").css('display','none');
                $(".favorite-controller.remove").css('display','inline-block');
            }else{
                $(".favorite-controller.add").css('display','inline-block');
                $(".favorite-controller.remove").css('display','none');
            }
        }

        function add_favorite(post_id){
            var favs = load_favorites();
            favs.push(post_id);
            save_favorites(favs);
        }
        function remove_favorite(post_id){
            var favs = load_favorites();
            var newFavs = [];
            for(var i=0; i < favs.length; i++){
                if(favs[i] != post_id){
                    newFavs.push(favs[i]);
                }
            }
            save_favorites(newFavs);
        }

        $(".favorite-controller.add").click(function(){
            var id = parseInt($("#favorite-post-id").data('post-id'));
            add_favorite(id);
            test_favorite();
        });
        $(".favorite-controller.remove").click(function(){
            var id = parseInt($("#favorite-post-id").data('post-id'));
            remove_favorite(id);
            test_favorite();
        });

        if($("#favorite-post-id").length > 0){
            test_favorite();
        }
        

        $(".fav-remove-list").click(function(){
            var post_id = parseInt($(this).data('post-id'));
            remove_favorite(post_id);
            $(this).closest(".favorites-grid").remove();
        });

        /*var favId = $("#favorite-post-id").val();
        var favIdString = 'property_' + favId;
        if($.cookie)*/
    }
    
    
    //ocupancy calendar from properties for rent
    var oCalendar = $("#ocupancy_calendar");
    if(oCalendar.length > 0){

        $("#ocupancy_calendar_container h3").click(function(){
            oCalendar.toggle();
        });

        var url = document.location.href;
        var q = { ocupancy: 1 };
        $.get(url, q, function(unavailable){
            var disabled = [];
            for(var i = 0; i < unavailable.length; i++){
                disabled.push(new Date(unavailable[i].Y, unavailable[i].m-1, unavailable[i].d));
            }
            oCalendar.calendar({
                disabledDays: disabled
            });
        },'json');

        
    }

    /*
    function resize_hero_to_screen(){
        var hero_properties = $(".page-heading-container.properties");
        var hero_developments = $(".developments");
        var hero_pm = $(".page-heading-container.property-management");
        var hero_concierge = $(".page-heading-container.sb-concierge");
        var hero = $(".hero");

        if(hero_properties.length > 0 || hero_developments.length > 0 || hero_pm.length > 0 || hero_concierge.length > 0 || hero.length > 0){ 
            var adminbar = ($("#wpadminbar").length > 0) ? $("#wpadminbar").outerHeight() : 0;
            var menu_h = $(".menu-container").outerHeight();
            var height = 0;

            if(viewport.w > 991){
                if(hero_properties.length > 0){
                    height = viewport.h - menu_h - adminbar;
                    hero_properties.outerHeight(height);
                }

                if(hero_developments.length > 0){
                    height = viewport.h - menu_h - adminbar;
                    hero_developments.outerHeight(height);
                }

                if(hero_pm.length > 0){
                    height = viewport.h - menu_h - adminbar;
                    hero_pm.outerHeight(height);
                }

                if(hero_concierge.length > 0){
                    height = viewport.h - menu_h - adminbar;
                    hero_concierge.outerHeight(height);
                }

                if(hero.length > 0){
                    height = viewport.h - menu_h - adminbar;
                    hero.outerHeight(height);
                }
            }
        }
    }
    resize_hero_to_screen();
    */
    

    //masonry
    /*$('#properties-renderer').masonry({
      // options
      itemSelector: '.properties-grid-item-container',
      columnWidth: 200
    });*/

    function container_menu_top_margin(){
        if(viewport.w > 991){
            var menu_height = $(".menu-container").outerHeight();
            $(".container-menu-top-margin").css('margin-top', menu_height + 'px');
        }else{
            $(".container-menu-top-margin").css('margin-top', '');
        }
    }

    /********************/
    /****** SB 2.0 ******/

    if($(".map-slider-map")){
        var lastTapMapIndex = -1;
        $(".map-icon-trigger").click(function(e){
            currentTapMapIndex = $(this).index();

            if(isMobile && lastTapMapIndex != currentTapMapIndex){
                e.preventDefault();
                lastTapMapIndex = currentTapMapIndex;
            }

            var index = $(this).data('item-id');
            $(".sb2-home-slide").eq(index).find('a').trigger('click');
            mapChangeSlide(index);
            mapTimer = 1;
        });

        /*
    
        var lastTapIndex = -1;
        $(".nav-dropdown").on('click', function(e){
            var dropdown =  $(this);
            if(viewport.w <= 991){
                currentTapIndex = $(this).index();
                if(lastTapIndex != currentTapIndex){
                    e.preventDefault();
                    dropdown.closest('.nav-menu').find('.w--open').removeClass('w--open');
                    dropdown.find('.w-dropdown-toggle').addClass('w--open');
                    dropdown.find('.w-dropdown-list').addClass('w--open');
                }
                lastTapIndex = currentTapIndex;
            }
        });

        */

        var iconHover = false;
        $(".map-icon-trigger").mouseenter(function(){
            iconHover = true;
            var _this = $(this);
            setTimeout(function(){
                if(iconHover){
                    var index = _this.data('item-id');
                    mapChangeSlide(index);
                    mapTimer = 1;
                    mapActive = false;
                }
            }, 250);
        });
        $(".map-icon-trigger").mouseleave(function(){
            iconHover = false;
            mapActive = true;
        });
        $(".sb2-home-slider").mouseenter(function(){
            mapActive = false;
        });
        $(".sb2-home-slider").mouseleave(function(){
            mapActive = true;
        });

        function mapChangeSlide(index){
            $(".map-slider-nav").children().eq(index).trigger('tap');
            mapIndex = index;
        }
        //map slider custom autoplay
        var mapTimer = 1;
        var mapIndex = 0;
        var mapTotal = $(".sb2-home-slide").length;
        var mapActive = true;

        setInterval(function(){
            if(mapActive && (!isMobile && viewport.w > 991)){
                if(mapTimer++ >= 5){
                    mapIndex++;
                    if(mapIndex >= mapTotal){
                        mapIndex = 0;
                    }
                    mapTimer = 1;
                    mapChangeSlide(mapIndex);
                }
            }
        }, 1000);

        $("#virtual-tour-trigger").click(function(){
            enable_form_popup();
            var iframeTpl = $('<iframe scrolling="auto" allowtransparency="true" width="100%" height="550" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            iframeTpl.attr('src', 'https://my.matterport.com/show/?m=P7GotoZaz5z');
            $("#virtual-tour-video-container").html('').append(iframeTpl);
        });
    }


    // Intro video - animations
    var sbHomeIntroElem = $(".sb2-home-main-content");
    function fix_homeVideoHeight(){
        var videoWrapper = $('.sb2-video-wrapper');
        if(videoWrapper.length == 0) return 0;

        if(viewport.w <= 991){
            var menuHeight = $(".menu-container").outerHeight();
            videoWrapper.css('height', '400px');
        }else{
            videoWrapper.css('height', '');
        }
    }
    
    function homeIntroEvents(){
        if(sbHomeIntroElem.length == 0)  return 0;

        /*if(viewport.w <= 991){
            sbHomeIntroElem.css('opacity', '1');
        }*/
    }

    if(sbHomeIntroElem.length > 0){
        //var introMoveStop;
        setTimeout(function(){
            /*
            sbHomeIntroElem.mousemove(function( e ) {
                if(viewport.w > 991){
                    sbHomeIntroElem.css('opacity', '1');
                    clearTimeout(introMoveStop);
                    introMoveStop = setTimeout(function(){
                        sbHomeIntroElem.css('opacity', 0.4);
                    }, 3500);
                }
            });

            sbHomeIntroElem.mouseleave(function(e){
                if(viewport.w <= 991){
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                }
            });
            */

            $(".chat.widget").fadeIn();
        }, 7000);

    }
    
    var tawkStarted = false;
    function tawkInit(){
        if(!tawkStarted && !_LIVE_SITE){
            tawkStarted = true;
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/' + _tawkKey + '/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        }
    }
    setTimeout(tawkInit, _tawkTime);


    //mp3 audio
    /*if(_is_front_page){
        var mp3 = _path + '/videos/datGroove.mp3';
        var sbSound = new Howl({
            src: [mp3],
            loop: true,
            volume: 0.3
        });
        sbSound.play();
    }
    function soundVolume(){
        if(typeof sbSound != "undefined" && sbSound){
            var newVolume = ((100 - ((get_scroll_top() * 100) / viewport.h)) / 2) * .01;
            newVolume = (newVolume > 0.2) ? 0.2 : newVolume;
            console.log(newVolume);
            sbSound.volume(newVolume);
        }
    }*/

    function fixVideoPoster(){
        var poster = $(".w-background-video-poster");
        var videoContainer = $(".sb2-video.w-background-video");
        var posterImg = (isMobile) ? videoContainer.data('poster-url-mobile') : videoContainer.data('poster-url');
        poster.css('background-image', 'url(' + posterImg + ')');
    }


    /*****************************/
    /*** SEARCH PROPERTIES MAP ***/
    /*****************************/

    var initSearchMap = function(){
        var mapStarted = false;
        var searchMap;
        var searchMapElem = $("#searchMap");
        var searchMapWrapper = $("#searchMapWrapper");
        var closeTrigger = $(".searchMapClose");
        var openTrigger = $(".searchMapOpen");
        var inputFilterSale = $("#mapSearchSale");
        var inputFilterRent = $("#mapSearchRent");
        var inputFilterDev = $("#mapSearchDev");
        var mapItems = null;
        var requestingData = false;
        
        var mapSearchFilter;
        var bounded = false;
        var infoWindow;

        if(searchMapElem.length > 0){

            getMapData();

            openTrigger.click(showMap);
            closeTrigger.click(hideMap);

            /*$(document).on('click', '.mapInnerImgWrapper a, .mapInnerTitle', function(){
                analyticsMapPropertyDetails(mapItems[$(this).data('index')].title);
            });*/

            $(document).on('click', '#mapSearchSale, #mapSearchRent, #mapSearchDev', function(){
                if($(this).hasClass('disabled')){
                    $(this).removeClass('disabled');
                }else{
                    $(this).addClass('disabled');
                }
                processMap();
            });

            //check if map viewer was clicked on menu
            $(document).on('click', '.w-dropdown-link', function(e){
                var val = $(this).attr('href');
                var regexp = /\B\#\w\w+\b/g
                var mapLink = val.match(regexp);

                if(mapLink == "#mapSales"){
                    e.preventDefault();
                    $("#mapSearchSale").removeClass('disabled');
                    $("#mapSearchRent").addClass('disabled');
                    bounded = false;
                    showMap();
                }else if(mapLink == "#mapRentals"){
                    e.preventDefault();
                    $("#mapSearchRent").removeClass('disabled');
                    $("#mapSearchSale").addClass('disabled');
                    bounded = false;
                    showMap();
                }
            });

            function showMap(){
                bodyScroll = false;
                set_scrollable_element(searchMapWrapper);

                searchMapWrapper.fadeIn();
                startMap();
                processMap();
                fixSearchMapHeight();

                analyticsMapOpened();
            }

            function hideMap(){
                searchMapWrapper.fadeOut();
                unset_scrollable_element();
                bodyScroll = true;
            }

            function startMap(){
                if(!mapStarted){
                    mapStarted = true;

                    var latLng = {
                        lat: parseFloat(20.664348833879615),
                        lng: parseFloat(-105.25233127176762)
                    };
                    var pos = new google.maps.LatLng(latLng.lat, latLng.lng);
                    searchMap = new google.maps.Map(document.getElementById('searchMap'), {
                        //center: pos,
                        //position: pos,
                        zoom: 11,
                        scrollwheel: true,
                        gestureHandling: 'greedy'
                    });

                    infoWindow = new google.maps.InfoWindow();
            
                }
            }

            function processMap(){
                if(mapItems){
                    renderMap();
                }else{
                    getMapData().then(function(){
                        renderMap();
                    });
                }
            }

            function renderMap(){
                clearMap();
        
                var bounds = new google.maps.LatLngBounds();
                var resultsFound = false;

                mapSetSearchFilter();

                for(var i=0; i < mapItems.length; i++){
                    if(mapItems[i].show){
                        resultsFound = true;
                        mapItems[i].gmapPosition = new google.maps.LatLng(mapItems[i].position.lat, mapItems[i].position.lng);

                        mapItems[i].marker = new google.maps.Marker({
                            position: mapItems[i].gmapPosition,
                            icon: _THEME_PATH + '/images/map/' + mapItems[i].type + '-pin.png',
                            map: searchMap
                        });

                        addInfoWindow(i);

                        bounds.extend(mapItems[i].marker.getPosition());
                    }
                }

                if(!bounded && resultsFound){
                    bounded = true;
                    searchMap.fitBounds(bounds);
                }
                
            }

            function addInfoWindow(i) {
                google.maps.event.addListener(mapItems[i].marker, 'click', (function(marker, i) {
                    return function() {
                        var content = '';
                        if(mapItems[i].img){
                            content +=  '<div class="mapInnerImgWrapper">' +
                                            '<a target="_blank" data-index="'+i+'" href="'+mapItems[i].permalink+'"><img src="' + mapItems[i].img + '" />' + '</a>' +
                                        '</div>';
                        }
                        content += '<a target="_blank" class="mapInnerTitle" data-index="'+i+'" href="'+mapItems[i].permalink+'">' + 
                                        mapItems[i].title + 
                                    '</a>';
                        content += '<div class="mapInnerDescription">' + mapItems[i].description + '</div>';
                        if(mapItems[i].price){
                            content += '<div class="mapInnerPrice">$' + 
                                            mapItems[i].price + ' <small>USD</small>' +
                                            '<div class="mapColorCircle ' + mapItems[i].type + ' "></div>' + 
                                        '</div>';
                        }


                        $.when(mapImgLoads(i)).then(function(){
                            infoWindow.setContent(content);
                            infoWindow.setOptions({ maxWidth: 250});
                            infoWindow.open(searchMap, marker);
                        });

                        analyticsMapOpenPin(mapItems[i].title);

                    }
                })(mapItems[i].marker, i));
            }

            function mapImgLoads(i){
                var $d = $.Deferred();

                var imgPath = mapItems[i].img;
                if(imgPath){
                    var img = new Image();
                    img.onload = function(){
                        $d.resolve();
                    };
                    img.src = imgPath;
                }else{
                    $d.resolve();
                }

                return $d.promise();
            }


            function clearMap(){
                for (var i = 0; i < mapItems.length; i++) {
                    if(mapItems[i].marker){
                        mapItems[i].marker.setMap(null);
                    }
                }
            }

            function mapSetSearchFilter(){
                mapSearchFilter = {};
                mapSearchFilter.saleAllowed = inputFilterSale.hasClass('disabled') ? false : true;
                mapSearchFilter.rentAllowed = inputFilterRent.hasClass('disabled') ? false : true;
                mapSearchFilter.devAllowed = inputFilterDev.hasClass('disabled') ? false : true;

                for(var i=0; i < mapItems.length; i++){
                    if(mapItems[i].type == 'permanent-sb' || (mapSearchFilter.saleAllowed && mapItems[i].type == 'property') || 
                        (mapSearchFilter.rentAllowed && mapItems[i].type == 'rent') ||
                        (mapSearchFilter.devAllowed && mapItems[i].type == 'development')){
                        mapItems[i].show = true;
                    }else{
                        mapItems[i].show = false;
                    }
                }

            }

            function getMapData(usePromise){
                return $.getJSON(_HOME_PAGE, {mapData: true}).done(function(res){
                    mapItems = res;
                });
            }

            function analyticsMapOpened() {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map opened' });
                }
            }
            
            function analyticsMapOpenPin(label) {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map pin: ' + label });
                }
            }

            function analyticsMapPropertyDetails(label) {
                if(!_IS_ADMIN){
                    ga('send', 'event', { eventCategory: 'Map interaction', eventAction: 'click', eventLabel: 'Map details: ' + label });
                }
            }
        }

    };

    function fixSearchMapHeight(){
        if($("#searchMap").length > 0){
            $("#searchMap").css('height', viewport.h - $(".sbmap-search-wrapper").outerHeight() + 'px');
        }
    }

    initSearchMap();

    /*****************************/
    /*****************************/

    /** SHOW SEARCH MLS **/
    $('.btn-show-mls').on('click', function(e){
        e.preventDefault();
        $('.sb2-cont-search-mls').fadeIn();
    });
    $('.sb2-btn-close').on('click', function(){
        $('.sb2-cont-search-mls').fadeOut();
    });
    $(".sb2-mls-search").on('submit', function(e){
        var form = $(this);
        var beds = form.find('#mls-max-beds').val();
        form.find('#mls-min-beds').val(beds);
        var baths = form.find('#mls-max-baths').val();
        form.find('#mls-min-baths').val(baths);
    });
    /** END SEARCH **/



    /*** FLEX MLS OVERRIDES ***/
    var flex_listing = $(".flexmls_connect__sr_result");
    if(flex_listing.length > 0){
        flex_listing.last().addClass('last');
    }
    var flex_search_button = $(".flexmls_connect__search_new_submit");
    /*if(flex_search_button){
        flex_search_button.css({
            background: '#333',
            color: '#fff',
            'text-shadow': 'none',
            'box-shadow': 'none',
            'display': 'block'
        });
    }*/



    /** LOT HV5 **/
    var hv5_play = $("#hv5-play");
    if(hv5_play.length > 0){
        var template = $("#hv5-video-template").text();
        hv5_play.on('click', function(){
            $(".hv5-iframe-container").css('z-index', 20);
            $(".hv5-iframe-container .hv5-iframe-embed-container").html(template);
            $(".hv5-video-img-placeholder").css('opacity', 0);
            hv5_play.fadeOut();
        });

        $("#hv5-private-form").on('submit', function(e){
            e.preventDefault();
            var key = $(this).data('pk');
            setTimeout(function(){
                if($("#hv5-key").val().toLowerCase() == key){
                    $(".hv5-private-overlay-block").fadeOut();
                }else{
                    $(".hv5-wrong-data").fadeIn();
                }
            }, 700);
            return false;
        });
    }

    /* TEMPLATE SERVICES SCRIPT */

    function form_in_process($btn, init){
        init = (typeof init == "undefined") ? false : init;
        var in_process = $btn.data('in-process');
        var dots = 0;
        var text = '';

        if(typeof in_process == "undefined" || init == true){
            $btn.data('in-process', '1');
            $btn.data('dots', '1');

            if(!$btn.data('original-value')){ //set original value for first time
                $btn.data('original-value', $btn.val());
            }
            in_process = 1;
        }

        in_process = parseInt(in_process);
        if(in_process == 1){
            dots = parseInt($btn.data('dots'));
            text = $btn.data('original-value');
            for(var i = 0; i < dots; i++){
                text += '.';
            }

            dots++;
            dots = (dots > 3) ? 1 : dots;

            $btn.data('dots', dots);
            $btn.val(text);
            setTimeout(function(){
                form_in_process($btn);
            }, 500);
        }
        
    }
    function form_stop_process($btn){
        $btn.data('in-process', '0');
        $btn.data('dots', '1');
        $btn.val($btn.data('original-value'));
    }

    var sending_contact_form_service = false;
    $("#contact_form_service").submit(function(e){
        e.preventDefault();
        var form = $(this);

        if(!sending_contact_form_service){
            var btn_process = form.find('.submit');
            form_in_process(btn_process, true);

            sending_contact_form_service = false;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-wrapper').find(".w-form-done").hide();
            $.post(action, req, function(data){
                /*form.fadeOut('slow', function(){
                });*/
            
                form.closest('.form-wrapper').find('.w-form-done').fadeIn();
                
                form_stop_process(btn_process);
                sending_contact_form_service = false;
                form.trigger("reset");
            },'json');
        }
    });
    if($("#contact_form_service").length > 0){
        setTimeout(function(){
            var input = $("<input type='hidden' name='cmd' value='765476543a'>");
            $("#contact_form_service").append(input);
        },1500);
    }



    /** Search Map Improved **/

    // SB search Map including flex properties
    function map2_handler(){
        var map2Modal = $(".map2-modal");
        var map2 = $(".map2-wrapper");
        var map2Container = $(".map2-container");
        var map2PropWrapper = $(".map2-properties-wrapper");
        var map2PropInner = $(".map2-properties-inner");

        map2Modal.outerHeight(viewport.h);
        map2.outerHeight(viewport.h);

        if(map2.length){
            if(viewport.w > 991){
                map2Container.css('padding-left', map2PropWrapper.outerWidth() + 'px');

                //vertical properties
                map2Container.outerHeight(viewport.h);
                map2PropWrapper.outerHeight(viewport.h);
                map2PropInner.outerWidth('');
                var propInnerOffset = 75 + 20 + 20; //padding top, extra padding top, padding bottom.
                map2PropInner.outerHeight( viewport.h - propInnerOffset );
            }else{
                map2Container.css('padding-left', '');

                //horizontal properties
                map2Container.outerHeight('');
                map2PropWrapper.outerHeight('');
                map2PropInner.outerHeight('');
                var propInnerPadding = parseInt(map2PropInner.css('padding-left')) + parseInt(map2PropInner.css('padding-right'));
                var items = map2PropInner.find(".map2-property-item");
                var marginRight = parseInt(items.first().css('margin-right'));
                var propInnerWidth = propInnerPadding + ( items.length * ( items.first().outerWidth() + marginRight ));
                map2PropInner.outerWidth(propInnerWidth);
            }
        }
    }

    function searchMapImproved(){
        var apiUrl = 'https://api.sbpvr.com/';
        var isVisible = false;

        var modal = $(".map2-modal");
        var formBtn = modal.find('.map2-button');
        var sidebarProperties = $(".map2-properties-wrapper");

        var template;

        var map;
        var infowindow;
        var mapStarted = false;
        var properties = [];

        var boundsCheckTimeout;

        //initializer
        function init(){
            setFormFields();
            renderMap();
            drawMap();
            template = $( $("#map2-item-template").html() );
        }
        init();


        //fill the form field select according to current page language
        function setFormFields(){
            //$.get(apiUrl + 'prepareFields', {lang: _LANG}, function(data){
            $.get(apiUrl + 'prepareFields', function(data){

                //populate status select field
                $(".map2-status-select").each(function(){
                    var select = $(this);
                    select.html('');

                    for(var status of data.property_statuses){

                        //evito crear el select de RENT por que no está funcionando la info de guardado de SB de la api.
                        /*if(status.id == 3 || status.id == 4){
                            continue;
                        }*/
                        var option = $("<option value='" + status.id + "'>"+ status.title +"</option>");
                        select.append(option);
                    }
                });

                //populate type select field
                $(".map2-type-select").each(function(){
                    var select = $(this);
                    select.html('');

                    var default_option = select.data('default-option');
                    var option = $("<option value=''>"+ default_option +"</option>");
                    select.append(option);

                    for(var type of data.property_types){
                        var option = $("<option value='" + type.id + "'>"+ type.title +"</option>");
                        select.append(option);
                    }
                });

            }, 'json');
        }

        //show the map on screen
        function showMap(callback){
            modal.fadeIn(function(){
                if(!isVisible){
                    callback();
                }
            });

            $("body").css('height', viewport.h + 'px').css('overflow', 'hidden');

            if(isVisible){
                callback();
            }
        }

        function hideMap(){
            $("body").css('height', '').css('overflow', '');
            modal.fadeOut();
            isVisible = false;
        }

        //makes the search request
        function search(req){
            formBtn.addClass('loading');
            req.push({name: 'lang', value: _LANG});

            //attach lat and lng limit zones

            clearMapProperties();
            $.get(apiUrl + 'search', req, function(data){
                properties = data;
                drawProperties();
                renderMap(); //render everything
                formBtn.removeClass('loading');

                //hides search menu form on mobile
                if(viewport.w <= 991){
                    $(".map2-menu-toggle").removeClass('w--open');
                    $(".map2-menu .w-nav-overlay").fadeOut();
                }
            });
        }

        //render properties from request on the sidebar
        function drawProperties(){
            if(properties.length){
                sidebarProperties.fadeIn();
                var wrapper = sidebarProperties.find(".map2-properties-inner");
                wrapper.html('');

                var counter = 0;
                for(var property of properties){

                    if(!inBounds(property.lat, property.lng)){ continue; }

                    if(counter++ > 40) { break; } //limit properties on the sidebar


                    var item = template.clone();
                    item.find(".map2-tpl-img-url").attr('href', property.permalink);
                    item.find(".map2-tpl-img-url").css('background-image', 'url(' + property.images + ')' );

                    //item.find(".map2-tpl-img").attr('src', property.images);
                    if(property.title.length > 70){
                        item.find(".map2-tpl-title").attr('href', property.permalink).text(property.title.substring(0, 30) + '...');
                    }else{
                        item.find(".map2-tpl-title").attr('href', property.permalink).text(property.title);
                    }

                    if(property.beds){
                        item.find(".map2-tpl-beds").find(".map2-detail-number").text(property.beds);
                    }else{
                        item.find(".map2-tpl-beds").remove();
                    }

                    if(property.baths){
                        item.find(".map2-tpl-baths").find(".map2-detail-number").text(property.baths);
                    }else{
                        item.find(".map2-tpl-baths").remove();
                    }

                    if(property.parking){
                        item.find(".map2-tpl-parking").find(".map2-detail-number").text(property.parking);
                    }else{
                        item.find(".map2-tpl-parking").remove();
                    }

                    item.find(".map2-tpl-description").text(property.description.substring(0, 50) + '...' );

                    var price = parseFloat(property.price);
                    if(price > 0){
                        price = addCommas(price.toFixed(0));
                        item.find(".map2-tpl-price").text(price);
                    }else{
                        item.find(".map2-property-price").remove();
                    }

                    item.find(".map2-tpl-more").attr('href', property.permalink);

                    wrapper.append(item);
                    map2_handler();
                }
                
            }else{
                sidebarProperties.fadeOut();
            }
        }

        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }


        //boolean: check if lat and lng is inside bounds of Google Maps Map
        function inBounds(lat, lng){
            var bounds = map.getBounds();

            if(bounds != null){
                if((lat > bounds.f.b && lat < bounds.f.f) && (lng > bounds.b.b && lng < bounds.b.f)){
                    return true;
                }
            }

            return false;
        }


        //draw map containe, map properties and fix height of the map if needed
        function renderMap(){
            drawMap();
            drawMapProperties();
            fixMapHeight();
        }


        //first mar draw and definition
        function drawMap(){
            if(!mapStarted){
                mapStarted = true;
                map = new google.maps.Map(document.getElementById('map2-google'), {
                    zoom: 11,
                    scrollwheel: true,
                    gestureHandling: 'greedy'
                });

                infowindow = new google.maps.InfoWindow();

                //when map changes, draw Properties from sidbar
                map.addListener('bounds_changed', function() {

                    clearTimeout(boundsCheckTimeout);

                    boundsCheckTimeout = setTimeout(function(){
                        drawProperties();
                    }, 400);

                });
            }
        }


        //draw pins of properties in map
        function drawMapProperties(){
            if(properties.length == 0){ return 0; }

            var bounds = new google.maps.LatLngBounds();
            for(var i=0; i < properties.length ; i++){


                if(properties[i].lat == null || typeof properties[i].lat == "undefined" || properties[i].lat == 0) { continue; }


                properties[i].gmapPosition = new google.maps.LatLng(properties[i].lat, properties[i].lng);

                properties[i].marker = new google.maps.Marker({
                    position: properties[i].gmapPosition,
                    icon: _THEME_PATH + '/images/sb-map2-pin.png',
                    map: map
                });

                addInfoWindow(i);
                bounds.extend(properties[i].marker.getPosition());
            }

            map.fitBounds(bounds);

            if(map.getZoom() > 15){
                map.setZoom(15);
            }
        }


        //add inforwindows to properties: i = index of the property in "properties" array
        function addInfoWindow(i) {
            google.maps.event.addListener(properties[i].marker, 'click', (function(marker, i) {
                return function() {
                    var content = '';

                    /* Image on the infowindow
                    if(properties[i].images){
                        content +=  '<div class="mapInnerImgWrapper">' +
                            '<a target="_blank" data-index="'+i+'" href="'+properties[i].permalink+'">' +
                                '<img src="' + properties[i].images + '" />' +
                            '</a>' +
                        '</div>';
                    }
                    */


                    content += '<a target="_blank" class="mapInnerTitle" data-index="'+i+'" href="'+properties[i].permalink+'">' +
                        properties[i].title +
                    '</a>';

                    var description = properties[i].description.substring(0, 80) + '...';

                    content += '<div class="mapInnerDescription">' + description + '</div>';

                    var price = parseFloat(properties[i].price);
                    if(price > 0){
                        price = addCommas(price.toFixed(0));
                        content += '<div class="mapInnerPrice">$' + price + ' <small>USD</small>' +
                            '</div>';
                    }


                    $.when(mapImgLoads(i)).then(function(){
                        infowindow.setContent(content);
                        infowindow.setOptions({ maxWidth: 250});
                        infowindow.open(map, marker);
                    });
                }
            })(properties[i].marker, i));
        }


        //lazy load img on the infowindows
        function mapImgLoads(i){
            var $d = $.Deferred();

            var imgPath = properties[i].images;
            if(imgPath){
                var img = new Image();
                img.onload = function(){
                    $d.resolve();
                };
                img.src = imgPath;
            }else{
                $d.resolve();
            }

            return $d.promise();
        }


        //remove properties pins from map
        function clearMapProperties(){
            if(properties.length > 0){
                for (var i = 0; i < properties.length; i++) {
                    if(properties[i].marker){
                        properties[i].marker.setMap(null);
                    }
                }
            }
        }


        //map height handler
        function fixMapHeight(){
            if($("#map2-google").length > 0){
                $("#map2-google").css('height', viewport.h - $(".sbmap-search-wrapper").outerHeight() + 'px');
            }
        }

        /************/
        /** EVENTS **/

        //scroll properties on 991 and below
        $('.map2-properties-wrapper').mousemove(function (e) {
            if(viewport.w <= 991){
                var moved = e.clientX / viewport.w;
                var innerWidth = $(this).find('.map2-properties-inner').outerWidth();
                var scrollAmount = innerWidth * moved;
                scrollAmount -= (viewport.w / 2);
                $('.map2-properties-wrapper').animate({scrollLeft: 0}, 0);
                $('.map2-properties-wrapper').animate({scrollLeft: scrollAmount},0);
            }
        });


        //form submit
        $(".map2-form").submit(function(e){
            e.preventDefault();
            var form = $(this);
            var req = form.serializeArray();
            showMap(function(){
                search(req);
            });
            //sincronizar_campos();
        });


        //hides map
        $(".map2-close-icon").click(function(){
            hideMap();
        });


        //mobile button trigger
        $("#map2-mobile-trigger").click(function(e){
            e.preventDefault();
            var req = [];
            showMap(function(){
                search(req);
            });
        });
    }

    searchMapImproved();

    /** End Search Map Improved **/

});