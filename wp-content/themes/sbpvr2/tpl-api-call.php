<?php
/*
Template Name: Api Call
*/
?>
<?php
    get_page_post_id();

    $args = array(
        'post_type' => $_GET['type'],
        'orderby' => 'rand',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'development',
                'field' => 'term_id',
                'terms' => array ($_GET['cat'])
            )
        )
    );

    $query = new WP_Query($args);

    $items = array();

    if(count($query->posts) > 0){
        $items = $query->posts;
    }

    $acf_fields = get_acf_fields(array('general_property_fields'));

    foreach($items as $k=>$v){
        $items[$k]->acf = get_acf_data($v->ID, $acf_fields);
        $items[$k]->permalink = get_permalink($v->ID);
    }

    echo json_encode($items);

    exit;
?>