var Webflow = Webflow || [];
Webflow.push(function() {
    // browser specific
    var viewport, viewport_p;
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    function get_scroll_top() {
        return $(window).scrollTop();
    }
    function get_viewport() {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        return {
            w: w,
            h: h
        };
    }
    function scroll_to_pos(top, time, extra_offset) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (top + extra_offset)
        }, time);
    }
    function scroll_to(elem, time, extra_offset, callback) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (elem.offset().top + extra_offset)
        }, time,
        function() { //callback
            if (typeof callback != "undefined") {
                callback();
            }
        });
    }
    viewport = get_viewport();
    Webflow.resize.on(function() {
      viewport = get_viewport(); 
    });
    Webflow.scroll.on(function() {
      handle_sticky_menu();
    });
    var navbarElem = $(".navbar");
    var navBrandElem = $(".brand");
    var navbarContainer = $(".navbar-container");
    var navMenu = $(".nav-menu");
    var navLink = $(".nav-link");
    function handle_sticky_menu(){
      if(viewport.w > 991 && get_scroll_top() > 300 ){
        navbarElem.css({
          '-webkit-transition': 'all 250ms ease',
          'transition': 'all 250ms ease'
        });
        navbarElem.css({
          'top': '0',
          'border-bottom': '1px solid #f0f0f0'
        });
        navbarContainer.css({
          'padding': '15px 20px 20px'
        });
        navMenu.css({
          'top': '4px'
        });
        navLink.css({
          'font-size': '15px'
        });
        navBrandElem.css({
          'width': '120px'
        });
      }else{
        navbarElem.css({
          '-webkit-transition': 'none',
          'transition': 'none'
        });
        navbarElem.css({
          'top':'-' + get_scroll_top() + 'px',
          'border-bottom': ''
        });
        navbarContainer.css({
          'padding': ''
        });
        navMenu.css({
          'top': ''
        });
        navLink.css({
          'font-size': ''
        });
        navBrandElem.css({
          'width': ''
        });
      }
    }

    function form_in_process($btn, init){
        init = (typeof init == "undefined") ? false : init;
        var in_process = $btn.data('in-process');
        var dots = 0;
        var text = '';

        if(typeof in_process == "undefined" || init == true){
            $btn.data('in-process', '1');
            $btn.data('dots', '1');

            if(!$btn.data('original-value')){ //set original value for first time
                $btn.data('original-value', $btn.val());
            }
            in_process = 1;
        }

        in_process = parseInt(in_process);
        if(in_process == 1){
            dots = parseInt($btn.data('dots'));
            text = $btn.data('original-value');
            for(var i = 0; i < dots; i++){
                text += '.';
            }

            dots++;
            dots = (dots > 3) ? 1 : dots;

            $btn.data('dots', dots);
            $btn.val(text);
            setTimeout(function(){
                form_in_process($btn);
            }, 500);
        }
        
    }
    function form_stop_process($btn){
        $btn.data('in-process', '0');
        $btn.data('dots', '1');
    }

    // contact form
    var sending_contact_form = false;
    $("#contact_form").submit(function(e){
        e.preventDefault();
        var form = $(this);

        if(!sending_contact_form){
            var btn_process = form.find('.form-submit');
            form_in_process(btn_process, true);

            sending_contact_form = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.reserve-form-wrapper').find(".w-form-done").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.fadeOut('slow', function(){
                        form.fadeOut();
                        form.closest('.reserve-form-wrapper').find('.w-form-done').fadeIn();

                        _ga_track('Website', 'redirect', 'Redirect VMARINA');
                        document.location = 'http://www.vmarina.mx';
                    });
                }
                form_stop_process(btn_process);
                sending_contact_form = false;
                //form.trigger("reset");
            },'json');
        }
    });
    $("#contact_form").find('.cmd').val('28283s');

    $(".reserve-now-btn").click(function(){
        setTimeout(function(){
            $("#reserve-now-input").focus();
        }, 750);
    });

    function _ga_track(category, action, label) {
        if(!_IS_ADMIN){
            ga('send', 'event', {
                eventCategory: category,
                eventAction: action,
                eventLabel: label
            });
        }
    }
    $(".ga-track").click(function(){
        var elem = $(this);
        var category = elem.data('ga-category');
        var action = elem.data('ga-action');
        var label = elem.data('ga-label');
        _ga_track(category, action, label);
    });
    $(".ga-track-submit").submit(function(){
        var elem = $(this);
        var category = elem.data('ga-category');
        var action = elem.data('ga-action');
        var label = elem.data('ga-label');
        _ga_track(category, action, label);
    });
});