<?php
/*
Template Name: SB Concierge
*/
?>
<?php 
    the_post();

    //catch concierge form ajax request
    if(count($_POST)>0){
        if(! (isset($_POST['f_cmd']) && $_POST['f_cmd'] == '7764') ){
            exit;
        }

        $body = '<h3>' . get_the_title() . '</h3>';
        $body .= "<b>".pll__('Name').":</b> " . htmlspecialchars($_POST['f_name']) . '<br>';
        $body .= "<b>".pll__('Phone').":</b> " . htmlspecialchars($_POST['f_phone']) . '<br>';
        $body .= "<b>".pll__('Email').":</b> " . htmlspecialchars($_POST['f_email']) . '<br>'; 
        if(isset($_POST['f_interest'])){
            $interest = '';
            foreach($_POST['f_interest'] as $v){
                $interest .= htmlspecialchars($v) . ', ';
            }
            $interest = rtrim($interest, ', ');
            $body .= "<b>".pll__("I'm interested in").":</b> " . $interest . '<br>';
        }
        $body .= "<b>".pll__('Message').":</b> " . htmlspecialchars($_POST['f_message']) . '<br>';


        $body .= "--------------------------------------------------------- <br>";
        $body .= "<b>Website language:</b> ";
        $body .= (pll_current_language() == 'en') ? 'English' : 'Spanish' . '<br>';


        $subject = 'SBPVR - SB Concierge';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        $to2 = 'alan.cordova@mexmags.com';
        wp_mail( $to2, $subject, $body, $headers );

        if(LIVE_SITE){
            $headers[] = 'Cc: mariana@sbpvr.com'; 
            $to = get_field('c_global_email','option');
            wp_mail( $to, $subject, $body, $headers );
        }

        
        echo json_encode(array('success'=>true));
        exit;
    }
?>
<?php get_header();?>
<div class="modal-bg" style="margin-top: 0;">
    <div class="modal-content">
        <div class="modal-container w-clearfix">
            <a href="#" class="v2-close-modal btn-close w-button"><em class="italic-text-2"></em></a>
            <div class="v2-contact-form">
                <h3 class="heading-16"><?php the_title()?></h3>
                <div class="text-block-6"><?=get_field('c_concierge_subtitle')?></div>
                <div class="form-block-2">
                    <form id="contact_form" action="<?=get_permalink()?>" class="form-2">
                        <input type="hidden" name="f_cmd" class="cmd" value="" />
                        <div class="div-block-17">
                            <input class="contact-form-input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                            <input class="contact-form-input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
                        </div>
                        <input class="contact-form-input-lg w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
                        <div class="checkbox-container">
                            <label class="label"><?=pll__("I'm interested in")?>:</label>
                            <div class="checkbox w-checkbox w-clearfix">
                                <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_1" name="f_interest[]" type="checkbox" value="<?=pll__('Activities')?>">
                                <label class="checkbox-text w-form-label" for="checkbox_1"><?=pll__('Activities')?></label>
                            </div>
                            <div class="checkbox w-checkbox w-clearfix">
                                <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_2" name="f_interest[]" type="checkbox" value="<?=pll__('Fishing Trips')?>">
                                <label class="checkbox-text w-form-label" for="checkbox_2"><?=pll__('Fishing Trips')?></label>
                            </div>
                            <div class="checkbox w-checkbox w-clearfix">
                                <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_3" name="f_interest[]" type="checkbox" value="<?=pll__('Local Gastronomy')?>">
                                <label class="checkbox-text w-form-label" for="checkbox_3"><?=pll__('Local Gastronomy')?></label>
                            </div>
                            <div class="checkbox w-checkbox w-clearfix">
                                <input class="checkbox-input scrollfix w-checkbox-input" id="checkbox_4" name="f_interest[]" type="checkbox" value="<?=pll__('Other')?>">
                                <label class="checkbox-text w-form-label" for="checkbox_4"><?=pll__('Other')?></label>
                            </div>
                        </div>
                        <div class="text-block-7"><?=pll__('Let us know if you have a special request')?></div>
                        <textarea class="contact-form-input-lg w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
                        <input class="btn-black w-button" type="submit" value="<?=pll__('Send')?>">
                    </form>
                    <div class="w-form-done">
                        <div><?=pll__('Contact success response')?></div>
                    </div>
                    <div class="w-form-fail">
                        <div><?=pll__('Contact error response')?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>
<div class="concierge-section">
    <div class="concierge-title">
        <div class="container-12 w-container">
            <?php require_once 'includes/modules/breadcrumbs.php';?>
            <h1 class="heading-11"><?=pll__('Concierge')?></h1>
            <h2 class="heading-12"><?php the_title()?></h2>
            <div class="text-block-5"><?=get_field('c_concierge_subtitle')?></div>
        </div>
    </div>
    <div class="concierge-activities">
        <div class="container-8 w-container">
            <h2 class="heading-13"><?=get_field('c_concierge_activities_main_title')?></h2>
            <p class="paragraph-11"><?=get_field('c_concierge_activities_content')?></p>
            <a href="#" class="btn-lg-transparent w-button" data-ix="show-modal"><?=pll__('Start here')?></a>
        </div>
    </div>
    <div class="concierge-tintoque">
        <div class="container-9 w-container">
            <h2 class="heading-14"><?=get_field('c_concierge_tintoque_main_title')?></h2>
            <div class="w-clearfix">
                <p class="paragraph-12"><?=get_field('c_concierge_tintoque_content')?></p>
            </div>
            <a href="http://tintoque.mx" class="btn-lg-transparent-right w-button" target="_blank"><?=pll__('Visit Tintoque Website')?></a>
        </div>
    </div>
    <div class="concierge-fishing">
        <div class="container-10 w-container">
            <h2 class="heading-15"><?=get_field('c_concierge_fishing_main_title')?></h2>
            <p class="paragraph-13"><?=get_field('c_concierge_fishing_content')?></p>
            <?php $fishingUrl = (LANG == 'es') ? 'http://fishingpvr.com/es/index.php' : 'http://fishingpvr.com'; ?>
            <a class="btn-lg-transparent w-button" href="<?=$fishingUrl?>" target="_blank">
                <?=pll__('Reserve Now')?>
            </a>
        </div>
    </div>
    <div class="concierge-surf">
        <div class="container-9 w-container">
            <h2 class="heading-14"><?=get_field('c_concierge_surf_main_title')?></h2>
            <div class="w-clearfix">
                <p class="paragraph-12"><?=get_field('c_concierge_surf_content')?></p>
            </div>
            <?php $surfUrl = (LANG == 'es') ? 'http://fishingpvr.com/es/index.php#surf-tours' : 'http://fishingpvr.com#surf-tours'; ?>
            <a class="btn-lg-transparent-right w-button" href="<?=$surfUrl?>" target="_blank">
                <?=pll__('Reserve Now')?>
            </a>
        </div>
    </div>
</div>

<?php get_footer();?>