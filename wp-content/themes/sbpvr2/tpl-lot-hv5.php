<?php
/*
Template Name: Lot HV-5
*/
?>
<?php
    the_post();

    $features = get_field('hv5_features');
    $gallery = get_field('hv5_gallery');
    $pdf = get_field('hv5_pdf');
?>
<?php global $body_padding; $body_padding = true; ?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>


    <div class="hv5-private-content">
        <div class="hv5-private-overlay-block w-form">
            <form id="hv5-private-form" class="hv5-private-form" data-pk="<?=get_field('hv5_private_key')?>" autocomplete="off">
                <label for="name" class="hv5-private-label-text">
                    <?=pll__('Lot hv-5 ask key')?>
                </label>
                <input type="text" class="hv5-private-input w-input" id="hv5-key" required="" autocomplete="off">
                <label class="hv5-wrong-data"><?=pll__('hv5-wrong-data')?></label>
                <input type="submit" value="<?=pll__("Enter")?>" class="hv5-private-btn w-button">

            </form>
        </div>

        <div class="hv5-main">
            <div class="hv5-main-content">
                <h1 class="hv5-title"><?=the_title()?></h1>
                <h2 class="hv5-subtitle"><?=pll__('Proyecto Costa Banderas')?></h2>
                <div class="hv5-details">
                    <div class="hv5-detail-item">
                        <img src="<?=THEME_PATH?>/lot-hv5/img/icon-measures.svg" class="hv5-detail-icon" alt="">
                        <div class="hv5-detail-text">13,92 ha</div>
                    </div>
                    <div class="hv5-detail-item"><img src="<?=THEME_PATH?>/lot-hv5/img/icon-location.svg" class="hv5-detail-icon" alt="">
                        <div class="hv5-detail-text">Costa Banderas, Nayarit</div>
                    </div>
                </div>
                <div class="hv5-content-p">
                    <?php the_content()?>
                </div>
                <blockquote class="hv5-blockquote"><?=get_field('hv5_quote')?></blockquote>
            </div>
            <div class="hv5-main-video">
                <div class="hv5-background-video">
                    <div id="hv5-video-template">
                        <?php echo htmlspecialchars('
                            <video controls autoplay controlsList="nodownload">
                                <source src="'.THEME_PATH.'/lot-hv5/video/lot-hv5.mp4" type="video/mp4" />
                                <source src="'.THEME_PATH.'/lot-hv5/video/lot-hv5.ogv" type="video/ogv" />
                                <source src="'.THEME_PATH.'/lot-hv5/video/lot-hv5.webm" type="video/webm" />
                            </video>
                        '); ?>
                    </div>
                    <div class="hv5-iframe-container">
                        <div class="hv5-iframe-embed-container"></div>
                    </div>
                    <img src="<?=THEME_PATH?>/lot-hv5/img/bg-video.jpg" class="hv5-video-img-placeholder" alt="Lot HV5 Video" title="Lot HV5 Video">
                    <a href="javascript:;" class="hv5-play-container" id="hv5-play">
                        <img src="<?=THEME_PATH?>/lot-hv5/img/icon-play-white.svg" class="hv5-play" alt="">
                    </a>
                </div>
            </div>
        </div>


        <div class="hv5-exp-main-container">
            <div class="hv5-parallax"></div>
            <div class="hv5-exp-container">
                <?php foreach($features as $feature):?>
                    <div class="hv5-exp-item" data-ix="hv5-hover-img">
                        <div class="hv5-exp-item-circle">
                            <img src="<?=$feature['hv5_feature_img']['url']?>" class="hv5-exp-item-circle-img">
                        </div>
                        <div class="hv5-exp-item-text"><?=$feature['hv5_feature_description']?></div>
                    </div>
                <?php endforeach;?>
            </div>

            <div class="hv5-exp-container-2 w-hidden-small w-hidden-tiny">
                <?php $thumbnail_gallery = 'hv5-gallery-1';?>
                <?php include 'lot-hv5/partial-gallery.php';?>
            </div>
        </div>

        <div class="hv5-exp-container-2 w-hidden-main w-hidden-medium">
            <?php $thumbnail_gallery = 'hv5-gallery-2';?>
            <?php include 'lot-hv5/partial-gallery.php';?>
        </div>
    </div>


<?php get_footer();?>