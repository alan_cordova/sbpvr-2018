<?php
/*
Template Name: Development detail
*/
?>
<?php
    the_post();

    $subtitle = get_field('c_development_subtitle');
    $secondary = get_field('c_development_secondary');
    $address = get_field('c_development_address');
    $properties_taxonomy = get_field('c_development_properties');
    $featured_img = get_field('c_development_featured');
    $gallery = get_field('c_development_gallery');

    $website = get_field('c_development_website');
    $map = get_field('c_development_map');

    $development_taxonomy = get_field('c_development_properties');
    
    //verify if contains sales
    //prepare search query for sales
    $developments = get_taxonomy_list('development', 'property');
    $has_sales_properties = false;
    foreach($developments as $d){
        if($development_taxonomy && $development_taxonomy->term_id == $d->term_id){
            $has_sales_properties = true;
        }
    }

    //verify if contains rentals
    //prepare search query for rentals
    $developments = get_taxonomy_list('development', 'rent');
    $has_rents_properties = false;
    foreach($developments as $d){
        if($development_taxonomy && $development_taxonomy->term_id == $d->term_id){
            $has_rents_properties = true;
        }
    }

    $amenities_left = get_field('c_development_amenities_left');
    $amenities_right = get_field('c_development_amenities_right');

    $is_marina_tower = (get_the_ID() == $cms_ids['marina-tower']);
    $is_tres_mares = (get_the_ID() == $cms_ids['tres-mares']);
?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>
<div class="modal-bg" style="margin-top: 0px">
    <div class="modal-content">
        <div class="v2-modal-developments modal-container w-clearfix">
            <a href="#" class="v2-close-modal btn-close w-button"><em class="italic-text-2"></em></a>
            <?php if($amenities_left || $amenities_right):?>
                <div class="dev-amenities">
                    <?php if($amenities_left):?>
                        <div class="dev-amenities-col left <?=!$amenities_right? 'col-full':''?>" >
                            <?=$amenities_left?>
                        </div><!--
                    --><?php endif;?><!--
                    --><?php if($amenities_right):?><!--
                        --><div class="dev-amenities-col right <?=!$amenities_left ? 'col-full':''?>">
                            <?=$amenities_right?>
                        </div><!--
                    --><?php endif;?>
                </div>
            <?php endif;?>
            <?php if($is_tres_mares): ?>
                <!-- tres mares video -->
                <div class="v2-container-iframe-video">
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php require_once 'includes/modules/menu.php';?>

<div class="development-title" style="background-image: url(<?=$featured_img['url']?>)">
    <div class="development-detail-title">
        <h1 class="heading-6"><?php the_title()?></h1>
        <?php require_once 'includes/modules/breadcrumbs.php';?>
    </div>
</div>
<div class="development-detail">
    <?php if($website || $is_marina_tower){?>  
        <?php $website_printed = false?>
        <?php if($is_marina_tower):?>
            <a href="https://marinatower.mx" class="title-section-btn w-button" target="_blank">marinatower.mx</a>
            <?php $website_printed = true;?>
        <?php endif;?>

        <?php if(!$website_printed && !$has_sales_properties && !$has_rents_properties):?>
            <a href="<?=$website?>" class="title-section-btn w-button" target="_blank"><?=pll__('Visit')?> <?=pll__('Web Site')?></a>
            <?php $website_printed = true;?>
        <?php endif;?>
    <?php } ?>
    <div class="paragraph-9"><?php the_content()?></div>
    <div class="content-btn-details">
        <a href="#" class="v2-show-specs btn-details w-button"><?=pll__('Amenities &amp; Specs')?></a>
        <a href="#" class="btn-details w-button"><?=pll__('Get PDF')?></a>
        <?php if($has_rents_properties || $has_sales_properties):?>
            <?php if($has_sales_properties):?>
                <?php 
                    $url = get_post_type_archive_link('property');
                    $url .= '?devID=' . $development_taxonomy->term_id;
                    $url .= '#search-container';
                ?>
                <a href="<?=$url?>" class="btn-details w-button"><?=pll__('View sales listing')?></a>
            <?php endif;?>
            <?php if($has_rents_properties):?>
                <?php 
                    $url = get_post_type_archive_link('rent');
                    $url .= '?devID=' . $development_taxonomy->term_id;
                    $url .= '#search-container';
                ?>
                <a href="<?=$url?>" class="btn-details w-button"><?=pll__('View rentals')?></a>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="development-address-map">
        <div class="text-block-3"><?=$address?></div>
        <a href="#" data-lat="<?=$map['lat']?>" data-lng="<?=$map['lng']?>" class="btn-details-map w-button" target="_blank"><?=pll__('Show me this property in map')?></a>
    </div>
</div>

<?php get_footer();?>