

<div class="hv5-gallery-container">
    <div class="hv5-thumbnails-main-block">
        <?php foreach($gallery as $k=>$img):?>
            <?php if($k > 8) break; ?>
            <a href="#" class="hv5-thumbnails-block w-inline-block w-lightbox" data-ix="hv5-hover-thumbnail">
                <?php $overlayVisible = $k == 4 ? 'visible767':'';?>
                <div class="hv5-thumbnail-overlay <?=$overlayVisible?>"></div>
                <img src="<?=$img['sizes']['gallery-250x250']?>" class="hv5-thumbnail-img" alt="">
                <script type="application/json" class="w-json">{
                        "items": [
                            {
                                "type": "image",
                                "url": "<?=$img['url']?>"
                            }
                        ],
                        "group": "<?=$thumbnail_gallery?>"
                    }</script>
            </a>
        <?php endforeach;?>
        <div class="hv5-thumbnails-hidden">
            <?php foreach($gallery as $k=>$img):?>
                <?php if($k <= 8) continue; ?>
                <a href="#" class="hv5-thumbnails-block w-inline-block w-lightbox" data-ix="hv5-hover-thumbnail">
                    <img src="<?=$img['sizes']['gallery-250x250']?>" class="hv5-thumbnail-img" alt="">
                    <script type="application/json" class="w-json">{
                            "items": [
                                {
                                    "type": "image",
                                    "url": "<?=$img['url']?>"
                                }
                            ],
                            "group": "<?=$thumbnail_gallery?>"
                        }</script>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    <div class="hv5-specifications-block">
        <div class="hv5-specifications-item first">
            <div class="hv5-spec-item-left">
                <img src="<?=THEME_PATH?>/lot-hv5/img/icon-circle-location.svg" class="hv5-spec-item-img hidden" alt="">
            </div>
            <div class="hv5-spec-item-right">
                <h3 class="hv5-specifications-title"><?=pll__('Specifications')?></h3>
            </div>
        </div>
        <div class="hv5-specifications-item">
            <div class="hv5-spec-item-left">
                <img src="<?=THEME_PATH?>/lot-hv5/img/icon-circle-location-white.svg" class="hv5-spec-item-img" alt="">
            </div>
            <div class="hv5-spec-item-right">
                <div class="hv5-spec-item-title"><?=pll__('Adjoining (colindancias)')?>:</div>
                <div class="hv5-spec-item-content">
                    <?=get_field('hv5_adjoining')?>
                </div>
            </div>
        </div>
        <div class="hv5-specifications-item last">
            <div class="hv5-spec-item-left">
                <img src="<?=THEME_PATH?>/lot-hv5/img/icon-paper-white.svg" class="hv5-spec-item-img" alt="">
            </div>
            <div class="hv5-spec-item-right">
                <div class="hv5-spec-item-title"><?=pll__('Normativity')?>:</div>
                <div class="hv5-spec-item-content">
                    <?=get_field('hv5_normativity')?>
                </div>
                <a href="<?=$pdf['url']?>" target="_blank" class="hv5-download-btn w-hidden-main w-button">
                    <?=pll__('Download PDF')?>
                </a>
            </div>
        </div>
    </div>
    <div class="hv5-download-block w-hidden-medium w-hidden-small w-hidden-tiny">
        <a href="<?=$pdf['url']?>" target="_blank" class="hv5-download-btn w-button">
            <?=pll__('Download PDF')?>
        </a>
    </div>
</div>