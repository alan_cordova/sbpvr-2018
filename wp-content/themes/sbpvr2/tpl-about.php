<?php
/*
Template Name: SB About
*/
?>
<?php 
    the_post();

    /* CONTACT SECTION */
    $textContact  = get_field('c_text_section_contact', 'option');
    $titleContact = get_field('c_title_section_contact', 'option');
?>
<?php require_once 'includes/modules/process-form.php';?>
<?php get_header();?>
<?php require_once 'includes/modules/menu.php';?>
<?php require_once 'includes/modules/side-menu.php';?>

<div class="modal-bg">
    <div class="modal-content">
        <div class="modal-container w-clearfix">
            <a href="#" class="v2-close-modal btn-close w-button"><em class="italic-text-2"></em></a>
            <iframe allowfullscreen="true" src="https://my.matterport.com/show/?m=P7GotoZaz5z" scrolling="auto" allowtransparency="true" class="video-3d"></iframe>
        </div>
    </div>
</div>

<div class="title-section">
    <div class="container-5 w-container">
      <?php require_once 'includes/modules/breadcrumbs.php';?>
      <h1 class="heading-2"><?=pll__('ABOUT US')?></h1>
      <div class="div-block-5">
        <h2 class="heading-3"><?php the_title()?></h2>
        <div class="paragraph-4"><?php the_content()?></div><a href="#" class="v2-virtual-tour title-section-btn w-button"><?=pll__('Take the Virtual Tour')?></a></div>
    </div>
  </div>
  <div class="content-section">
    <div class="container-6 w-container">
      <div class="div-block-6">
        <h2 class="heading-4"><?=get_field('c_about_secondary_title')?></h2>
        <div class="v2-text-why-about-us"><?=get_field('c_about_secondary_content')?></div>
      </div>
      <div class="div-block-7 w-hidden-small w-hidden-tiny"><img src="<?=THEME_PATH?>/images/sb.svg" class="image"></div>
    </div>
  </div>
  <div class="contact-section">
    <div class="contact-flex">
      <div class="contact-form">
        <h3><?=$titleContact?></h3>
        <p><?=$textContact?></p>
        <div class="form-block-2">
          <form id="contact_form" action="<?=get_permalink()?>" class="form-2">
              <input type="hidden" name="f_cmd" class="cmd" value="" />
              <div class="div-block-9">
                  <input class="contact-form-input w-input" maxlength="256" name="f_name" placeholder="<?=pll__('Name')?>" required="required" type="text">
                  <input class="contact-form-input w-input" maxlength="256" name="f_email" placeholder="<?=pll__('Email')?>" required="required" type="email">
              </div>
              <input class="contact-form-input-lg w-input" maxlength="256" name="f_phone" placeholder="<?=pll__('Phone')?>" required="required" type="text">
              <textarea class="contact-form-input-lg w-input" maxlength="5000" name="f_message" placeholder="<?=pll__('Message')?>..." required="required"></textarea>
              <input class="v2-btn-black btn-black w-button" type="submit" value="<?=pll__('Send')?>">
              <a class="v2-take-me btn-black w-button"><?=pll__('Take Me There')?></a>
          </form>
          <div class="w-form-done">
              <div><?=pll__('Contact success response')?></div>
          </div>
          <div class="w-form-fail">
              <div><?=pll__('Contact error response')?></div>
          </div>
        </div>
      </div>
      <?php $map = get_field('c_global_address_location', 'option');?>
      <div class="map map-container map-block w-hidden-small w-hidden-tiny" 
          data-lat="<?=$map['lat']?>" 
          data-lng="<?=$map['lng']?>" 
          data-marker="<?=THEME_PATH?>/images/icon-location-SB.png" 
          data-marker-title="Silva Brisset Realtors" 
          id="map-container">
      </div>
    </div>
  </div>
<?php get_footer();?>