<!DOCTYPE html>
<html>

  <head>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <h1>Hello Plunker!</h1>
    
    <div id="date" data-fecha="<?php echo strtotime("now + 185 days");?>"></div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  
    <script>
      $(function(){
        var to_date = $("#date").data('fecha');
        var current_date = (new Date().getTime() / 1000) + 5;
        
        setInterval(function(){
          var days = (to_date - current_date) / (60*60*24);
          console.log(days);
        }, 1000);
      });
    </script>
  </body>

</html>